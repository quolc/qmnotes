(* Content-type: application/vnd.wolfram.cdf.text *)

(*** Wolfram CDF File ***)
(* http://www.wolfram.com/cdf *)

(* CreatedBy='Mathematica 10.0' *)

(*************************************************************************)
(*                                                                       *)
(*  The Mathematica License under which this file was created prohibits  *)
(*  restricting third parties in receipt of this file from republishing  *)
(*  or redistributing it by any means, including but not limited to      *)
(*  rights management or terms of use, without the express consent of    *)
(*  Wolfram Research, Inc. For additional information concerning CDF     *)
(*  licensing and redistribution see:                                    *)
(*                                                                       *)
(*        www.wolfram.com/cdf/adopting-cdf/licensing-options.html        *)
(*                                                                       *)
(*************************************************************************)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[      1064,         20]
NotebookDataLength[     10253,        385]
NotebookOptionsPosition[      9310,        325]
NotebookOutlinePosition[      9669,        341]
CellTagsIndexPosition[      9626,        338]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell["\:307e\:305a\:3001\:898f\:683c\:5316\:3055\:308c\:3066\:3044\:306a\:3044\
\:6ce2\:52d5\:95a2\:6570\:3092\:5165\:529b\:3002a \:304c\:30dc\:30fc\:30a2\
\:534a\:5f84\:3002", "Subsection",
 CellChangeTimes->{{3.64266145274247*^9, 3.642661470323722*^9}}],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"U1s", "=", 
  RowBox[{"Exp", "[", 
   RowBox[{
    RowBox[{"-", "r"}], "/", "a"}], "]"}]}]], "Input",
 CellChangeTimes->{{3.642658015573536*^9, 3.642658022013523*^9}}],

Cell[BoxData[
 SuperscriptBox["\[ExponentialE]", 
  RowBox[{"-", 
   FractionBox["r", "a"]}]]], "Output",
 CellChangeTimes->{3.642658022537162*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"U2s", "=", 
  RowBox[{
   RowBox[{"(", 
    RowBox[{"2", "-", 
     RowBox[{"r", "/", "a"}]}], ")"}], 
   RowBox[{"Exp", "[", 
    RowBox[{
     RowBox[{"-", "r"}], "/", 
     RowBox[{"(", 
      RowBox[{"2", "a"}], ")"}]}], "]"}]}]}]], "Input",
 CellChangeTimes->{{3.6426580231989594`*^9, 3.64265803474228*^9}}],

Cell[BoxData[
 RowBox[{
  SuperscriptBox["\[ExponentialE]", 
   RowBox[{"-", 
    FractionBox["r", 
     RowBox[{"2", " ", "a"}]]}]], " ", 
  RowBox[{"(", 
   RowBox[{"2", "-", 
    FractionBox["r", "a"]}], ")"}]}]], "Output",
 CellChangeTimes->{3.6426580354608393`*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"U2pz", "=", 
  RowBox[{"z", " ", 
   RowBox[{"Exp", "[", 
    RowBox[{
     RowBox[{"-", "r"}], "/", 
     RowBox[{"(", 
      RowBox[{"2", "a"}], ")"}]}], "]"}]}]}]], "Input",
 CellChangeTimes->{{3.642658054943632*^9, 3.6426580668524857`*^9}}],

Cell[BoxData[
 RowBox[{
  SuperscriptBox["\[ExponentialE]", 
   RowBox[{"-", 
    FractionBox["r", 
     RowBox[{"2", " ", "a"}]]}]], " ", "z"}]], "Output",
 CellChangeTimes->{3.642658067489065*^9}]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell["\:6975\:5ea7\:6a19\:3067\:306e z \:306e\:5f0f\:3002", "Subsection",
 CellChangeTimes->{{3.642661486951126*^9, 3.642661489410581*^9}}],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"z", "=", 
  RowBox[{"r", " ", 
   RowBox[{"Cos", "[", "\[Theta]", "]"}]}]}]], "Input",
 CellChangeTimes->{{3.642658271207329*^9, 3.642658277595784*^9}}],

Cell[BoxData[
 RowBox[{"r", " ", 
  RowBox[{"Cos", "[", "\[Theta]", "]"}]}]], "Output",
 CellChangeTimes->{{3.642658275651533*^9, 3.6426582778560963`*^9}}]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell["\:6975\:5ea7\:6a19\:3067\:306e\:7a4d\:5206\:3002", "Subsection",
 CellChangeTimes->{{3.642661486951126*^9, 3.642661505482643*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{"int", "[", "f_", "]"}], ":=", 
  RowBox[{"Integrate", "[", 
   RowBox[{
    RowBox[{"f", " ", 
     RowBox[{"Sin", "[", "\[Theta]", "]"}], " ", 
     RowBox[{"r", "^", "2"}]}], ",", 
    RowBox[{"{", 
     RowBox[{"\[Phi]", ",", "0", ",", 
      RowBox[{"2", "Pi"}]}], "}"}], ",", 
    RowBox[{"{", 
     RowBox[{"\[Theta]", ",", "0", ",", "Pi"}], "}"}], ",", 
    RowBox[{"{", 
     RowBox[{"r", ",", "0", ",", "Infinity"}], "}"}]}], "]"}]}]], "Input",
 CellChangeTimes->{{3.642658069486081*^9, 3.642658099095244*^9}, {
  3.642658149649942*^9, 3.6426581736254997`*^9}, {3.642658236555475*^9, 
  3.642658240354451*^9}}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"\:898f\:683c\:5316\:3055\:308c\:305f\:3068\:3053\:308d\:3067", 
  "\:3001", 
  RowBox[{"2", "s", " ", "\:3068", " ", 
   FormBox[
    RowBox[{"2", 
     SubscriptBox["p", "z"], " "}],
    TraditionalForm], "\:3067", " ", "z", " ", "\:3092\:631f\:3080\:3068"}], 
  "\:3001", 
  RowBox[{"(", 
   RowBox[{"\:5f0f", 
    RowBox[{"(", "6.94", ")"}]}], ")"}]}]], "Subsection",
 CellChangeTimes->{{3.64266158476679*^9, 3.642661590864552*^9}, {
  3.6426616714470453`*^9, 3.642661732690425*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{"$Assumptions", "=", 
   RowBox[{"{", 
    RowBox[{"a", ">", "0"}], "}"}]}], ";"}]], "Input",
 CellChangeTimes->{{3.6426604109774113`*^9, 3.642660423693434*^9}}],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"int", "[", 
    RowBox[{"U2s", " ", "z", " ", "U2pz"}], "]"}], "/", 
   RowBox[{"Sqrt", "[", 
    RowBox[{
     RowBox[{"int", "[", 
      RowBox[{"U2s", " ", "U2s"}], "]"}], 
     RowBox[{"int", "[", 
      RowBox[{"U2pz", " ", "U2pz"}], "]"}]}], "]"}]}], "//", 
  "Simplify"}]], "Input",
 CellChangeTimes->{{3.642660356314039*^9, 3.64266043704521*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{"-", "3"}], " ", "a"}]], "Output",
 CellChangeTimes->{
  3.64266039197285*^9, {3.642660427127534*^9, 3.642660438083877*^9}}]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell[TextData[{
 "\:898f\:683c\:5316\:3055\:308c\:305f\:3068\:3053\:308d\:3067\:30011s \:3068 \
",
 Cell[BoxData[
  FormBox[
   RowBox[{"2", 
    SubscriptBox["p", "z"]}], TraditionalForm]],
  FormatType->"TraditionalForm"],
 " \:3067 z \:3092\:631f\:3080\:3068\:3001 (\:5f0f(6.103))"
}], "Subsection",
 CellChangeTimes->{{3.642661486951126*^9, 3.642661527081749*^9}, {
  3.6426616391220837`*^9, 3.642661643729972*^9}}],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{
    RowBox[{"int", "[", 
     RowBox[{"U1s", " ", "z", " ", "U2pz"}], "]"}], "^", "2"}], "/", 
   RowBox[{"(", 
    RowBox[{
     RowBox[{"int", "[", 
      RowBox[{"U1s", " ", "U1s"}], "]"}], " ", 
     RowBox[{"int", "[", 
      RowBox[{"U2pz", " ", "U2pz"}], "]"}]}], ")"}]}], " ", "/", " ", 
  RowBox[{"(", 
   RowBox[{
    RowBox[{"3", "/", "8"}], " ", 
    RowBox[{"k", "/", "a"}]}], ")"}]}]], "Input",
 CellChangeTimes->{{3.642658426752603*^9, 3.642658460257526*^9}}],

Cell[BoxData[
 FractionBox[
  RowBox[{"262144", " ", 
   SuperscriptBox["a", "3"]}], 
  RowBox[{"177147", " ", "k"}]]], "Output",
 CellChangeTimes->{3.642658866199711*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  FractionBox[
   RowBox[{"262144", " "}], 
   RowBox[{"177147", " "}]], "//", "N"}]], "Input",
 CellChangeTimes->{{3.6426591722780437`*^9, 3.642659174148972*^9}}],

Cell[BoxData["1.4798105528177163`"], "Output",
 CellChangeTimes->{3.6426591744596577`*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"FactorInteger", "[", 
  FractionBox[
   RowBox[{"262144", " "}], 
   RowBox[{"177147", " "}]], "]"}]], "Input",
 CellChangeTimes->{{3.642659109438653*^9, 3.642659115295706*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{"2", ",", "18"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"3", ",", 
     RowBox[{"-", "11"}]}], "}"}]}], "}"}]], "Output",
 CellChangeTimes->{3.642659115593074*^9}]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell[TextData[{
 "\:898f\:683c\:5316\:3055\:308c\:305f\:3068\:3053\:308d\:3067\:30011s \:3068 \
1s \:3067 ",
 Cell[BoxData[
  FormBox[
   SuperscriptBox["z", "2"], TraditionalForm]],
  FormatType->"TraditionalForm"],
 " \:3092\:631f\:3080\:3068\:3001 (\:5f0f(6.106))"
}], "Subsection",
 CellChangeTimes->{{3.642661486951126*^9, 3.642661568972641*^9}, {
  3.6426616569724913`*^9, 3.6426616587932577`*^9}}],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"int", "[", 
    RowBox[{"U1s", " ", 
     RowBox[{"z", "^", "2"}], " ", "U1s"}], "]"}], "/", 
   RowBox[{"int", "[", 
    RowBox[{"U1s", " ", "U1s"}], "]"}]}], "  ", "/", " ", 
  RowBox[{"(", 
   RowBox[{
    RowBox[{"3", "/", "8"}], " ", 
    RowBox[{"k", "/", "a"}]}], ")"}]}]], "Input",
 CellChangeTimes->{{3.6426591480397997`*^9, 3.642659160784462*^9}}],

Cell[BoxData[
 FractionBox[
  RowBox[{"8", " ", 
   SuperscriptBox["a", "3"]}], 
  RowBox[{"3", " ", "k"}]]], "Output",
 CellChangeTimes->{3.642659165232017*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"8", "/", "3"}], " ", "//", "N"}]], "Input",
 CellChangeTimes->{{3.642659178670656*^9, 3.642659180724696*^9}}],

Cell[BoxData["2.6666666666666665`"], "Output",
 CellChangeTimes->{3.6426591810670357`*^9}]
}, Open  ]]
}, Open  ]]
},
WindowSize->{808, 755},
WindowMargins->{{Automatic, 169}, {Automatic, 0}},
FrontEndVersion->"10.0 for Mac OS X x86 (32-bit, 64-bit Kernel) (September \
11, 2014)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[1486, 35, 255, 3, 44, "Subsection"],
Cell[CellGroupData[{
Cell[1766, 42, 191, 5, 28, "Input"],
Cell[1960, 49, 147, 4, 38, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[2144, 58, 336, 11, 28, "Input"],
Cell[2483, 71, 270, 9, 47, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[2790, 85, 268, 8, 28, "Input"],
Cell[3061, 95, 198, 6, 38, "Output"]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[3308, 107, 139, 1, 44, "Subsection"],
Cell[CellGroupData[{
Cell[3472, 112, 176, 4, 28, "Input"],
Cell[3651, 118, 155, 3, 28, "Output"]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[3855, 127, 136, 1, 44, "Subsection"],
Cell[3994, 130, 654, 17, 28, "Input"]
}, Open  ]],
Cell[CellGroupData[{
Cell[4685, 152, 510, 13, 49, "Subsection"],
Cell[5198, 167, 195, 5, 28, "Input"],
Cell[CellGroupData[{
Cell[5418, 176, 401, 12, 28, "Input"],
Cell[5822, 190, 158, 4, 28, "Output"]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[6029, 200, 419, 11, 46, "Subsection"],
Cell[CellGroupData[{
Cell[6473, 215, 521, 16, 28, "Input"],
Cell[6997, 233, 171, 5, 49, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[7205, 243, 187, 5, 48, "Input"],
Cell[7395, 250, 90, 1, 28, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[7522, 256, 201, 5, 48, "Input"],
Cell[7726, 263, 236, 8, 28, "Output"]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[8011, 277, 404, 10, 45, "Subsection"],
Cell[CellGroupData[{
Cell[8440, 291, 404, 12, 28, "Input"],
Cell[8847, 305, 161, 5, 50, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[9045, 315, 144, 3, 28, "Input"],
Cell[9192, 320, 90, 1, 28, "Output"]
}, Open  ]]
}, Open  ]]
}
]
*)

(* End of internal cache information *)

(* NotebookSignature pxT4Mrnk7MswtD1oYQW4n9fc *)
