(* Content-type: application/vnd.wolfram.cdf.text *)

(*** Wolfram CDF File ***)
(* http://www.wolfram.com/cdf *)

(* CreatedBy='Mathematica 10.0' *)

(*************************************************************************)
(*                                                                       *)
(*  The Mathematica License under which this file was created prohibits  *)
(*  restricting third parties in receipt of this file from republishing  *)
(*  or redistributing it by any means, including but not limited to      *)
(*  rights management or terms of use, without the express consent of    *)
(*  Wolfram Research, Inc. For additional information concerning CDF     *)
(*  licensing and redistribution see:                                    *)
(*                                                                       *)
(*        www.wolfram.com/cdf/adopting-cdf/licensing-options.html        *)
(*                                                                       *)
(*************************************************************************)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[      1064,         20]
NotebookDataLength[     16436,        562]
NotebookOptionsPosition[     15694,        510]
NotebookOutlinePosition[     16152,        530]
CellTagsIndexPosition[     16109,        527]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell["\:5143\:306e\:52d5\:5f84\:65b9\:5411\:306e\:65b9\:7a0b\:5f0f", \
"Subsection"],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"eq", "=", 
  RowBox[{
   RowBox[{
    RowBox[{
     RowBox[{"-", "1"}], "/", 
     RowBox[{"r", "^", "2"}]}], " ", 
    RowBox[{"D", "[", " ", 
     RowBox[{
      RowBox[{
       RowBox[{"r", "^", "2"}], " ", 
       RowBox[{"D", "[", " ", 
        RowBox[{
         RowBox[{"F", "[", "r", "]"}], ",", "r"}], "]"}]}], ",", "r"}], 
     "]"}]}], " ", "+", " ", 
   RowBox[{
    RowBox[{"(", 
     RowBox[{
      RowBox[{"L", 
       RowBox[{
        RowBox[{"(", 
         RowBox[{"L", "+", "1"}], ")"}], "/", 
        RowBox[{"r", "^", "2"}]}]}], " ", "+", " ", 
      RowBox[{
       RowBox[{"m", "^", "2"}], 
       RowBox[{
        RowBox[{"\[Omega]", "^", "2"}], "/", 
        RowBox[{"\[HBar]", "^", "2"}]}], "  ", 
       RowBox[{"r", "^", "2"}]}]}], " ", ")"}], 
    RowBox[{"F", "[", "r", "]"}]}], "-", 
   RowBox[{"2", "m", " ", 
    RowBox[{"En", "/", 
     RowBox[{"\[HBar]", "^", "2"}]}], " ", 
    RowBox[{"F", "[", "r", "]"}]}]}]}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"(", 
    RowBox[{
     FractionBox[
      RowBox[{"L", " ", 
       RowBox[{"(", 
        RowBox[{"1", "+", "L"}], ")"}]}], 
      SuperscriptBox["r", "2"]], "+", 
     FractionBox[
      RowBox[{
       SuperscriptBox["m", "2"], " ", 
       SuperscriptBox["r", "2"], " ", 
       SuperscriptBox["\[Omega]", "2"]}], 
      SuperscriptBox["\[HBar]", "2"]]}], ")"}], " ", 
   RowBox[{"F", "[", "r", "]"}]}], "-", 
  FractionBox[
   RowBox[{"2", " ", "En", " ", "m", " ", 
    RowBox[{"F", "[", "r", "]"}]}], 
   SuperscriptBox["\[HBar]", "2"]], "-", 
  FractionBox[
   RowBox[{
    RowBox[{"2", " ", "r", " ", 
     RowBox[{
      SuperscriptBox["F", "\[Prime]",
       MultilineFunction->None], "[", "r", "]"}]}], "+", 
    RowBox[{
     SuperscriptBox["r", "2"], " ", 
     RowBox[{
      SuperscriptBox["F", "\[Prime]\[Prime]",
       MultilineFunction->None], "[", "r", "]"}]}]}], 
   SuperscriptBox["r", "2"]]}]], "Output"]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell[TextData[{
 StyleBox["Mathematica",
  FontSlant->"Italic"],
 " \:306f\:8ce2\:3044\:306e\:3067\:76f4\:63a5\:3068\:3051\:307e\:3059\:304c:"
}], "Subsection"],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"DSolve", "[", 
   RowBox[{
    RowBox[{"eq", "\[Equal]", "0"}], ",", 
    RowBox[{"F", "[", "r", "]"}], ",", "r"}], "]"}], "//", 
  "Simplify"}]], "Input"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{"{", 
   RowBox[{
    RowBox[{"F", "[", "r", "]"}], "\[Rule]", 
    RowBox[{
     FractionBox["1", 
      SuperscriptBox["r", 
       RowBox[{"3", "/", "2"}]]], 
     RowBox[{
      SuperscriptBox["2", 
       RowBox[{
        FractionBox["1", "4"], "-", 
        FractionBox["L", "2"]}]], " ", 
      SuperscriptBox["\[ExponentialE]", 
       RowBox[{"-", 
        FractionBox[
         RowBox[{"m", " ", 
          SuperscriptBox["r", "2"], " ", "\[Omega]"}], 
         RowBox[{"2", " ", "\[HBar]"}]]}]], " ", 
      SuperscriptBox[
       RowBox[{"(", 
        SuperscriptBox["r", "2"], ")"}], 
       RowBox[{
        FractionBox["1", "4"], "-", 
        FractionBox["L", "2"]}]], " ", 
      RowBox[{"(", 
       RowBox[{
        RowBox[{
         RowBox[{"C", "[", "1", "]"}], " ", 
         RowBox[{"HypergeometricU", "[", 
          RowBox[{
           RowBox[{
            FractionBox["1", "4"], " ", 
            RowBox[{"(", 
             RowBox[{"1", "-", 
              RowBox[{"2", " ", "L"}], "-", 
              FractionBox[
               RowBox[{"2", " ", "En"}], 
               RowBox[{"\[Omega]", " ", "\[HBar]"}]]}], ")"}]}], ",", 
           RowBox[{
            FractionBox["1", "2"], "-", "L"}], ",", 
           FractionBox[
            RowBox[{"m", " ", 
             SuperscriptBox["r", "2"], " ", "\[Omega]"}], "\[HBar]"]}], 
          "]"}]}], "+", 
        RowBox[{
         RowBox[{"C", "[", "2", "]"}], " ", 
         RowBox[{"LaguerreL", "[", 
          RowBox[{
           RowBox[{
            FractionBox["1", "4"], " ", 
            RowBox[{"(", 
             RowBox[{
              RowBox[{"-", "1"}], "+", 
              RowBox[{"2", " ", "L"}], "+", 
              FractionBox[
               RowBox[{"2", " ", "En"}], 
               RowBox[{"\[Omega]", " ", "\[HBar]"}]]}], ")"}]}], ",", 
           RowBox[{
            RowBox[{"-", 
             FractionBox["1", "2"]}], "-", "L"}], ",", 
           FractionBox[
            RowBox[{"m", " ", 
             SuperscriptBox["r", "2"], " ", "\[Omega]"}], "\[HBar]"]}], 
          "]"}]}]}], ")"}]}]}]}], "}"}], "}"}]], "Output"]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell[TextData[StyleBox["\:539f\:70b9\:3068\:7121\:9650\:9060\:3067\:306e\:632f\
\:821e\:3044\:3092\:3068\:308a\:3060\:3059\:305f\:3081\:3001\:5909\:6570\:5909\
\:63db\:3059\:308b\:3068",
 FontSlant->"Italic"]], "Subsection"],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"eq", "/.", " ", 
   RowBox[{"F", "\[Rule]", 
    RowBox[{"(", " ", 
     RowBox[{
      RowBox[{
       RowBox[{"w", "[", "#", "]"}], " ", 
       RowBox[{"#", "^", "L"}], " ", 
       RowBox[{"Exp", "[", 
        RowBox[{
         RowBox[{"-", "m"}], " ", 
         RowBox[{"\[Omega]", " ", "/", "\[HBar]"}], " ", 
         RowBox[{
          RowBox[{"#", "^", "2"}], "/", "2"}]}], "]"}]}], " ", "&"}], 
     ")"}]}]}], "//", "Simplify"}]], "Input"],

Cell[BoxData[
 FractionBox[
  RowBox[{
   SuperscriptBox["\[ExponentialE]", 
    RowBox[{"-", 
     FractionBox[
      RowBox[{"m", " ", 
       SuperscriptBox["r", "2"], " ", "\[Omega]"}], 
      RowBox[{"2", " ", "\[HBar]"}]]}]], " ", 
   SuperscriptBox["r", 
    RowBox[{
     RowBox[{"-", "1"}], "+", "L"}]], " ", 
   RowBox[{"(", 
    RowBox[{
     RowBox[{"m", " ", "r", " ", 
      RowBox[{"(", 
       RowBox[{
        RowBox[{
         RowBox[{"-", "2"}], " ", "En"}], "+", 
        RowBox[{
         RowBox[{"(", 
          RowBox[{"3", "+", 
           RowBox[{"2", " ", "L"}]}], ")"}], " ", "\[Omega]", " ", 
         "\[HBar]"}]}], ")"}], " ", 
      RowBox[{"w", "[", "r", "]"}]}], "-", 
     RowBox[{"\[HBar]", " ", 
      RowBox[{"(", 
       RowBox[{
        RowBox[{"2", " ", 
         RowBox[{"(", 
          RowBox[{
           RowBox[{
            RowBox[{"-", "m"}], " ", 
            SuperscriptBox["r", "2"], " ", "\[Omega]"}], "+", "\[HBar]", "+", 
           
           RowBox[{"L", " ", "\[HBar]"}]}], ")"}], " ", 
         RowBox[{
          SuperscriptBox["w", "\[Prime]",
           MultilineFunction->None], "[", "r", "]"}]}], "+", 
        RowBox[{"r", " ", "\[HBar]", " ", 
         RowBox[{
          SuperscriptBox["w", "\[Prime]\[Prime]",
           MultilineFunction->None], "[", "r", "]"}]}]}], ")"}]}]}], ")"}]}], 
  
  SuperscriptBox["\[HBar]", "2"]]], "Output"]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell[TextData[StyleBox["\:5206\:5b50\:306e\:62ec\:5f27\:306e\:4e2d\:3092\:3068\
\:308b\:3068",
 FontSlant->"Italic"]], "Subsection"],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"eq1", "=", 
  RowBox[{
   RowBox[{"m", " ", "r", " ", 
    RowBox[{"(", 
     RowBox[{
      RowBox[{
       RowBox[{"-", "2"}], " ", "En"}], "+", 
      RowBox[{
       RowBox[{"(", 
        RowBox[{"3", "+", 
         RowBox[{"2", " ", "L"}]}], ")"}], " ", "\[Omega]", " ", 
       "\[HBar]"}]}], ")"}], " ", 
    RowBox[{"w", "[", "r", "]"}]}], "-", 
   RowBox[{"\[HBar]", " ", 
    RowBox[{"(", 
     RowBox[{
      RowBox[{"2", " ", 
       RowBox[{"(", 
        RowBox[{
         RowBox[{
          RowBox[{"-", "m"}], " ", 
          SuperscriptBox["r", "2"], " ", "\[Omega]"}], "+", "\[HBar]", "+", 
         RowBox[{"L", " ", "\[HBar]"}]}], ")"}], " ", 
       RowBox[{
        SuperscriptBox["w", "\[Prime]",
         MultilineFunction->None], "[", "r", "]"}]}], "+", 
      RowBox[{"r", " ", "\[HBar]", " ", 
       RowBox[{
        SuperscriptBox["w", "\[Prime]\[Prime]",
         MultilineFunction->None], "[", "r", "]"}]}]}], ")"}]}]}]}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"m", " ", "r", " ", 
   RowBox[{"(", 
    RowBox[{
     RowBox[{
      RowBox[{"-", "2"}], " ", "En"}], "+", 
     RowBox[{
      RowBox[{"(", 
       RowBox[{"3", "+", 
        RowBox[{"2", " ", "L"}]}], ")"}], " ", "\[Omega]", " ", "\[HBar]"}]}],
     ")"}], " ", 
   RowBox[{"w", "[", "r", "]"}]}], "-", 
  RowBox[{"\[HBar]", " ", 
   RowBox[{"(", 
    RowBox[{
     RowBox[{"2", " ", 
      RowBox[{"(", 
       RowBox[{
        RowBox[{
         RowBox[{"-", "m"}], " ", 
         SuperscriptBox["r", "2"], " ", "\[Omega]"}], "+", "\[HBar]", "+", 
        RowBox[{"L", " ", "\[HBar]"}]}], ")"}], " ", 
      RowBox[{
       SuperscriptBox["w", "\[Prime]",
        MultilineFunction->None], "[", "r", "]"}]}], "+", 
     RowBox[{"r", " ", "\[HBar]", " ", 
      RowBox[{
       SuperscriptBox["w", "\[Prime]\[Prime]",
        MultilineFunction->None], "[", "r", "]"}]}]}], ")"}]}]}]], "Output"]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell[TextData[StyleBox["\:3055\:3089\:306b\:5909\:6570\:5909\:63db\:3002\
Mathematica \:3067ODE\:3067\:5909\:6570\:5909\:63db\:3059\:308b\:306e\:306f\
\:6b21\:306e\:3088\:3046\:306b\:50d5\:306f\:3084\:308a\:307e\:3059\:3002\:307e\
\:305a\:3001\:65b0\:5909\:6570 \[Rho] \:3092\:65e7\:5909\:6570 r \:306e\:95a2\
\:6570\:3068\:3057\:3066\:304b\:3044\:3066\:3001\:9006\:95a2\:6570\:3092\:5b9a\
\:7fa9\:3002",
 FontSlant->"Italic"]], "Subsection"],

Cell[BoxData[
 RowBox[{
  RowBox[{"\[Rho]", "[", "r_", "]"}], ":=", " ", 
  RowBox[{"m", " ", 
   RowBox[{"\[Omega]", "/", "\[HBar]"}], " ", 
   RowBox[{"r", "^", "2"}]}]}]], "Input"],

Cell[BoxData[
 RowBox[{"rAsFunc", ":=", 
  RowBox[{"InverseFunction", "[", "\[Rho]", "]"}]}]], "Input"]
}, Open  ]],

Cell[CellGroupData[{

Cell[TextData[StyleBox["\:305d\:308c\:3092\:7528\:3044\:3066\:5f0f\:3092\:5909\
\:63db\:3057\:307e\:3059\:3002",
 FontSlant->"Italic"]], "Subsection"],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"(", 
   RowBox[{"eq1", "/.", "  ", 
    RowBox[{"{", 
     RowBox[{
      RowBox[{"w", "\[Rule]", " ", 
       RowBox[{"(", 
        RowBox[{
         RowBox[{"W", "[", 
          RowBox[{"\[Rho]", "[", "#", "]"}], "]"}], "&"}], ")"}]}], ",", 
      RowBox[{"r", "\[Rule]", " ", 
       RowBox[{"rAsFunc", "[", "\[Rho]", "]"}]}]}], "}"}]}], ")"}], "//", 
  "FullSimplify"}]], "Input"],

Cell[BoxData[
 RowBox[{
  StyleBox[
   RowBox[{"InverseFunction", "::", "ifun"}], "MessageName"], ":", 
  " ", "\<\"\:9006\:95a2\:6570\:304c\:4f7f\:7528\:3055\:308c\:3066\:3044\:307e\
\:3059\:ff0e\:5024\:306f\:591a\:4fa1\:9006\:95a2\:6570\:306e\:305f\:3081\:5931\
\:308f\:308c\:3066\:3044\:308b\:53ef\:80fd\:6027\:304c\:3042\:308a\:307e\:3059\
\:ff0e \\!\\(\\*ButtonBox[\\\"\[RightSkeleton]\\\", \
ButtonStyle->\\\"Link\\\", ButtonFrame->None, \
ButtonData:>\\\"paclet:ref/message/InverseFunction/ifun\\\", ButtonNote -> \\\
\"InverseFunction::ifun\\\"]\\)\"\>"}]], "Message", "MSG"],

Cell[BoxData[
 FractionBox[
  RowBox[{
   SqrtBox["m"], " ", 
   SqrtBox["\[Rho]"], " ", 
   SqrtBox["\[HBar]"], " ", 
   RowBox[{"(", 
    RowBox[{
     RowBox[{
      RowBox[{"(", 
       RowBox[{
        RowBox[{"2", " ", "En"}], "-", 
        RowBox[{
         RowBox[{"(", 
          RowBox[{"3", "+", 
           RowBox[{"2", " ", "L"}]}], ")"}], " ", "\[Omega]", " ", 
         "\[HBar]"}]}], ")"}], " ", 
      RowBox[{"W", "[", "\[Rho]", "]"}]}], "+", 
     RowBox[{"2", " ", "\[Omega]", " ", "\[HBar]", " ", 
      RowBox[{"(", 
       RowBox[{
        RowBox[{
         RowBox[{"(", 
          RowBox[{"3", "+", 
           RowBox[{"2", " ", "L"}], "-", 
           RowBox[{"2", " ", "\[Rho]"}]}], ")"}], " ", 
         RowBox[{
          SuperscriptBox["W", "\[Prime]",
           MultilineFunction->None], "[", "\[Rho]", "]"}]}], "+", 
        RowBox[{"2", " ", "\[Rho]", " ", 
         RowBox[{
          SuperscriptBox["W", "\[Prime]\[Prime]",
           MultilineFunction->None], "[", "\[Rho]", "]"}]}]}], ")"}]}]}], 
    ")"}]}], 
  SqrtBox["\[Omega]"]]], "Output"]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell[TextData[StyleBox["\:307e\:305f\:5206\:5b50\:3092\:53d6\:308a\:51fa\:3057\
\:3066\:3001\:3061\:3087\:3063\:3068\:308f\:308b\:3068\:3001Laguerre \:306e\
\:966a\:5fae\:5206\:65b9\:7a0b\:5f0f\:3067\:3042\:308b\:3053\:3068\:304c\:308f\
\:304b\:308a\:307e\:3059\:3002",
 FontSlant->"Italic"]], "Subsection"],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"eq2", "=", 
  RowBox[{
   RowBox[{
    RowBox[{
     RowBox[{"(", 
      RowBox[{
       RowBox[{"2", " ", "En"}], "-", 
       RowBox[{
        RowBox[{"(", 
         RowBox[{"3", "+", 
          RowBox[{"2", " ", "L"}]}], ")"}], " ", "\[Omega]", " ", 
        "\[HBar]"}]}], ")"}], " ", 
     RowBox[{"W", "[", "\[Rho]", "]"}]}], "+", 
    RowBox[{"2", " ", "\[Omega]", " ", "\[HBar]", " ", 
     RowBox[{
      RowBox[{"(", 
       RowBox[{
        RowBox[{
         RowBox[{"(", 
          RowBox[{"3", "+", 
           RowBox[{"2", " ", "L"}], "-", 
           RowBox[{"2", " ", "\[Rho]"}]}], ")"}], " ", 
         RowBox[{
          SuperscriptBox["W", "\[Prime]",
           MultilineFunction->None], "[", "\[Rho]", "]"}]}], "+", 
        RowBox[{"2", " ", "\[Rho]", " ", 
         RowBox[{
          SuperscriptBox["W", "\[Prime]\[Prime]",
           MultilineFunction->None], "[", "\[Rho]", "]"}]}]}], ")"}], "/", 
      RowBox[{"(", 
       RowBox[{"4", "\[Omega]", " ", "\[HBar]"}], ")"}]}]}]}], " ", "//", 
   "Simplify"}]}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"(", 
    RowBox[{
     RowBox[{"2", " ", "En"}], "-", 
     RowBox[{
      RowBox[{"(", 
       RowBox[{"3", "+", 
        RowBox[{"2", " ", "L"}]}], ")"}], " ", "\[Omega]", " ", "\[HBar]"}]}],
     ")"}], " ", 
   RowBox[{"W", "[", "\[Rho]", "]"}]}], "+", 
  RowBox[{
   RowBox[{"(", 
    RowBox[{
     FractionBox["3", "2"], "+", "L", "-", "\[Rho]"}], ")"}], " ", 
   RowBox[{
    SuperscriptBox["W", "\[Prime]",
     MultilineFunction->None], "[", "\[Rho]", "]"}]}], "+", 
  RowBox[{"\[Rho]", " ", 
   RowBox[{
    SuperscriptBox["W", "\[Prime]\[Prime]",
     MultilineFunction->None], "[", "\[Rho]", "]"}]}]}]], "Output"]
}, Open  ]]
}, Open  ]]
},
WindowSize->{808, 911},
Visible->True,
ScrollingOptions->{"VerticalScrollRange"->Fit},
ShowCellBracket->Automatic,
CellContext->Notebook,
TrackCellChangeTimes->False,
FrontEndVersion->"10.0 for Mac OS X x86 (32-bit, 64-bit Kernel) (2014\:5e749\
\:670811\:65e5)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[1486, 35, 84, 1, 44, "Subsection"],
Cell[CellGroupData[{
Cell[1595, 40, 981, 33, 46, "Input"],
Cell[2579, 75, 974, 32, 54, "Output"]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[3602, 113, 160, 4, 44, "Subsection"],
Cell[CellGroupData[{
Cell[3787, 121, 190, 6, 28, "Input"],
Cell[3980, 129, 2158, 65, 100, "Output"]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[6187, 200, 224, 3, 44, "Subsection"],
Cell[CellGroupData[{
Cell[6436, 207, 485, 15, 28, "Input"],
Cell[6924, 224, 1403, 44, 64, "Output"]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[8376, 274, 132, 2, 44, "Subsection"],
Cell[CellGroupData[{
Cell[8533, 280, 986, 30, 35, "Input"],
Cell[9522, 312, 932, 29, 35, "Output"]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[10503, 347, 442, 6, 69, "Subsection"],
Cell[10948, 355, 183, 5, 28, "Input"],
Cell[11134, 362, 103, 2, 28, "Input"]
}, Open  ]],
Cell[CellGroupData[{
Cell[11274, 369, 150, 2, 44, "Subsection"],
Cell[CellGroupData[{
Cell[11449, 375, 419, 13, 28, "Input"],
Cell[11871, 390, 583, 10, 24, "Message"],
Cell[12457, 402, 1082, 34, 56, "Output"]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[13588, 442, 307, 4, 69, "Subsection"],
Cell[CellGroupData[{
Cell[13920, 450, 1071, 32, 30, "Input"],
Cell[14994, 484, 672, 22, 48, "Output"]
}, Open  ]]
}, Open  ]]
}
]
*)

(* End of internal cache information *)

(* NotebookSignature 0wDcBvHhcxN3nB1gxecKWCVd *)
