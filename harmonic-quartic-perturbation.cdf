(* Content-type: application/vnd.wolfram.cdf.text *)

(*** Wolfram CDF File ***)
(* http://www.wolfram.com/cdf *)

(* CreatedBy='Mathematica 10.0' *)

(*************************************************************************)
(*                                                                       *)
(*  The Mathematica License under which this file was created prohibits  *)
(*  restricting third parties in receipt of this file from republishing  *)
(*  or redistributing it by any means, including but not limited to      *)
(*  rights management or terms of use, without the express consent of    *)
(*  Wolfram Research, Inc. For additional information concerning CDF     *)
(*  licensing and redistribution see:                                    *)
(*                                                                       *)
(*        www.wolfram.com/cdf/adopting-cdf/licensing-options.html        *)
(*                                                                       *)
(*************************************************************************)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[      1064,         20]
NotebookDataLength[    110115,       2619]
NotebookOptionsPosition[    106430,       2475]
NotebookOutlinePosition[    106888,       2495]
CellTagsIndexPosition[    106845,       2492]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell["\:6442\:52d5\:8ad6", "Section"],

Cell[CellGroupData[{

Cell["\<\
\:6f14\:7b97\:5b50\:3092\:30b1\:30c3\:30c8\:306b\:304b\:3051\:308b\:969b\:306e\
\:8a08\:7b97\:30eb\:30fc\:30eb\:3092 Mathematica \:306b\:6559\:3048\:308b\
\:3002\[LineSeparator]\tAct[{\:6f14\:7b97\:5b50,\:6f14\:7b97\:5b50, ... \
\:6f14\:7b97\:5b50}, \:30b1\:30c3\:30c8] \[LineSeparator]\:3068\:66f8\:304f\
\:3053\:3068\:306b\:3059\:308b\:3002\
\>", "Subsection"],

Cell["\<\
\:307e\:305a\:306f\:7dda\:5f62\:6027\:3002\:5de6\:8fba\:306e _ \:4e00\:500b\
\:306f\:5909\:6570\:540d\:3001_ \:307f\:3063\:3064\:306f\:3001\:30ea\:30b9\
\:30c8\:5185\:306e0\:500b\:4ee5\:4e0a\:306e\:8981\:7d20\:3092\:6307\:3059\
\:3002\
\>", "Text"],

Cell[BoxData[
 RowBox[{
  RowBox[{"Act", "[", 
   RowBox[{
    RowBox[{"{", 
     RowBox[{"c___", ",", 
      RowBox[{"a_", "+", "b_"}]}], "}"}], ",", "psi_"}], "]"}], ":=", 
  RowBox[{
   RowBox[{"Act", "[", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{"c", ",", "a"}], "}"}], ",", "psi"}], "]"}], "+", 
   RowBox[{"Act", "[", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{"c", ",", "b"}], "}"}], ",", "psi"}], "]"}]}]}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"Act", "[", 
   RowBox[{"a_", ",", 
    RowBox[{"psi_", "+", "phi_"}]}], "]"}], ":=", 
  RowBox[{
   RowBox[{"Act", "[", 
    RowBox[{"a", ",", "psi"}], "]"}], "+", 
   RowBox[{"Act", "[", 
    RowBox[{"a", ",", "phi"}], "]"}]}]}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"Act", "[", 
   RowBox[{"a_", ",", 
    RowBox[{"c_", " ", 
     RowBox[{"Ket", "[", "n_", "]"}]}]}], "]"}], " ", ":=", " ", 
  RowBox[{"c", " ", 
   RowBox[{"Act", "[", 
    RowBox[{"a", " ", ",", 
     RowBox[{"Ket", "[", "n", "]"}]}], "]"}]}]}]], "Input"],

Cell["A \:3068 AD \
\:304c\:6d88\:6ec5\:6f14\:7b97\:5b50\:3068\:751f\:6210\:6f14\:7b97\:5b50\:3002\
", "Text"],

Cell[BoxData[
 RowBox[{
  RowBox[{"Act", "[", 
   RowBox[{
    RowBox[{"{", 
     RowBox[{"c___", ",", "A"}], "}"}], ",", 
    RowBox[{"Ket", "[", "n_", "]"}]}], "]"}], ":=", 
  RowBox[{
   RowBox[{"Sqrt", "[", "n", "]"}], " ", 
   RowBox[{"Act", "[", 
    RowBox[{
     RowBox[{"{", "c", "}"}], ",", 
     RowBox[{"Ket", "[", 
      RowBox[{"n", "-", "1"}], "]"}]}], "]"}]}]}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"Act", "[", 
   RowBox[{
    RowBox[{"{", 
     RowBox[{"c___", ",", "AD"}], "}"}], ",", 
    RowBox[{"Ket", "[", "n_", "]"}]}], "]"}], ":=", 
  RowBox[{
   RowBox[{"Sqrt", "[", 
    RowBox[{"n", "+", "1"}], "]"}], 
   RowBox[{"Act", "[", 
    RowBox[{
     RowBox[{"{", "c", "}"}], ",", 
     RowBox[{"Ket", "[", 
      RowBox[{"n", "+", "1"}], "]"}]}], "]"}]}]}]], "Input"],

Cell["\:304b\:3089\:3063\:307d\:306e\:6f14\:7b97\:5b50\:306f\:306a\:306b\:3082\
\:3057\:306a\:3044\:3002", "Text"],

Cell[BoxData[
 RowBox[{
  RowBox[{"Act", "[", 
   RowBox[{
    RowBox[{"{", "}"}], ",", "psi_"}], "]"}], ":=", "psi"}]], "Input"],

Cell[TextData[{
 "\:3053\:308c\:3067\:6442\:52d5\:306b\:7528\:3044\:308b ",
 Cell[BoxData[
  FormBox[
   SuperscriptBox["x", "4"], TraditionalForm]]],
 " \:304c\:5b9a\:7fa9\:51fa\:6765\:308b\:3002"
}], "Text"],

Cell[BoxData[
 RowBox[{
  RowBox[{"V", "[", "psi_", "]"}], ":=", 
  RowBox[{
   RowBox[{"Act", "[", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{
       RowBox[{"A", "+", "AD"}], ",", 
       RowBox[{"A", "+", "AD"}], ",", 
       RowBox[{"A", "+", "AD"}], ",", 
       RowBox[{"A", "+", "AD"}]}], "}"}], ",", "psi"}], "]"}], "/", 
   "4"}]}]], "Input"],

Cell["\:78ba\:8a8d\:3057\:3088\:3046:", "Text"],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"V", "[", 
   RowBox[{"Ket", "[", "0", "]"}], "]"}], "//", "Expand"}]], "Input"],

Cell[BoxData[
 RowBox[{
  FractionBox[
   RowBox[{"3", " ", 
    TemplateBox[{"0"},
     "Ket"]}], "4"], "+", 
  FractionBox[
   RowBox[{"3", " ", 
    TemplateBox[{"2"},
     "Ket"]}], 
   SqrtBox["2"]], "+", 
  RowBox[{
   SqrtBox[
    FractionBox["3", "2"]], " ", 
   TemplateBox[{"4"},
    "Ket"]}]}]], "Output"]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell["\:540c\:69d8\:306b\:3001\:30b1\:30c3\:30c8\:306e |0\[RightAngleBracket] \
\:306e\:4fc2\:6570\:3092\:63a1\:308a\:3060\:3059\:64cd\:4f5c\:3092\:5b9a\:7fa9\
\:3002 0 \:30d6\:30e9\:3068\:306e\:5185\:7a4d\:3002", "Subsection"],

Cell[BoxData[
 RowBox[{
  RowBox[{"Proj", "[", 
   RowBox[{"psi_", "+", "phi_"}], "]"}], ":=", 
  RowBox[{
   RowBox[{"Proj", "[", "psi", "]"}], "+", 
   RowBox[{"Proj", "[", "phi", "]"}]}]}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"Proj", "[", 
   RowBox[{"c_", " ", 
    RowBox[{"Ket", "[", "n_", "]"}]}], "]"}], ":=", 
  RowBox[{"c", " ", 
   RowBox[{"Proj", "[", 
    RowBox[{"Ket", "[", "n", "]"}], "]"}]}]}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"Proj", "[", 
   RowBox[{"Ket", "[", "n_", "]"}], "]"}], ":=", 
  RowBox[{"If", "[", 
   RowBox[{
    RowBox[{"n", "\[Equal]", "0"}], ",", "1", ",", "0"}], "]"}]}]], "Input"]
}, Open  ]],

Cell[CellGroupData[{

Cell[TextData[{
 Cell[BoxData[
  FormBox[
   RowBox[{"\:540c\:69d8\:306b", "\:3001", 
    SubscriptBox["R", "0"]}], TraditionalForm]]],
 "(g) \:3092\:5b9a\:7fa9\:3002order \:306f\:3001g \:3092\:4f55\:6b21\:307e\
\:3067\:5c55\:958b\:3059\:308b\:304b\:3002"
}], "Subsection"],

Cell["\:3053\:3053\:306f\:5358\:306b\:7dda\:5f62\:6027\:3002", "Text"],

Cell[BoxData[
 RowBox[{
  RowBox[{"R", "[", 
   RowBox[{"order_", ",", 
    RowBox[{"psi_", "+", "phi_"}]}], "]"}], ":=", 
  RowBox[{
   RowBox[{"R", "[", 
    RowBox[{"order", ",", "psi"}], "]"}], "+", " ", 
   RowBox[{"R", "[", 
    RowBox[{"order", ",", "phi"}], "]"}]}]}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"R", "[", 
   RowBox[{"order_", ",", 
    RowBox[{"c_", " ", 
     RowBox[{"Ket", "[", "n_", "]"}]}]}], "]"}], ":=", 
  RowBox[{"c", " ", 
   RowBox[{"R", "[", 
    RowBox[{"order", ",", 
     RowBox[{"Ket", "[", "n", "]"}]}], "]"}]}]}]], "Input"],

Cell[TextData[{
 Cell[BoxData[
  FormBox[
   RowBox[{
    SubscriptBox["R", "0"], "(", "g", ")"}], TraditionalForm]]],
 " \:306e ",
 Cell[BoxData[
  TemplateBox[{"n"},
   "Ket"]], "Output",
  CellChangeTimes->{3.642136973895424*^9}],
 "\:3078\:306e\:4f5c\:7528\:306f\:3001n \:304c 0 \:306a\:3089\:6d88\:3059\
\:3001 n \:304c\:975e\:30bc\:30ed\:306a\:3089 ",
 Cell[BoxData[
  FormBox[
   RowBox[{"(", 
    SubscriptBox["E", "0"]}], TraditionalForm]]],
 "(g)-(n+1/2)) \:3067\:5272\:308b\:3002"
}], "Text"],

Cell[BoxData[
 RowBox[{
  RowBox[{"R", "[", 
   RowBox[{"order_", ",", 
    RowBox[{"Ket", "[", "n_", "]"}]}], "]"}], ":=", 
  RowBox[{"If", "[", 
   RowBox[{
    RowBox[{"n", "\[Equal]", "0"}], ",", "0", ",", 
    RowBox[{
     RowBox[{"Ket", "[", "n", "]"}], "/", 
     RowBox[{"(", 
      RowBox[{
       RowBox[{"Energy", "[", "order", "]"}], "-", 
       RowBox[{"(", 
        RowBox[{"n", "+", 
         RowBox[{"1", "/", "2"}]}], ")"}]}], ")"}]}]}], "]"}]}]], "Input"]
}, Open  ]],

Cell[CellGroupData[{

Cell["\<\
Brillouin-Wigner \:6442\:52d5\:8ad6\:306e\:5f0f\:306f\:3001state \:3068 \
energy \:304c\:3042\:308b order \:307e\:3067\:308f\:304b\:308c\:3070\:3001\
\:305d\:308c\:3092\:53f3\:8fba\:306b\:4ee3\:5165\:3059\:308b\:3068\:6b21\:306e\
 order+1 \:304c\:308f\:304b\:308b\:5f62\:3092\:3057\:3066\:3044\:308b\:3002\
\:305d\:308c\:3092\:5358\:306b\:66f8\:304f\:3002\
\>", "Subsection"],

Cell[TextData[{
 "\:6700\:4f4e\:57fa\:5e95\:72b6\:614b ",
 Cell[BoxData[
  TemplateBox[{RowBox[{
      SubscriptBox["\[Psi]", "0"], 
      RowBox[{"(", "g", ")"}]}]},
   "Ket"]],
  CellChangeTimes->{{3.642137068522334*^9, 3.64213707699266*^9}, 
    3.6421371133960323`*^9}],
 " \:3092\:4e0e\:3048\:3089\:308c\:305f order \:307e\:3067\:5c55\:958b\:3057\
\:305f\:3082\:306e\:3092 State[order], ",
 Cell[BoxData[
  FormBox[
   RowBox[{
    RowBox[{
     SubscriptBox["E", "0"], "(", "g", ")"}], " "}], TraditionalForm]]],
 "\:3092\:540c\:69d8\:306b\:4e0e\:3048\:3089\:308c\:305f order \:307e\:3067\
\:5c55\:958b\:3057\:305f\:3082\:306e\:3092 Energy[order] \:3068\:66f8\:304f\
\:3053\:3068\:306b\:3059\:308b\:3002"
}], "Text"],

Cell["\:30bc\:30ed\:6b21\:306e\:7b54\:3048\:306f\:77e5\:3063\:3066\:3044\:308b\
\:306e\:3067\:3001\:5165\:529b:", "Text"],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"State", "[", "0", "]"}], "=", 
  RowBox[{"Ket", "[", "0", "]"}]}]], "Input"],

Cell[BoxData[
 TemplateBox[{"0"},
  "Ket"]], "Output"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{
  RowBox[{"Energy", "[", "0", "]"}], "=", 
  RowBox[{"1", "/", "2"}]}]], "Input"],

Cell[BoxData[
 FractionBox["1", "2"]], "Output"]
}, Open  ]],

Cell[TextData[{
 "VState \:306f\:3001V",
 Cell[BoxData[
  TemplateBox[{RowBox[{
      SubscriptBox["\[Psi]", "0"], 
      RowBox[{"(", "g", ")"}]}]},
   "Ket"]],
  CellChangeTimes->{{3.642137068522334*^9, 3.64213707699266*^9}, 
    3.6421371133960323`*^9}],
 " \:304c \:30a8\:30cd\:30eb\:30ae\:30fc\:306e\:5f0f\:306b\:3082\:72b6\:614b\
\:306e\:5f0f\:306b\:3082\:51fa\:3066\:304f\:308b\:306e\:3067\:8a08\:7b97\:306e\
\:30b9\:30d4\:30fc\:30c9\:30a2\:30c3\:30d7\:306e\:305f\:3081\:306b\:5b9a\:7fa9\
\:3057\:305f\:3002"
}], "Text"],

Cell[TextData[{
 "f[x_]:=f[x]= \:307b\:3052\:307b\:3052\:3000\:3068\:3044\:3046\:306e\:306f\
\:3001f[x] \:3092\:4e00\:5ea6\:8a08\:7b97\:3057\:305f\:7d50\:679c\:3092\:899a\
\:3048\:3066\:304a\:304f ",
 StyleBox["Mathematica",
  FontSlant->"Italic"],
 " \:306e idiom. "
}], "Text"],

Cell[BoxData[
 RowBox[{
  RowBox[{"VState", "[", "order_", "]"}], ":=", 
  RowBox[{
   RowBox[{"VState", "[", "order", "]"}], "=", 
   RowBox[{
    RowBox[{"V", "[", 
     RowBox[{
      RowBox[{
       RowBox[{"State", "[", "order", "]"}], "//", "Normal"}], "//", 
      "Expand"}], "]"}], "//", "Expand"}]}]}]], "Input"],

Cell["\:30ce\:30fc\:30c8\:306e(6.39)\:5f0f\:3002", "Text"],

Cell[BoxData[
 RowBox[{
  RowBox[{"Energy", "[", "order_", "]"}], ":=", 
  RowBox[{
   RowBox[{"Energy", "[", "order", "]"}], "=", 
   RowBox[{
    RowBox[{
     RowBox[{"Energy", "[", "0", "]"}], "+", 
     RowBox[{"g", " ", 
      RowBox[{"Proj", "[", " ", 
       RowBox[{"VState", "[", 
        RowBox[{"order", "-", "1"}], "]"}], "]"}]}], "+", " ", 
     RowBox[{
      RowBox[{"O", "[", "g", "]"}], "^", 
      RowBox[{"(", 
       RowBox[{"order", "+", "1"}], ")"}]}]}], "//", "Normal"}]}]}]], "Input"],

Cell["\:30ce\:30fc\:30c8\:306e(6.43)\:5f0f\:3002", "Text"],

Cell[BoxData[
 RowBox[{
  RowBox[{"State", "[", "order_", "]"}], ":=", 
  RowBox[{
   RowBox[{"State", "[", "order", "]"}], "=", 
   RowBox[{
    RowBox[{
     RowBox[{"State", "[", "0", "]"}], "+", " ", 
     RowBox[{"g", " ", 
      RowBox[{"R", "[", 
       RowBox[{
        RowBox[{"order", "-", "1"}], ",", 
        RowBox[{"VState", "[", 
         RowBox[{"order", "-", "1"}], "]"}]}], "]"}]}], " ", "+", " ", 
     RowBox[{
      RowBox[{"O", "[", "g", "]"}], "^", 
      RowBox[{"(", 
       RowBox[{"order", "+", "1"}], ")"}]}]}], "//", "Normal"}]}]}]], "Input"]
}, Open  ]],

Cell[CellGroupData[{

Cell["\:3053\:308c\:3067\:6e96\:5099\:304c\:304a\:308f\:308a\:300220\:6b21\
\:307e\:3067\:8a08\:7b97\:3057\:3088\:3046: ", "Subsection"],

Cell["\:3046\:3061\:3067\:306f30\:79d2\:3050\:3089\:3044\:304b\:304b\:3063\
\:305f\:3002", "Text"],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Energy", "[", "20", "]"}]], "Input"],

Cell[BoxData[
 RowBox[{
  FractionBox["1", "2"], "+", 
  FractionBox[
   RowBox[{"3", " ", "g"}], "4"], "-", 
  FractionBox[
   RowBox[{"21", " ", 
    SuperscriptBox["g", "2"]}], "8"], "+", 
  FractionBox[
   RowBox[{"333", " ", 
    SuperscriptBox["g", "3"]}], "16"], "-", 
  FractionBox[
   RowBox[{"30885", " ", 
    SuperscriptBox["g", "4"]}], "128"], "+", 
  FractionBox[
   RowBox[{"916731", " ", 
    SuperscriptBox["g", "5"]}], "256"], "-", 
  FractionBox[
   RowBox[{"65518401", " ", 
    SuperscriptBox["g", "6"]}], "1024"], "+", 
  FractionBox[
   RowBox[{"2723294673", " ", 
    SuperscriptBox["g", "7"]}], "2048"], "-", 
  FractionBox[
   RowBox[{"1030495099053", " ", 
    SuperscriptBox["g", "8"]}], "32768"], "+", 
  FractionBox[
   RowBox[{"54626982511455", " ", 
    SuperscriptBox["g", "9"]}], "65536"], "-", 
  FractionBox[
   RowBox[{"6417007431590595", " ", 
    SuperscriptBox["g", "10"]}], "262144"], "+", 
  FractionBox[
   RowBox[{"413837985580636167", " ", 
    SuperscriptBox["g", "11"]}], "524288"], "-", 
  FractionBox[
   RowBox[{"116344863173284543665", " ", 
    SuperscriptBox["g", "12"]}], "4194304"], "+", 
  FractionBox[
   RowBox[{"8855406003085477228503", " ", 
    SuperscriptBox["g", "13"]}], "8388608"], "-", 
  FractionBox[
   RowBox[{"1451836748576538293163705", " ", 
    SuperscriptBox["g", "14"]}], "33554432"], "+", 
  FractionBox[
   RowBox[{"127561682802713500067360049", " ", 
    SuperscriptBox["g", "15"]}], "67108864"], "-", 
  FractionBox[
   RowBox[{"191385927852560927887828084605", " ", 
    SuperscriptBox["g", "16"]}], "2147483648"], "+", 
  FractionBox[
   RowBox[{"19080610783320698048964226601511", " ", 
    SuperscriptBox["g", "17"]}], "4294967296"], "-", 
  FractionBox[
   RowBox[{"4031194983593309788607032686292335", " ", 
    SuperscriptBox["g", "18"]}], "17179869184"], "+", 
  FractionBox[
   RowBox[{"449820604540765836160529697491458635", " ", 
    SuperscriptBox["g", "19"]}], "34359738368"], "-", 
  FractionBox[
   RowBox[{"211491057584560795425148309663914344715", " ", 
    SuperscriptBox["g", "20"]}], "274877906944"]}]], "Output"]
}, Open  ]]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell["\:6570\:5024\:8a08\:7b97", "Section"],

Cell[CellGroupData[{

Cell["\:540c\:3058\:7cfb\:3092\:6570\:5024\:7684\:306b\:3057\:3089\:3079\:3088\
\:3046\:3002\:30a8\:30cd\:30eb\:30ae\:30fc\:304c\:5224\:3063\:3066\:3044\:308c\
\:3070\:3001\:6700\:4f4e\:57fa\:5e95\:72b6\:614b\:306f\:30b7\:30e5\:30ec\:30fc\
\:30c7\:30a3\:30f3\:30ac\:30fc\:65b9\:7a0b\:5f0f\:3092\:89e3\:3051\:3070\:826f\
\:3044\:3002", "Subsection"],

Cell["\<\
\:307e\:305a\:306f\:3001\:8a66\:884c\:30a8\:30cd\:30eb\:30ae\:30fc\:3068\:6442\
\:52d5\:306e\:5927\:304d\:3055 g \:304c\:4e0e\:3048\:3089\:308c\:305f\:3068\
\:304d\:306b\:3001x=0 \:304b\:3089\:6b63\:306e\:65b9\:5411\:3078\:89e3\:304b\
\:305b\:308b\:88dc\:52a9\:30eb\:30fc\:30c1\:30f3\:3092\:3064\:304f\:308b\:3002\
\:305f\:3060\:3057\:3044\:30a8\:30cd\:30eb\:30ae\:30fc\:3067\:306a\:3044\:3068\
\:3001\:3059\:3050\:306b\:6ce2\:52d5\:95a2\:6570\:306e\:5024\:304c\:7206\:767a\
\:3059\:308b\:306e\:3067\:3001\:305d\:3053\:3067\:306e\:5024\:3068\:7206\:767a\
\:3057\:305f x \:3092 endval, endx \:306b\:8a18\:9332\:3055\:305b\:308b\:3053\
\:3068\:306b\:3059\:308b\:3002\
\>", "Text"],

Cell[BoxData[
 RowBox[{
  RowBox[{"solve", "[", 
   RowBox[{"g_", ",", "En_"}], "]"}], ":=", 
  RowBox[{
   RowBox[{"NDSolve", "[", 
    RowBox[{
     RowBox[{"{", "\[IndentingNewLine]", 
      RowBox[{
       RowBox[{
        RowBox[{
         RowBox[{
          RowBox[{"-", " ", 
           RowBox[{"D", "[", " ", 
            RowBox[{
             RowBox[{"F", "[", "x", "]"}], ",", 
             RowBox[{"{", 
              RowBox[{"x", ",", "2"}], "}"}]}], "]"}]}], "/", "2"}], " ", "+", 
         RowBox[{
          RowBox[{"(", 
           RowBox[{
            RowBox[{
             RowBox[{"x", "^", "2"}], "/", "2"}], "+", " ", 
            RowBox[{"g", " ", 
             RowBox[{"x", "^", "4"}]}], "-", " ", "En"}], ")"}], " ", 
          RowBox[{"F", "[", "x", "]"}]}]}], "\[Equal]", "0"}], ",", 
       "\[IndentingNewLine]", 
       RowBox[{
        RowBox[{"F", "[", "0", "]"}], "\[Equal]", "1"}], ",", 
       RowBox[{
        RowBox[{
         RowBox[{"F", "'"}], "[", "0", "]"}], "\[Equal]", "0"}], ",", 
       RowBox[{"WhenEvent", "[", 
        RowBox[{
         RowBox[{
          RowBox[{"Abs", "[", 
           RowBox[{"F", "[", "x", "]"}], "]"}], ">", 
          RowBox[{"10", "^", "3"}]}], " ", ",", 
         RowBox[{
          RowBox[{"endval", "=", 
           RowBox[{"F", "[", "x", "]"}]}], ";", 
          RowBox[{"endx", "=", "x"}], ";", "\"\<StopIntegration\>\""}]}], 
        "]"}]}], "}"}], ",", "\[IndentingNewLine]", 
     RowBox[{"F", "[", "x", "]"}], ",", 
     RowBox[{"{", 
      RowBox[{"x", ",", "0", ",", "100"}], "}"}]}], "]"}], 
   "\[LeftDoubleBracket]", "1", "\[RightDoubleBracket]"}]}]], "Input"],

Cell["\<\
g \:304c\:4e0e\:3048\:3089\:308c\:305f\:3068\:304d\:306e\:57fa\:5e95\:72b6\
\:614b\:306e\:30a8\:30cd\:30eb\:30ae\:30fc\:306f\:3001E=.7 \:3042\:305f\:308a\
\:304b\:3089\:8a66\:884c\:3092\:306f\:3058\:3081\:308b\:3002\:306f\:3058\:3081\
\:306f\:6ce2\:52d5\:95a2\:6570\:304c\:4e0b\:306b\:767a\:6563\:3059\:308b\:306e\
\:3067\:3001E \:3092 delta \:305a\:3064\:3055\:3052\:3066\:3001\:6ce2\:52d5\
\:95a2\:6570\:304c\:4e0a\:306b\:767a\:6563\:3059\:308b\:307e\:3067\:52d5\:304b\
\:3059\:3002\:4e0a\:306b\:767a\:6563\:3057\:305f\:3089\:3001delta \
\:306e\:523b\:307f\:3092\:5c0f\:3055\:304f\:3057\:3066\:3001E \:3092 delta \
\:305a\:3064\:3042\:3052\:3066\:3001\:6ce2\:52d5\:95a2\:6570\:304c\:4e0b\:306b\
\:767a\:6563\:3059\:308b\:307e\:3067\:52d5\:304b\:3059\:3002\:3053\:308c\:3092\
\:3001delta \
\:306e\:523b\:307f\:304c\:5341\:5206\:3061\:3044\:3055\:304f\:306a\:308b\:307e\
\:3067\:7e70\:308a\:66ff\:3048\:3059\:3002\
\>", "Text"],

Cell[BoxData[
 RowBox[{
  RowBox[{"VacuumEnergy", "[", "g_", "]"}], ":=", 
  RowBox[{
   RowBox[{"VacuumEnergy", "[", "g", "]"}], "=", 
   RowBox[{"(", 
    RowBox[{
     RowBox[{"En", "=", ".7"}], ";", 
     RowBox[{"delta", "=", 
      RowBox[{"-", ".01"}]}], ";", 
     RowBox[{"targetsign", "=", "1"}], ";", 
     RowBox[{"endval", "=", 
      RowBox[{"10", "^", "3"}]}], ";", 
     RowBox[{"While", "[", 
      RowBox[{
       RowBox[{
        RowBox[{"Abs", "[", "delta", "]"}], "\[GreaterEqual]", " ", 
        ".0000001"}], ",", 
       RowBox[{
        RowBox[{"solve", "[", 
         RowBox[{"g", ",", "En"}], "]"}], ";", 
        RowBox[{"(*", 
         RowBox[{
          RowBox[{"Print", "[", 
           RowBox[{
           "\"\<Energy:\>\"", ",", "En", ",", "\"\<end x:\>\"", ",", "endx", 
            ",", "\"\<endval:\>\"", ",", "endval"}], "]"}], ";"}], "*)"}], 
        RowBox[{"If", "[", 
         RowBox[{
          RowBox[{
           RowBox[{"endval", " ", "targetsign"}], " ", ">", "0"}], " ", ",", 
          " ", 
          RowBox[{
           RowBox[{"delta", " ", "=", " ", 
            RowBox[{"delta", " ", "*", " ", 
             RowBox[{"-", "0.1"}]}]}], ";", 
           RowBox[{"targetsign", "=", 
            RowBox[{"-", "targetsign"}]}]}]}], "]"}], ";", 
        RowBox[{"En", "+=", "delta"}]}]}], "]"}], ";", "En"}], 
    ")"}]}]}]], "Input"],

Cell["\:63a2\:7d22\:304c\:3046\:307e\:304f\:3044\:3063\:3066\:3044\:308b\:3053\
\:3068\:306e\:78ba\:8a8d\:306e\:305f\:3081\:306b En, endx, endval \
\:3092\:8868\:793a\:3055\:305b\:3066\:304a\:304f:", "Text"],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Dynamic", "[", "En", "]"}]], "Input"],

Cell[BoxData[
 DynamicBox[ToBoxes[$CellContext`En, StandardForm],
  ImageSizeCache->{16., {0., 8.}}]], "Output"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Dynamic", "[", "endx", "]"}]], "Input"],

Cell[BoxData[
 DynamicBox[ToBoxes[$CellContext`endx, StandardForm],
  ImageSizeCache->{31., {1., 9.}}]], "Output"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Dynamic", "[", "endval", "]"}]], "Input"],

Cell[BoxData[
 DynamicBox[ToBoxes[$CellContext`endval, StandardForm],
  ImageSizeCache->{47., {1., 9.}}]], "Output"]
}, Open  ]],

Cell["\:8a66\:3057\:3066\:307f\:3088\:3046:", "Text"],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"VacuumEnergy", "[", ".4", "]"}]], "Input"],

Cell[BoxData["0.668772589999999`"], "Output"]
}, Open  ]],

Cell["\:3061\:3083\:3093\:3068\:52d5\:3044\:3066\:3044\:308b\:3088\:3046\:3060\
\:3002", "Text"]
}, Open  ]],

Cell[CellGroupData[{

Cell["\:6442\:52d5\:8a08\:7b97\:3068\:6bd4\:8f03\:3057\:3066\:30d7\:30ed\:30c3\
\:30c8\:3057\:3066\:307f\:308b:", "Subsection"],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Plot", "[", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{
     RowBox[{"VacuumEnergy", "[", "g", "]"}], ",", 
     RowBox[{"Energy", "[", "1", "]"}], ",", 
     RowBox[{"Energy", "[", "2", "]"}], ",", 
     RowBox[{"Energy", "[", "3", "]"}], ",", 
     RowBox[{"Energy", "[", "4", "]"}], ",", 
     RowBox[{"Energy", "[", "5", "]"}]}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"g", ",", "0", ",", "0.2"}], "}"}], ",", 
   RowBox[{"PlotLegends", "\[Rule]", " ", 
    RowBox[{"{", 
     RowBox[{
     "\"\<numerical\>\"", ",", "\"\<1st\>\"", ",", "\"\<2nd\>\"", ",", 
      "\"\<3rd\>\"", ",", "\"\<4th\>\"", ",", "\"\<5th\>\""}], "}"}]}]}], 
  "]"}]], "Input"],

Cell[BoxData[
 TemplateBox[{GraphicsBox[{{{}, {}, {
       Directive[
        Opacity[1.], 
        RGBColor[0.368417, 0.506779, 0.709798], 
        AbsoluteThickness[1.6]], 
       LineBox[CompressedData["
1:eJwVzntUjAkABfCmGq2oXWOYU3qqTaJOiTyimx5CR3pRajvNapMzzij0oHYq
S4+J1bBREaKHpJeiSEwv5aPUEqlm9D02MRslipXVfvvHPff8zrl/XNOdkb7h
6mpqalvY/N+XVbejO9PsnE6scp2Ynh5E6o+8m9X8H+B/8Pp+oRoJ3tq4oky+
CSL8m1SNrAPbrxom8m0Rusiu6DCHxNTslh0xfGdUGXIOqWuQ6Kzs+hTF98Z5
0vSA+gwSDpznbXv5QmQIPSK/zCKhWryjQsyPQlqAj6hDQEIc27Pl96ZkFD18
wVPZkgjVyXzCHUrHhFNyTpsHiXL1zXRXpAy0uUtjeQiJxqDF78taspDxp7bm
62gSMh2e1rnOHISOZf1slEEiJq93mZ7Neahermq7lE9i1PqSNv9uPvSqRJ82
1pJQdwyeNrt3GY/rXnXrPyRhUqsxnaAqxL5PFS+XD5LIe2n7m+J9MYyKU20y
x0kEX6wSckdLMC8w1Xg1l4L5Y6PNsYJrOJqc0mquR8GzdE7dqH45PMILTaKW
UJB8HLe5vqkS2cvnu086UTi0Z+5hw7oqOKY5fv/Mh8LOmsiyV7XX4XM4rN06
jIL3MlelX0c1POTUyfoYCm+1+ra6d9VgXUjeYGcqhUz+q/b+4RsIzqmS6uZS
8BPnif2nbmK2rZFdQymFNoN523y/1qJnwT2J5h0KThpcl4+8W0h8oK840EEh
3LkuSl9wG1El/hbpCgrlBK/TfH49uj9YBxmOUJiSDtk8tbqDZ2aLtj39SuFz
fvwTrnUDHpWnTK6bTePLu9QEhdtdnNRuT2oxoGEoXeubHXAPxcEb6I6lNMYO
3RA9D5DD9PEfYbmONP7q9mlVODRCykl7G+9Jw1JmFS271YiCMv+3FkE0JBO9
a2pXNmGjjd7M8N00/CedNSuamrDabfJIdRyNIwuPvYtwacbxiUX2z1NoLFfJ
Bj50NiOwoHCv5mka65Rxb9Z7t0D0rfqubgGNgFh7XfXuFoyt6DGpqaLxobR0
RVZQK8ZHmrVC5DSGtTUSXw+0grvrhHKkg0bIDAGhu/s+LE/e7pIMsH+vTFtd
eX8ffbwI+uobGi3RRDlnfxu+5Cv87k/SMFd62dv824Ycmyw3pSYDZdTNPoOk
dvwqpHa9mcOguHTsjORbOzbZ5+mdNWIwnjS8vSr9AQjxo4MuSxmcSZavLJpJ
QOJbxhlexSC0z2zm0CwC15Yk7zFezcByWXq/uS6BEuPCiwGs6xlvSSGPQL7W
iNYD1oMb6OaCBQQyehNelKxh9zpcr8vWBD6XOeqK1jK4k7v5l4s+BH6aO8L5
GwyOjlesGPQjYL5QqGbmzMDLc66W8XYCr217vgWxJr8OXL0QRCDGq2HqIWtN
oXj0fBgBn0KFvHQ9u7eQxefFEKBEYp7IlYEg6aOnIo7Aviy7gUusyd5AQ4N4
Ag4FRX19rPdLTRvPJRKQDijzN7kxyB6p5p5LI6AWceEfS3cGQndBb7+UwCnZ
zmYha6sLCSX6xwksrLc4lsO6Yaub51kZAWedSoPvNjBIKSkx6D9FoMfhwBBY
e3F03umdJhAqXFkRx1oQtE++I5vAuHQqtpI1Wf1MlptLILVGjmHW/wEpEWKy

        "]]}, {
       Directive[
        Opacity[1.], 
        RGBColor[0.880722, 0.611041, 0.142051], 
        AbsoluteThickness[1.6]], 
       LineBox[CompressedData["
1:eJwV0ms01HkcBvBBIkUuU5YdpdKmsWal227Jo6I9sQaxSHVyVCenbZDkzpRN
UTZTR81aQlZuJeUu99tMvzIpKuu2/Wfmr9DmltpC2f+++J7nfM55Xjwvviv8
AvccUWWxWM7M/Z/ZI9UhsvPrbHfW5KiwWBTOrdYvL2HrQmBrFe3LWN8m7GYy
2xTippoPjYy9pQUmsWwrvJF2vjmjQmFmUcveU2w7XHnGeqGmRkFW3PFvENsV
irf7CzXmU9ik8kISwPbFGdMl7osXURhZu/eOgB2Ehvj4nGVfURCEPnP+rek0
DsUM59lbUziondypPpgA86IwvWJHCkWqjoqOQBF0a51fZvhRaPRZO3G7JQWm
H3O1r0VTEGnra6TJfofbcW720xQKp9K7rY1413FLe52YXUxhzPKGFrsuC2su
Ccf7H1BQ3bpvblV9Nlp3fSwepiiYVqjNRY3k4MK3Ux2fpymk/20V1z+Ri0BH
Y09PAzn2Zd71VR/LR/iPCXYbLOQwe7zMMdTwFgpPRFw0cpDDqVCvcsy4CLNd
KTY7D8gRMzXJu7e7GKGH1vNDwuSI+MXgjEnlXRh0GrVyL8vhVxp4+1XFPTwN
52atLpDD1XrngHt7Cap+qu9f0yLHW40eF4eOUtR7RwsD+uRIZr+S9r4uw9vL
dXG17+VwF6QLPGbKYVe4ayR5sQISzpKf98xWoGT6U2XeGgVs1dR3TOlXgR9u
9UqyXYEjdpVBxobV0OJdjuT6KFBE9GVmS+9jqPmHkwbBCswkDvK6uDUYP3uk
XS1JgY9ZkZ3qlrUwFNZc/D5HgenRc1H99nXwz025FVKvgEmizR6xVz36bpd/
t+kvBcYjyo698GpAwBuWsc24AvQTt9b+TY2w8Oo4vFtLCXMRN0RU1QjNGemS
tJVKxLzv3lKxuQmasZoWwVuU8PhgN+9OUxOseO1Zrh5K/Lry4ujRHc0IX3o/
+uRxJTaMiPreyZoxYmFRff2cEtsGwoa3u7YgOkKyb1GmEl6h63VUn7TAKrfA
j1WpxLvCwo0pPq3QYek+nHmixGsttdihvlYsvJIl3jqsxIH5hkTHvw3WbmVt
cWo06Lw5bt5EG6LGFnu7mNBoCSFFKsES0G1XXX030jAb4K/nfZYgjGQUhLvQ
GAgq7+EIpeCqd/o/P0ojt3D8WswXKeaWOydVCmlMCl973k14gC9HnXRvpNK4
drph880FBJyhk9OP79E42LNqweBCguaqioktJTTMrRN6zXQI/BM/DeUyvq90
jcnRJyjlCrtjS2m83KVo/vNrAkfBhTJeOdPXVudnWxKET2YFXKqiUZPqeDjT
jaBrVqZwbqBxdvLOxpfuBBEy3d5qxnwnA43lngTLMtyfrm6kQc32FWT4EBxD
T8NnxvN8BWPXDxGoxNHpRc1M/xtRZPopAp7GjKe2hGb+YMqpP4zgWfc2fgRj
qtvbhBNJEJl/2mGQcXDiisa0WALJ7vkb6qQ0xP+UqKedJ9ifpKcnIDR8HQy7
exMJVA94aPYw5mZE5RsnEeRbiufsH9KodbF3+kNE8O4xZ5TziEZ8fj6n9wpB
aubBwQTGfBXtUaOrBLZB2f1TjA19TjTsFRPQdoNdvu3MnpLnotRUggt65o/a
Gf8H5RhrQQ==
        "]]}, {
       Directive[
        Opacity[1.], 
        RGBColor[0.560181, 0.691569, 0.194885], 
        AbsoluteThickness[1.6]], 
       LineBox[CompressedData["
1:eJwV13k8VHsUAHBLhFTCI7JGkiIhITkllShElkQ8lZaXpazxCEVFyxSF7Euy
hSQJMffOkClarFGYq5TlodAixTv3r/l853fXc8/vd85P2cPH9igfDw+PEC8P
D/2bPfLEv+XiBpPtNbn4DxdiVok/KpcUA9fMgUB3tLhx0N3rkkrQdTeaYqKd
nhXIh0tqw9Cy2duRvFyYFWUdCJDcChHeOVF8/FxoKX31w1fSBm4YjrjwCXJB
n7ez0VvSHVSlbeR/LeLCyJoDJV6SvrDiy78tHGkueAW2771KRAD/oQK1dm0u
uC2+3ioweAlq3BTyI3dx4T6fxcArHwZESIaHS7pygem85msxKwH49zCC6/y4
wFgsvjClJQmqmtJ7dC9xISC1S0dGKw22/5vjkJ7GhQnNLBHJp5kQJWm4mnrA
Bb7NB+dV6rJB4d5U4nI2F5Qq+edDR3Lhu3bjc9kuLqT2aUe9/5oHNTJZGWpD
XDiYUeYuMJEPTmVtzXo/uaD6UsEiULoIKnbY+34UosCycNnjCdn7YNIf+7ZB
moKw6UmtB7tLwTjiVoPaagrO/iMRKf+4DOxVBiTCNlLg8dCn+FPlAyjw4zfV
MqXARmd7r11zORTtnq7Ws6ZgbGG39Y5XDyFgPl2wyJmC65KfnvV8roD52kMX
9ntSYOeV6rV/9hHcEtW24T9NQaPcX/a2vytBRbfDwDKEAhN+AdNp8SqY+Ggy
nHKegqNbH/vKSj8B7nQOcSeOgvsc8RZVqWrYePfEXNNNCmYvD2q1adSAe1FV
bmcyBT8zQ1oFNGth+nvGveoMCn6Nx4S+N3sKy3ySJFzuUiB/2dg20bEOvJuV
PysWUsA9nm6VdLAOAqtmQBuda85jmexWB92Be65tRWsIsbenHKuD8pMei/5G
G1y03JgRVAe5WjYXs9C20c7L7yXWQfxq3RilIgpiIoP7KrvqILlXKlOqmIIv
ZytOdjrWww5ne7MP9ylg6K/7oelSD/bd31Qm0euncs5Hu9fDf2KbfvCUUOB9
Kj5V70Q9PC5ICVZAjx468/JmSD3URI59ckJ/2q6tY51WD6EvHp5+gX6/uHjm
2UA9tDc8h3ulFPz7XCVG8XM9LHSIiq9Ar7iYIh40Wg+s9J4eAu3ME7dWbboe
ug54mb1Hn5qp0CL5mOAduOK8WBkFw6Px7kqSTLDdfackEP3x9T72e30mTNuv
rDN8QAGluyz+gwETGr1qnpqh+26//nvEiAnLb1wpt0a/dbGe/2HCBMNfnaFH
0c1De4wkdjHhrIPoVQb6EY952W5HJgzqzxR8RMdom6RWBuH9E9+1XCin4Hz8
n5NPzzJBVzHQ/xo64nutITuUCcHl5mJJ6JDazV1vzjEhYvbW6iK0t7mh+NhF
JlyRftP+Gu3orndJJYkJ14M4a2QfUqDO0PBnVDFhVxf5NBf9dMeORVermVAw
kEoVo21n3bIv1zJhNFV2tgId5nnrdRSTCXIKvEsb0K3GPJoBTfh+diHVH+nx
4Y7BA2+ZEH9RXmllBY6bRjqq/GRCmOxqtVvo4z9TxhV/4fPzBc2loP/cr4yW
+82E50knm7PR6jL/PfyLh4CsVuPdZeiwCQcxISECJoKvNj6nx1PXcf6TJmAh
o//VH3r8W5dR5SYCtId2yrs8wngk/5HpMyQgsy9kpQf6whaVGQFjAsa4oSuO
o2OjvR/bbyVAQXhwyA+d9JeA3jdzAno+232NRZdv1NHUcyagpi+nqhJd2e0o
6uJCwMzN/F816CdhYaPnDxEgaReiTaCZDU0FbR4EcL9fCHqBbrF3U/P7h4A3
zfp+/eihgCuK5f8SIFcuqixQScF/MuVz3eEErFYdyBBGTzzt6uWNJGBp9gXx
JejvAiqp+6IJCCC026TQ/LefLP96lYCUa6vI1WiFyk/LtDMI2OcR6bALvdJZ
9KtjFgHqN1eLW6JXzW14fS6HgL6WO4QVet3OsGuv7qGPD83aow07JRb5lBFg
pKnUdxi9//vWBSUEAQcmeA+EoQUUR0bLWATELhhRiEBX7opve9hAgOp53Z4o
tHTyYPYTDgEJtSPal9A9m2O3Nbwh4G+lkUM30XFHdNc0tRHwLqL3VQLa+Op7
sRcdBNRZ6OgnotP6tLivuwlgyvJ8SEG7R7aFv6cIKCta9zQXvazwX8/+DwSc
cVzbnYcmW1dZDQwSkH4qbiQfraIaLD80TMCGNp+xYvSnRrnaqa8E7LjB8atA
J0405HyfIsDmUptRJdp8uU/czDcCGoZMfjxGF5wgnOdnCBgZNLSpQZ8S9ZwR
4SNB6Q0zikDLb1xKLV5AQoBE5zSJfula1SQmSIJO/R4XNnp9qUiSlAgJQ7aH
Fj1Dc7senpMRJYEymrFtQjN4XI/JLSFh+5Vv1znoyX2l+ivFSdj2j/jIC3RO
iJPCKkkS+n7qLGyh45/DK6guRcLgRIXMS/SjabsOTVkSNGV7FV6jPeX/1GrL
keC99/yyN2ipnXm5ugokyIzEztAOTvzpZ7iShAK3uoI29Bpm1kFjVRIYBiOn
2+nvM2SxHdRI6A8JWt+Bjl02rWGqTkKimucA7c1GaeI7NEio2lwZ20nnp8fO
X7vWkWBafUy9C50aN0FZaJGgnx1SQ3vu/bYHNjokOJWwibfoEoHRJDs9Ei62
zup1o920EiIc9ElQdE9Ioy3muOX4AQOMr+O1WdrMc5+sXYxIiH08bNWDPp1/
fZObMQmukXmJtFe+MVD0MCEhuYLdQbt1hhI8upUEk31mwu/Q51fGjR8zJWGP
4yod2tmGoHDbjIS4JUG2tJk2U3vZO0mw/rrXk3b/sXthk+YkJM0XeNGeCz94
X8mSBE/DuOO05W8v7bXaS0J1+lcH2sb3WaJh1iTcXN9rQPsgO8i4aB8JX0b3
LqEd8m7tqW47EtrbtnfRz5c82Z+y0IGELZ/r4mlXCSe82OhEwlq1+u20O5XM
Zw87k5DBMPtMv/+3Tb81brrg86hZh9OWtC5zZh4iQWqoT4i2jueR2HF3/L4d
UzF0fPeFLa+WO0xC9tjVn3T8fROahy2OklC24b4r7etFETJnj5FglrWvkv5+
JaTe7nsnMJ7GoXy0R7+k5vN7k6DwzTWAzgcRoX1vN/iS8ExcOpXOF3VFASH3
MyR8P2hf2Urn216vY7WBJAz4/t1A51/0EeXEkWASMo3XV79C54Z2NC4PJeGD
blQ2na9UwRa1gHMk/Mp9vLcZ7SK4ZFDzEgnHb7gvoedHqDwp6RJLgs0Tx4wG
9B29QLPYKyTUi7ar0POpy6Mv5xODBA19l4X0/LOtL/HISMb5E19oUE3nR6fH
zZYUEhykXxytoufXmBQ5m0bCRKNvDD2/W1acU3bKxnjkW2aX0/M92JorVkSC
fyCYFaKPXecXg/skPHC4vJheP2LyHoNXKY57OD2/i2a1K2ZwHmL+/I5WyEKb
bPjiGlVLwoyR4gZ6vdIbYbybaiGhjbt7nl4PeWKaJP68IrHOiOiFo5uVefYI
tpLQmDvsGoo+csCndnknCcsf8t0IQN9q2pu2pQ/PL8n0P0Gv73kibhfHSShd
eWWDNb2+mZomMr5gPi3QqaLX72t9Z18lT2L+GX7XNUerSQ1D8XcSXt/pFtmG
dox+pvRmDuNrLKqvQ+fb4QsDsktZ8GdWKUocfWG+RlZ1GQvuLCx2puuJTcqU
raYEC1qO2quLoIdaPVggzYJZGf4cXrSM6bbcI4os8Pn7+V9fsF6FKM0dLVnP
gtpTQl+fo7f0Bg1vs2HBqL9peDA6+4XIyFZbFoQ/OyRF10/B6rQR2M8C7xsC
uV7ol7dZo1ucWHAkbjKDrr/uNkvHDd1ZkCltIbuHrsesvMkNvjh+Y2K5PPp5
QcesMoMFMiKSJo+xnmslHf+tdJMFeubnLtL1Pz5m9rdiAguK//g3FqBdDivP
ySexoPTlvGoqekzOi0c2kwXCwvFHItBijAUC4mXo/tKxnWjHQN0lfK/xemKs
1c3Yn2jYvXFY18qCaW0Zfxb693qfDId2FvzgVa6oRmcOF2oXvcV4VexYWoAe
cVlpt5/C+7t+2BCNDt++LClvkgVban3UN6Pvik0oW/7FhuLi5p407K+Cx66c
DJBmw88Op7R4tMVzjYcZMmy4XHrb7jJ6/Lyn2bQ8G4L9Vqf4ow1+9HqmqbEh
1iTttgW6ube5aGITG6KobpNp7P+mCgs3Jjizod45cb8J+oxoc0yyCxtUhtoc
dNGTXmNd6YfY4O/oZKVOW1s7pMCDDe/fGciL0658VFd3Es8PdTIaxH71C8nc
NRzChi5p8TOX0WM9nQdMUthgRnh3cbDf/SzCHz70Dq/f3XJWA/vnduf10jJ9
bEgcrsxfgSYLD5bt5rKBN0T4hSg63aJioPAjG6plR4fHsV93iD26y2uMDVUH
MxnlaLZwk9jkHBsm3rVGbkJnC13NmVVugK0djlObsP93FZTmLDneAMFFlLgE
7hc+3pvXuPe1Afyqp9oscD/C8ufc5z3TCC4a/zSvyqRAtddKV+tPI6ROdnS3
4n6m1/dRt9y5Z8CIl1A1iKcgr/DL7bC5Z1DhrpEldQXf/9xnh7JLTVD2xo2b
hvul2xH1m+4Kc+CSwXi0QCjWq24V4cFFHPhx9UpWL+6v1HUu9agu4UC/8IfS
R+jqDzZhueIckA6oyj2K7t85QOas4ICmjMAE+ywev1jAKluTA6OJqwQjgymo
SbY4krGPAyU+500mAjDfJ0s29ttxQCNB7UED2spSYqGiAwfk+iOlUtHc3+8K
0p054D3k/sQcvcDdayLtMAcqmphuWf54vBojJDWAA631C9fb+GH/dW7a8n0Q
B9otNVtW+dH9h5O8XAgHrOZEDs2ewfy4rMxMCedAUee4Yx468b9ygZSLHBCy
dyqexf2k+w7prp7LHHgpCdzXaI300HzZKxwQme0VzEPXWptZ3mFwYMzIbJ0N
Ojo/X67nJgfiExatXYW24l08LnOLAw8k4+R++eLzOJ+uP5DIAfMakucVmlve
wUhO5oDTpfKuHPT/m3aLbA==
        "]]}, {
       Directive[
        Opacity[1.], 
        RGBColor[0.922526, 0.385626, 0.209179], 
        AbsoluteThickness[1.6]], 
       LineBox[CompressedData["
1:eJwVkHdQ03cYxgMYqSIrxsYBVhlWVrQqUkd9QBFOqOyyPIRaqKOHjBIoIENT
UayW1AqIbARZsgUE1ASCwW8PlJMKB0L9Zf1QqomAaCu2NP3jveeeu+d573PP
+iNRPhHaDAbjoOb+17KpjriBc5/t2ddVrsVgUMiwZLU2s43w7QF5fJjGs3Yn
VGSx14GhuiAVaXxgX7VpKnszKnwsc09rUZhfJg7isR3hHDvH19ahMNDw6F00
2wu6nklHtRdT2K41LDnJDoNryWDMez0KU1ZB9ZHsaGg1pMf0cyhExv9+8FJ3
Og6Wf27wcjOFUP2sx0zlebiK5MsGXSnUabvJHkUJEBTgqkVCKIiCraZviq+g
grwIt+ZREOizdPMHriLOLM205AIFXsHIllXcQuTteOYtKKWgtitdyr5bgoXW
V3uH2ylo7zq0YH6vDFWDP3fw+imsa9NZSJ4qh22Iy/FOGYWCPzafGZ++ASO5
5UjqOwqHihvDmOoq7HjZUa7Uk8Li4Vq3eE4t4lMNqzeYSeFeY9yuXl0HNZ9f
x3GQIuXNDLfpQAPsrJcaGnpIkfjd8tOm7Y2wMLVjz4ZLcaQl6ibd1oSYCO7z
2iQpvLbsm/Dtbwbj6hmbtstSvNId9dz/qAVNy5z+rKmWIotN941N3sIsa/Q4
VyyFb2RBpN98K7TFzsrZcSkkJiu+8vnQhpbYSMvWOSn26DD3vmHdhpVVw4MI
YxkiHNujV3M6sNbJfN7UVoY6whqw+LgTdrau05tcZZjPVHKHrLuw5Nc5xbUI
Gf4qSXrMtLsD7yFLAz2+DO9VGcnjzndxKeNsjk25DKaZu31yA+7BPkQ1x5DI
8Drx1onhACECVbmJ/bQMikHv3vHtIiTcdPuS6MmxUWAdJ7gtghFZyGjfJEfK
3MjONoduRK439PH2k8PvreOi+u5uCIuyv69IkYNv9pPq6N4eVP2tA1Ihx7Yp
wdPZgR7ofppTsHJIji8mEl44eYnxKqbP2GpBjoD4rQbag2JwL9pRxrYKzNbU
2F8J7gWjeMq/97ACk0t1Up8/7cVvozwjHYECIYs5xODYfYzs7Jy91KeAonLB
unL6Pl4L2z8KYSghjiN1WrES+EnKfuE5KGEx4bGV+48E88dnVAKeEhPRraMm
aX24HhpRO9ykxI2a1zkp//Zh+jlVaqZWYiZt0r/x/AOYuSTR3G00ctKFDhVL
CGrLo4KME2iEjpovUeoRWO65lez0A42NW86PWRgQFI6mP4xJpNEp90opZxH0
uj1gDiXTeOYi67m+huAUv7YrN12T12d6lNkRrOkqOmB9gUZXnlt4sTeBf+Xy
FXGFNH6cqbd/5ktwsn+Vb00RDQ/35bqf+BN4Zgf1UMU0qA9Pq4uCCSpdCrZ5
lNFYFBapLvyGYFc2OWZTqclvECQV8AhSPYrZ6kYanLQ37uMJBNKHo8c2Nmv6
I4GmJkkE0+Ibqq9baMRmrhflpxIEOhpkPWmlkfuymZl/juC+ut5c2EkjbD9n
ZCyTwIa/kvm+i4Z1UXLV6osELbOOK+3v0rjj6ex+TUAQUqIYqhXSOFtVZTJ2
WbPH2xOnJkUaHi191apsgu2HJf7mPRqe4BhhUC7BjvAPR0LFGp7mJ4K8PM2/
bFZpfi+N/wA8HGWy
        "]]}, {
       Directive[
        Opacity[1.], 
        RGBColor[0.528488, 0.470624, 0.701351], 
        AbsoluteThickness[1.6]], 
       LineBox[CompressedData["
1:eJwV13lcTF8UAPC0oiKtpEIi9UsYRIlDUWpEWVomlLRpoU0YoZBCtO8l2vVL
Us28N0tNe01ayFRaMI8WpZ9WiaLfnb/m8537zufdd96995y3zunScRdhISEh
hUVCQoLfzBFGQEvYtn3GrGz0Dx/ubZCllcjLwGGzL4GOyLKGV3Ii5deC/OgD
ohLZtuG56k35rUBs35AYsogPc1I1dpfl98Ns6NQdYRE+tLxs++kjbwlBD+zd
hMX5oLeos/6ivCMU2vZf+i3JhxEtuyJveR8o38s+3qzEB+9AnsWjqmAoqK7i
Dm/lg4N0ZLvYQDjg+q+E6kz58ELY/HPbpShwjm+4kHeGD5UUrYnCmjiwrRHt
4gXwIUpaViK1JQmkvtWOTdznw+W0LtIq3XSYe7c+z+gpH8Y2P1sqX/4U8qfn
9eJofBDeY7+wviITGHSu+HAjH9bSRRauj2TDl8qMtfof+JD2cevtvolcUBYv
Hgsd44N9RrGj2Fg+VGwSzW8QIkCjVc08UOlfiDXK/5EnSwC5YAU2pvwCRpS2
xPqrE3BjelL3ldlLiMxIC7tJIuCap1yIKlYMIm7fysQOEOBUeqlwkP4Knp2g
mFAsCLAkGX840VwC46KDJvvsCMi9L53l8KYEVjyMCHdD/sPvcvfklYApybAk
Crkg0mv6dl8JRExHln9GFv0vXrJktAR6FGdLQikEYHlf9WWkSwEid8k02BOg
pvo4odmiFKba9h7bdZaA/yS6jx1qK4UuE7HC104ErDVk5OS9K4XbgToRE8jH
fZLnlrwvhdovuy2UzhOAv7fLa+WXAq7Z7OuEfPt5z1+byVIIOeRq/BNZkdz3
wkOhDGZ/FMwruxCw7/EnySj7MhjqLXY3dyPAp5pzbtKhDCh7HMleyFkzGdhJ
5zLwTDCSfoy82MHx/CrvMqgccNd9i9y+hWBm3iwD/Wis/5Q7Aa5vP1+gPSsD
sVkdBbsLBETKDzb0DJXBnuK16008CZj8L6ho0yi6/qqk6Xlk6wa5+MDxMojX
zDULRlahHnCS/VUG06NlIwzk/E9p82ZLafBqBSvlHy8CKgtObGPo0IBncTl3
sTcBE/urUhJ9afC6R0/q+UUCTirbhgxcpsGCb/i3KmR86rvbdioNigze5PUg
38xV2dl2mwaMf5ZikpcIWCp1tU08jgbfTdJ1vZDVu3RFA+k0cK8lLm/0IeCE
d5r3yTkadPZrLgr2JUDG0sDFdoEGiQEc8VjkVtL706dF6HCITR7NQTaflTvi
LEkHn8NyNq+Rje481A5YTQeFe3IF8n4EbE2mDsXuoUMA6+qeZ8jfr6/8lAh0
+N7SalCCXHiW3plqTAdShv2qGmRNjcm6LDIdLFKH/fuR1V5eyC61p4PrhrD0
Df4ESNXZnnt3nQ7TvvvxZ8hNeTO2XbfosOObQ04xcviDOMveO3SYMP/hw0EW
sXwDXx7SwXK9VVYf8u8eU7WpVDpUnb7qrxBAwPC4Xq8smw6iL3n0YOTcd7x2
xUo6FAanhD5Cdqb7NSnX0sEhRJaUgsy/XsRQb6ZDRPnQ+lLk9+Ibk0h9dIiG
lgtfkOtVFE4dn6dD++S//nsvE+DY19hXKISBgXv03GHk36lBzhJiGLxQ1qec
RNZd3e/PlsZA8WJkqgdy/KrSmI1rMAgISNKJF4x3uyqHrMfgcuwb9wzkxiTl
zF5NDJR/ml9/jjyvdPtV1FYMyg7Wq5cjOytavfl9AIOGDfLUz8h/O0RtT5lg
cLAk1fUbclI8/umlOQbij/o3TSM3y68dcz6BQaPrNV2xQAK2y41Jtzlj8Ovs
8J4NyM3tmfFaFzDYsJnG00F2jbFWveuNgfGpSqMdyCkrKnT0AzGo8T7xxAhZ
WOYROSsMA9bvTKszyGlt+9/9eYiBylhGzHlkvchpim0UBupku/wLyB7LTntI
J2OQfpljdRlZtHX5pFs6Bnwb8gAV+cmjmmvVzzDw5FpZBCO/lfrn/pUCDI5Y
R6Q8EMQ3f5RpL8KgxCXjVqQgPiImSacUg5DRkd1xyLslf+cRLAzaVYY105Hb
m15sMazEIC+FeuYZsteDc1hCLQb3dgu75SA/XcKtJ7dg4Gb16kchsgE36Gju
WwzmgzFqMTIvfGunUCcG1fq+70qRLx7uP2Pfg8Gq5pYFOvLixUkDtI8o/i57
gYGc2UD2lvmCxv/VecdGNgxbmPYYQve/K03lIF8SdxNdO46B9dGPB2oF8fXK
EdRpDCJLC53qBfGhrXIdsxi00PKtGwXxh26nbvmDQWlolWoTcqeo3voHi3Do
PzaIvUb2qR0u6BfDQW6v+IYW5KV300mwFIdZXxmXVuRsYytm8jIcLJdPX2lD
3iciZjQti8PQtqdn3iCTmjk9oko4uP7cIvkWWTOe6q+wGgfZyN3RAquc3Sm1
cQ0OH7YWjgksozmerbceB/aKGxvbkcXGC/aaauKQdD59p8C/cZdOm39wcDi1
oCLw95C1l9y34DAlG98niP9i3itxbTsOu+ssAwR+L5fw9P4uHKRiNPoF82nu
s9RP2YODaZqwjsBVOZLtBYADR+QDWTB/+sV6D5YxGh/IMRY8X8GuEJFmUxwK
KcfkBM+fIWSY1kfGAae+ZgryE8ed2fHfMRy220kZCPIXHvOq5c8JHHrFRaMF
+Q2y93JdZouDX3Q2R5B/Xw3NBbXTOBSL8OsE78f1PyJxiyMOZJf8rGpkCj1t
635nHGaYY7aVyMdu2XAt3XFwFM8fKEc+eFjW6ZwXup9JjRkLeXNPWOztABzy
H48k0pDVs4x0Yq/iEBHaE1KCrOj1pzYrCAeKxQ+jl8hCf/1mau+ifEj6m+cj
T9dvjuwIR++jixGVjTwc+VVzMAIHk7VTL54K1ve6s3YS8TgcoYl6JyHXj6yc
VErGYQ2ncplgP7BK3z3YlI7Dxmq9MMF+yT5kxjbLwSFt2cr5UOQrF3asiaDh
AHHq0z6C/UEaw9IYONQcW2fuiew499zyRTkO1Z7ugS7I5o/W3G6twyGck2Bl
J1gfr5YOyHTi8FasXwYE64Nad2NdDw59SxKUdgvWh3GwIukjDkXyatNbBeuB
98P0xCAOoe8DddWRK2f5BfEzOKzOwXOFkV32Yz7KSgyQav2UxELn1YOiTSmK
qxngmSSlWYJcrJJSI7uGAWuoHQ/zBefrbJCipCYDaLhodxzy41dG7Dk9Bgz9
tg32RMbUWyU+WDPgGKl1Rga5Lwq2dVMYcHTY/KsosvBCMaXjLAOMzcJezqLz
/Ehv7IsWVwaIhUlwPgnO/1i7ExWBDDCffMr/F3mJ2MCTjAQGFGmKqxsi6/pb
N6amMODaop/um5FPEg0TiU8Y0HKk8a4a8pPygoNRuQygL+YZCCGTAn1GgukM
WK0rnlGF6pP90NxOp04GzLUF7TFEfsGVbdFQZAJL/+w5GVQPi+RT7l1RZkLj
RM6l36jeFjus29+kxgTtIHmzfuSSH1tKL2kyweW2yhkMmb7OIpm5mwkKnTwJ
e+TKa2EulvZMSJ0vt0lC9b6qdtmaLAcm1OxqWX4LuXp5wvsf55nw/FJvtAty
XU42OdWLCfvtCqa2ITe9rdo2eIMJMTwJnUbUP/C05v9cf8qE401ODiOo3+gI
uIO1ZjMhGzKKWpA7OUt91z1nwiWhl/XFyN2nVvXXv2LC6ol5y0DkjyF6TStq
mGASeaD5L+pvvnb7JOQNMGF4VfcRMeS5+wO677RZkFr6dnzIA/VPH/1/9eqy
YNH6Wyn1yCUk4dp+EgtYK6Rlc5A9etXsZgxYoBzx0OMcco+O7Z1VZBZ4a1E4
Xag/Y7Y1dTl4siBU7uIIE/VvVIXi4NF/WRDra9NFcUXzvbCP/OMlC1zuuqtt
RyZVNCv8LWWBPfXMNknkEdfhguVsFqjiJUwm6icpuHoHqZkFun6mdkrI+vbx
WtdGWWAnNIvVo/5z9im1XWwzG2wGm+//cUT9ULvaZpdtbHAivz38GtldpCas
dicbVD4aNCYhv3eR3HtnHxtu1gxLbEfGtdPyhCzZkKXjGe7sQMDVsvKgOT82
+G2Ou846Q8CvBiHNCZwN4WnuCbtRf036lR1iWc6G3TLb9P+i/ttD26zvZRUb
ftLx5Brknojo6ItNbGhpexlrgcy0Up//1ssG8cIv7mdtUT56jd8O/mWDLx4e
6m+N9tP3e9f7DpYDi6o17nEcfR/UEa6aZuXQO9Nhp4X8OtXwuJ9FOcw+Wxoy
ZIXq/eHJTYuty0HKiyFxHnlf5pnO7W7lEGcxOGVtScCdUzu2PbxfDr+GDHeS
jqJ+jk0M6reWw4tMBS7NjADV+4bHE20qwFHha8wooP3m/uRokn0F6AQNW2Qg
Zx8WIic7VMB5NrXJCll7ca1xqlsFRFuJL6bvQ/U+jLwz40oFXPj2Lub6XvS9
EEpZmZdYAZ0+b7p+GxBwL+TqR3pXBWywn8nk7iRg/FqZR6cNBzxWfrbK1iYg
Sk/n5+bTHIiUdPizD3nLVNadUEcO5G3RoHRroXrvFZu24wIHhB5qaUojfzvr
1xpD5UDMjmgpX00CBo23ko6lc8Ap68i4lgY6X6QLfzV85sDoKfdAB1UC+t9Y
1fbpVULjg2lT3jICNkVpB0ThlaDI8t6gPM6HGz+6DOi7quAs9mTHjhY+nJzZ
L1pUVQVJ/7wyePqMD3fUH353M6qGT84/Vxj582HHSFTvVEs1FMx81z9twIe9
H64MH7CsAQulwkr9P5/AJnD7MuE3NfB1ntAYoX6CqYKCnXGUWkiMEhmc/fAR
hpaK3PzaWwtys901YoYf4Yy4EneZex3cVWt8XEz9AP15C9p5E3Vw+uItGu95
H9QEcF8s8qsHnvtUrExHL2h8OLpd9089iGlRFOu6e+CDD61b5VYDtAqT73b3
dENuwXjCjb8NMDNpE/Kz+T1M3hqyLg5vBKmIgqd4VBckBHN25SzhwqyT5qLk
453g0L1+yYAkF3LxvoD3RzphEym8R2MZFzgs9kpF005gfrG8kS3Lhag1CWMP
93TCJ5PP1VmrudCIf2g7p4GulxY7mrmZCyyKqhT9Rwewks2dM6y4UFDoNf8n
oQPuThbt/HSCC+Iasg3C0R1wlCwnscaaC4NlqXSxhx3An+99/oTCBdrkmLD4
rQ4QdfQeSz/PBbnIQpNxV3T9xihq2mUu+Kl3anvqdYDSrWly3xUumNYqrzq+
FcV32aqqULnwJCtm4y7tDvC7v64y9SYXdPOK0udUOyBxtEQsNYwLaemJk95i
HeB4SKmr5z6an0ngm0MLPNB+cj1fOQKZUti0+hcP2McOklOiuHBdt1i2epQH
ofn5Kj0xaD6P6h1iBnlwdJH091XxXNgfV9fgyOeBEsWXY5fIBW6QlfnmHh7w
SzqikpO54B24bOjnOx78D6L8u6I=
        "]]}, {
       Directive[
        Opacity[1.], 
        RGBColor[0.772079, 0.431554, 0.102387], 
        AbsoluteThickness[1.6]], 
       LineBox[CompressedData["
1:eJwBwQI+/SFib1JlAgAAACsAAAACAAAAouq5dM2HMT5Du6QBAADgP4YlErWy
FBA/SEnjdmAA4D8SPHeljxQgPxXsiuHAAOA/WMepHX4UMD/9MyWXgQHgP/sM
w1l1FEA/jonzgwID4D/Nr8/3cBRQPzb9W2cCBuA/NgHWxm4UYD/er/Jv+gvg
P+opWa5tFHA/VSOHVMwX4D9tdtRNjcGAPz9OPRDqMOA/Xw2P0gXliD+OK+f+
xkfgP60CSuLPb5A/bTZdaq9d4D/AWinxrMOUP5gtOJbudOA/kA0SB5vNmD/g
fcCMMIrgP3Wc1zIZLp0/FUzOw8+g4D/vLaEKFL2gPwOHl62stuA/Ajtb/yO+
oj+X5FG11srgPyC2A/976qQ/GFGwi4/g4D+c3jCC3PGmP/GnXFDT9OA/W5+w
YAXvqD+89+yO0AjhPyTOHkp2F6s/k1DU99Ie4T9LqhG37xqtPzqr1ondM+E/
ffTyLrFJrz+0MFVBfkvhP3lrE4Edt7A/2+19QtZj4T9is2+s5raxP3atjabk
e+E/UDJD3VPMsj/uu+gH35fhP+0H2U9Fz7M/Qf+2wi+04T+PFObH2ue0P5/i
A3LQ1eE/U22cbVT7tT/DWsuGbvrhP8YcFVVS/LY/ox1/rG0g4j8+AwVC9BK4
P64wnJmOTuI/ZUC3cBoXuT97L8sxBX/iP63JEs0kFro/Emo8OV+04j/7ieUu
0yq7P85X5Xi/9eI/+KB60gUtvD+WBcvIxTrjP/ruhnvcRL0/HxvZooSP4z8d
iTxSl1e+P+fsDPoU7uM/8Hm0atZXvz9stB1IslHkP+TQUcTcNsA/ZBrWQajL
5D8okCp0kLjAP1sm3A0PTOU/ffXXOrY3wT8VnwDWCtnlP1T2QASuwcE/7aEX
y7SE5j+DIovuZ0LCP2QuuFopOOc/gVszdFeUwj/e3FYX+bfnP6mXV2M=
        "]]}}}, {
    DisplayFunction -> Identity, AspectRatio -> 
     NCache[GoldenRatio^(-1), 0.6180339887498948], Axes -> {True, True}, 
     AxesLabel -> {None, None}, AxesOrigin -> {0, 0.32}, DisplayFunction :> 
     Identity, Frame -> {{False, False}, {False, False}}, 
     FrameLabel -> {{None, None}, {None, None}}, 
     FrameTicks -> {{Automatic, Automatic}, {Automatic, Automatic}}, 
     GridLines -> {None, None}, GridLinesStyle -> Directive[
       GrayLevel[0.5, 0.4]], 
     Method -> {
      "DefaultBoundaryStyle" -> Automatic, "ScalingFunctions" -> None}, 
     PlotRange -> {{0, 0.2}, {0.32543752254591746`, 0.7412076431127408}}, 
     PlotRangeClipping -> True, PlotRangePadding -> {{
        Scaled[0.02], 
        Scaled[0.02]}, {
        Scaled[0.05], 
        Scaled[0.05]}}, Ticks -> {Automatic, Automatic}}],FormBox[
    FormBox[
     TemplateBox[{
      "\"numerical\"", "\"1st\"", "\"2nd\"", "\"3rd\"", "\"4th\"", "\"5th\""},
       "LineLegend", DisplayFunction -> (FormBox[
        StyleBox[
         StyleBox[
          PaneBox[
           TagBox[
            GridBox[{{
               TagBox[
                GridBox[{{
                   GraphicsBox[{{
                    Directive[
                    EdgeForm[
                    Directive[
                    Opacity[0.3], 
                    GrayLevel[0]]], 
                    PointSize[0.5], 
                    Opacity[1.], 
                    RGBColor[0.368417, 0.506779, 0.709798], 
                    AbsoluteThickness[1.6]], {
                    LineBox[{{0, 10}, {20, 10}}]}}, {
                    Directive[
                    EdgeForm[
                    Directive[
                    Opacity[0.3], 
                    GrayLevel[0]]], 
                    PointSize[0.5], 
                    Opacity[1.], 
                    RGBColor[0.368417, 0.506779, 0.709798], 
                    AbsoluteThickness[1.6]], {}}}, AspectRatio -> Full, 
                    ImageSize -> {20, 10}, PlotRangePadding -> None, 
                    ImagePadding -> Automatic, 
                    BaselinePosition -> (Scaled[0.1] -> Baseline)], #}, {
                   GraphicsBox[{{
                    Directive[
                    EdgeForm[
                    Directive[
                    Opacity[0.3], 
                    GrayLevel[0]]], 
                    PointSize[0.5], 
                    Opacity[1.], 
                    RGBColor[0.880722, 0.611041, 0.142051], 
                    AbsoluteThickness[1.6]], {
                    LineBox[{{0, 10}, {20, 10}}]}}, {
                    Directive[
                    EdgeForm[
                    Directive[
                    Opacity[0.3], 
                    GrayLevel[0]]], 
                    PointSize[0.5], 
                    Opacity[1.], 
                    RGBColor[0.880722, 0.611041, 0.142051], 
                    AbsoluteThickness[1.6]], {}}}, AspectRatio -> Full, 
                    ImageSize -> {20, 10}, PlotRangePadding -> None, 
                    ImagePadding -> Automatic, 
                    BaselinePosition -> (Scaled[0.1] -> Baseline)], #2}, {
                   GraphicsBox[{{
                    Directive[
                    EdgeForm[
                    Directive[
                    Opacity[0.3], 
                    GrayLevel[0]]], 
                    PointSize[0.5], 
                    Opacity[1.], 
                    RGBColor[0.560181, 0.691569, 0.194885], 
                    AbsoluteThickness[1.6]], {
                    LineBox[{{0, 10}, {20, 10}}]}}, {
                    Directive[
                    EdgeForm[
                    Directive[
                    Opacity[0.3], 
                    GrayLevel[0]]], 
                    PointSize[0.5], 
                    Opacity[1.], 
                    RGBColor[0.560181, 0.691569, 0.194885], 
                    AbsoluteThickness[1.6]], {}}}, AspectRatio -> Full, 
                    ImageSize -> {20, 10}, PlotRangePadding -> None, 
                    ImagePadding -> Automatic, 
                    BaselinePosition -> (Scaled[0.1] -> Baseline)], #3}, {
                   GraphicsBox[{{
                    Directive[
                    EdgeForm[
                    Directive[
                    Opacity[0.3], 
                    GrayLevel[0]]], 
                    PointSize[0.5], 
                    Opacity[1.], 
                    RGBColor[0.922526, 0.385626, 0.209179], 
                    AbsoluteThickness[1.6]], {
                    LineBox[{{0, 10}, {20, 10}}]}}, {
                    Directive[
                    EdgeForm[
                    Directive[
                    Opacity[0.3], 
                    GrayLevel[0]]], 
                    PointSize[0.5], 
                    Opacity[1.], 
                    RGBColor[0.922526, 0.385626, 0.209179], 
                    AbsoluteThickness[1.6]], {}}}, AspectRatio -> Full, 
                    ImageSize -> {20, 10}, PlotRangePadding -> None, 
                    ImagePadding -> Automatic, 
                    BaselinePosition -> (Scaled[0.1] -> Baseline)], #4}, {
                   GraphicsBox[{{
                    Directive[
                    EdgeForm[
                    Directive[
                    Opacity[0.3], 
                    GrayLevel[0]]], 
                    PointSize[0.5], 
                    Opacity[1.], 
                    RGBColor[0.528488, 0.470624, 0.701351], 
                    AbsoluteThickness[1.6]], {
                    LineBox[{{0, 10}, {20, 10}}]}}, {
                    Directive[
                    EdgeForm[
                    Directive[
                    Opacity[0.3], 
                    GrayLevel[0]]], 
                    PointSize[0.5], 
                    Opacity[1.], 
                    RGBColor[0.528488, 0.470624, 0.701351], 
                    AbsoluteThickness[1.6]], {}}}, AspectRatio -> Full, 
                    ImageSize -> {20, 10}, PlotRangePadding -> None, 
                    ImagePadding -> Automatic, 
                    BaselinePosition -> (Scaled[0.1] -> Baseline)], #5}, {
                   GraphicsBox[{{
                    Directive[
                    EdgeForm[
                    Directive[
                    Opacity[0.3], 
                    GrayLevel[0]]], 
                    PointSize[0.5], 
                    Opacity[1.], 
                    RGBColor[0.772079, 0.431554, 0.102387], 
                    AbsoluteThickness[1.6]], {
                    LineBox[{{0, 10}, {20, 10}}]}}, {
                    Directive[
                    EdgeForm[
                    Directive[
                    Opacity[0.3], 
                    GrayLevel[0]]], 
                    PointSize[0.5], 
                    Opacity[1.], 
                    RGBColor[0.772079, 0.431554, 0.102387], 
                    AbsoluteThickness[1.6]], {}}}, AspectRatio -> Full, 
                    ImageSize -> {20, 10}, PlotRangePadding -> None, 
                    ImagePadding -> Automatic, 
                    BaselinePosition -> (Scaled[0.1] -> Baseline)], #6}}, 
                 GridBoxAlignment -> {
                  "Columns" -> {Center, Left}, "Rows" -> {{Baseline}}}, 
                 AutoDelete -> False, 
                 GridBoxDividers -> {
                  "Columns" -> {{False}}, "Rows" -> {{False}}}, 
                 GridBoxItemSize -> {"Columns" -> {{All}}, "Rows" -> {{All}}},
                  GridBoxSpacings -> {
                  "Columns" -> {{0.5}}, "Rows" -> {{0.8}}}], "Grid"]}}, 
             GridBoxAlignment -> {"Columns" -> {{Left}}, "Rows" -> {{Top}}}, 
             AutoDelete -> False, 
             GridBoxItemSize -> {
              "Columns" -> {{Automatic}}, "Rows" -> {{Automatic}}}, 
             GridBoxSpacings -> {"Columns" -> {{1}}, "Rows" -> {{0}}}], 
            "Grid"], Alignment -> Left, AppearanceElements -> None, 
           ImageMargins -> {{5, 5}, {5, 5}}, ImageSizeAction -> 
           "ResizeToFit"], LineIndent -> 0, StripOnInput -> False], {
         FontFamily -> "Arial"}, Background -> Automatic, StripOnInput -> 
         False], TraditionalForm]& ), 
      InterpretationFunction :> (RowBox[{"LineLegend", "[", 
         RowBox[{
           RowBox[{"{", 
             RowBox[{
               RowBox[{"Directive", "[", 
                 RowBox[{
                   RowBox[{"Opacity", "[", "1.`", "]"}], ",", 
                   InterpretationBox[
                    ButtonBox[
                    TooltipBox[
                    RowBox[{
                    GraphicsBox[{{
                    GrayLevel[0], 
                    RectangleBox[{0, 0}]}, {
                    GrayLevel[0], 
                    RectangleBox[{1, -1}]}, {
                    RGBColor[0.368417, 0.506779, 0.709798], 
                    RectangleBox[{0, -1}, {2, 1}]}}, AspectRatio -> 1, Frame -> 
                    True, FrameStyle -> 
                    RGBColor[
                    0.24561133333333335`, 0.3378526666666667, 
                    0.4731986666666667], FrameTicks -> None, PlotRangePadding -> 
                    None, ImageSize -> 
                    Dynamic[{
                    Automatic, 
                    1.35 (CurrentValue["FontCapHeight"]/AbsoluteCurrentValue[
                    Magnification])}]], "\[InvisibleSpace]"}], 
                    "RGBColor[0.368417, 0.506779, 0.709798]"], Appearance -> 
                    None, BaseStyle -> {}, BaselinePosition -> Baseline, 
                    DefaultBaseStyle -> {}, ButtonFunction :> 
                    With[{Typeset`box$ = EvaluationBox[]}, 
                    If[
                    Not[
                    AbsoluteCurrentValue["Deployed"]], 
                    SelectionMove[Typeset`box$, All, Expression]; 
                    FrontEnd`Private`$ColorSelectorInitialAlpha = 1; 
                    FrontEnd`Private`$ColorSelectorInitialColor = 
                    RGBColor[0.368417, 0.506779, 0.709798]; 
                    FrontEnd`Private`$ColorSelectorUseMakeBoxes = True; 
                    MathLink`CallFrontEnd[
                    FrontEnd`AttachCell[Typeset`box$, 
                    FrontEndResource["RGBColorValueSelector"], {
                    0, {Left, Bottom}}, {Left, Top}, 
                    "ClosingActions" -> {
                    "SelectionDeparture", "ParentChanged", 
                    "EvaluatorQuit"}]]]], BaseStyle -> Inherited, Evaluator -> 
                    Automatic, Method -> "Preemptive"], 
                    RGBColor[0.368417, 0.506779, 0.709798], Editable -> False,
                     Selectable -> False], ",", 
                   RowBox[{"AbsoluteThickness", "[", "1.6`", "]"}]}], "]"}], 
               ",", 
               RowBox[{"Directive", "[", 
                 RowBox[{
                   RowBox[{"Opacity", "[", "1.`", "]"}], ",", 
                   InterpretationBox[
                    ButtonBox[
                    TooltipBox[
                    RowBox[{
                    GraphicsBox[{{
                    GrayLevel[0], 
                    RectangleBox[{0, 0}]}, {
                    GrayLevel[0], 
                    RectangleBox[{1, -1}]}, {
                    RGBColor[0.880722, 0.611041, 0.142051], 
                    RectangleBox[{0, -1}, {2, 1}]}}, AspectRatio -> 1, Frame -> 
                    True, FrameStyle -> 
                    RGBColor[
                    0.587148, 0.40736066666666665`, 0.09470066666666668], 
                    FrameTicks -> None, PlotRangePadding -> None, ImageSize -> 
                    Dynamic[{
                    Automatic, 
                    1.35 (CurrentValue["FontCapHeight"]/AbsoluteCurrentValue[
                    Magnification])}]], "\[InvisibleSpace]"}], 
                    "RGBColor[0.880722, 0.611041, 0.142051]"], Appearance -> 
                    None, BaseStyle -> {}, BaselinePosition -> Baseline, 
                    DefaultBaseStyle -> {}, ButtonFunction :> 
                    With[{Typeset`box$ = EvaluationBox[]}, 
                    If[
                    Not[
                    AbsoluteCurrentValue["Deployed"]], 
                    SelectionMove[Typeset`box$, All, Expression]; 
                    FrontEnd`Private`$ColorSelectorInitialAlpha = 1; 
                    FrontEnd`Private`$ColorSelectorInitialColor = 
                    RGBColor[0.880722, 0.611041, 0.142051]; 
                    FrontEnd`Private`$ColorSelectorUseMakeBoxes = True; 
                    MathLink`CallFrontEnd[
                    FrontEnd`AttachCell[Typeset`box$, 
                    FrontEndResource["RGBColorValueSelector"], {
                    0, {Left, Bottom}}, {Left, Top}, 
                    "ClosingActions" -> {
                    "SelectionDeparture", "ParentChanged", 
                    "EvaluatorQuit"}]]]], BaseStyle -> Inherited, Evaluator -> 
                    Automatic, Method -> "Preemptive"], 
                    RGBColor[0.880722, 0.611041, 0.142051], Editable -> False,
                     Selectable -> False], ",", 
                   RowBox[{"AbsoluteThickness", "[", "1.6`", "]"}]}], "]"}], 
               ",", 
               RowBox[{"Directive", "[", 
                 RowBox[{
                   RowBox[{"Opacity", "[", "1.`", "]"}], ",", 
                   InterpretationBox[
                    ButtonBox[
                    TooltipBox[
                    RowBox[{
                    GraphicsBox[{{
                    GrayLevel[0], 
                    RectangleBox[{0, 0}]}, {
                    GrayLevel[0], 
                    RectangleBox[{1, -1}]}, {
                    RGBColor[0.560181, 0.691569, 0.194885], 
                    RectangleBox[{0, -1}, {2, 1}]}}, AspectRatio -> 1, Frame -> 
                    True, FrameStyle -> 
                    RGBColor[
                    0.37345400000000006`, 0.461046, 0.12992333333333334`], 
                    FrameTicks -> None, PlotRangePadding -> None, ImageSize -> 
                    Dynamic[{
                    Automatic, 
                    1.35 (CurrentValue["FontCapHeight"]/AbsoluteCurrentValue[
                    Magnification])}]], "\[InvisibleSpace]"}], 
                    "RGBColor[0.560181, 0.691569, 0.194885]"], Appearance -> 
                    None, BaseStyle -> {}, BaselinePosition -> Baseline, 
                    DefaultBaseStyle -> {}, ButtonFunction :> 
                    With[{Typeset`box$ = EvaluationBox[]}, 
                    If[
                    Not[
                    AbsoluteCurrentValue["Deployed"]], 
                    SelectionMove[Typeset`box$, All, Expression]; 
                    FrontEnd`Private`$ColorSelectorInitialAlpha = 1; 
                    FrontEnd`Private`$ColorSelectorInitialColor = 
                    RGBColor[0.560181, 0.691569, 0.194885]; 
                    FrontEnd`Private`$ColorSelectorUseMakeBoxes = True; 
                    MathLink`CallFrontEnd[
                    FrontEnd`AttachCell[Typeset`box$, 
                    FrontEndResource["RGBColorValueSelector"], {
                    0, {Left, Bottom}}, {Left, Top}, 
                    "ClosingActions" -> {
                    "SelectionDeparture", "ParentChanged", 
                    "EvaluatorQuit"}]]]], BaseStyle -> Inherited, Evaluator -> 
                    Automatic, Method -> "Preemptive"], 
                    RGBColor[0.560181, 0.691569, 0.194885], Editable -> False,
                     Selectable -> False], ",", 
                   RowBox[{"AbsoluteThickness", "[", "1.6`", "]"}]}], "]"}], 
               ",", 
               RowBox[{"Directive", "[", 
                 RowBox[{
                   RowBox[{"Opacity", "[", "1.`", "]"}], ",", 
                   InterpretationBox[
                    ButtonBox[
                    TooltipBox[
                    RowBox[{
                    GraphicsBox[{{
                    GrayLevel[0], 
                    RectangleBox[{0, 0}]}, {
                    GrayLevel[0], 
                    RectangleBox[{1, -1}]}, {
                    RGBColor[0.922526, 0.385626, 0.209179], 
                    RectangleBox[{0, -1}, {2, 1}]}}, AspectRatio -> 1, Frame -> 
                    True, FrameStyle -> 
                    RGBColor[
                    0.6150173333333333, 0.25708400000000003`, 
                    0.13945266666666667`], FrameTicks -> None, 
                    PlotRangePadding -> None, ImageSize -> 
                    Dynamic[{
                    Automatic, 
                    1.35 (CurrentValue["FontCapHeight"]/AbsoluteCurrentValue[
                    Magnification])}]], "\[InvisibleSpace]"}], 
                    "RGBColor[0.922526, 0.385626, 0.209179]"], Appearance -> 
                    None, BaseStyle -> {}, BaselinePosition -> Baseline, 
                    DefaultBaseStyle -> {}, ButtonFunction :> 
                    With[{Typeset`box$ = EvaluationBox[]}, 
                    If[
                    Not[
                    AbsoluteCurrentValue["Deployed"]], 
                    SelectionMove[Typeset`box$, All, Expression]; 
                    FrontEnd`Private`$ColorSelectorInitialAlpha = 1; 
                    FrontEnd`Private`$ColorSelectorInitialColor = 
                    RGBColor[0.922526, 0.385626, 0.209179]; 
                    FrontEnd`Private`$ColorSelectorUseMakeBoxes = True; 
                    MathLink`CallFrontEnd[
                    FrontEnd`AttachCell[Typeset`box$, 
                    FrontEndResource["RGBColorValueSelector"], {
                    0, {Left, Bottom}}, {Left, Top}, 
                    "ClosingActions" -> {
                    "SelectionDeparture", "ParentChanged", 
                    "EvaluatorQuit"}]]]], BaseStyle -> Inherited, Evaluator -> 
                    Automatic, Method -> "Preemptive"], 
                    RGBColor[0.922526, 0.385626, 0.209179], Editable -> False,
                     Selectable -> False], ",", 
                   RowBox[{"AbsoluteThickness", "[", "1.6`", "]"}]}], "]"}], 
               ",", 
               RowBox[{"Directive", "[", 
                 RowBox[{
                   RowBox[{"Opacity", "[", "1.`", "]"}], ",", 
                   InterpretationBox[
                    ButtonBox[
                    TooltipBox[
                    RowBox[{
                    GraphicsBox[{{
                    GrayLevel[0], 
                    RectangleBox[{0, 0}]}, {
                    GrayLevel[0], 
                    RectangleBox[{1, -1}]}, {
                    RGBColor[0.528488, 0.470624, 0.701351], 
                    RectangleBox[{0, -1}, {2, 1}]}}, AspectRatio -> 1, Frame -> 
                    True, FrameStyle -> 
                    RGBColor[
                    0.3523253333333333, 0.3137493333333333, 
                    0.46756733333333333`], FrameTicks -> None, 
                    PlotRangePadding -> None, ImageSize -> 
                    Dynamic[{
                    Automatic, 
                    1.35 (CurrentValue["FontCapHeight"]/AbsoluteCurrentValue[
                    Magnification])}]], "\[InvisibleSpace]"}], 
                    "RGBColor[0.528488, 0.470624, 0.701351]"], Appearance -> 
                    None, BaseStyle -> {}, BaselinePosition -> Baseline, 
                    DefaultBaseStyle -> {}, ButtonFunction :> 
                    With[{Typeset`box$ = EvaluationBox[]}, 
                    If[
                    Not[
                    AbsoluteCurrentValue["Deployed"]], 
                    SelectionMove[Typeset`box$, All, Expression]; 
                    FrontEnd`Private`$ColorSelectorInitialAlpha = 1; 
                    FrontEnd`Private`$ColorSelectorInitialColor = 
                    RGBColor[0.528488, 0.470624, 0.701351]; 
                    FrontEnd`Private`$ColorSelectorUseMakeBoxes = True; 
                    MathLink`CallFrontEnd[
                    FrontEnd`AttachCell[Typeset`box$, 
                    FrontEndResource["RGBColorValueSelector"], {
                    0, {Left, Bottom}}, {Left, Top}, 
                    "ClosingActions" -> {
                    "SelectionDeparture", "ParentChanged", 
                    "EvaluatorQuit"}]]]], BaseStyle -> Inherited, Evaluator -> 
                    Automatic, Method -> "Preemptive"], 
                    RGBColor[0.528488, 0.470624, 0.701351], Editable -> False,
                     Selectable -> False], ",", 
                   RowBox[{"AbsoluteThickness", "[", "1.6`", "]"}]}], "]"}], 
               ",", 
               RowBox[{"Directive", "[", 
                 RowBox[{
                   RowBox[{"Opacity", "[", "1.`", "]"}], ",", 
                   InterpretationBox[
                    ButtonBox[
                    TooltipBox[
                    RowBox[{
                    GraphicsBox[{{
                    GrayLevel[0], 
                    RectangleBox[{0, 0}]}, {
                    GrayLevel[0], 
                    RectangleBox[{1, -1}]}, {
                    RGBColor[0.772079, 0.431554, 0.102387], 
                    RectangleBox[{0, -1}, {2, 1}]}}, AspectRatio -> 1, Frame -> 
                    True, FrameStyle -> 
                    RGBColor[
                    0.5147193333333333, 0.28770266666666666`, 
                    0.06825800000000001], FrameTicks -> None, 
                    PlotRangePadding -> None, ImageSize -> 
                    Dynamic[{
                    Automatic, 
                    1.35 (CurrentValue["FontCapHeight"]/AbsoluteCurrentValue[
                    Magnification])}]], "\[InvisibleSpace]"}], 
                    "RGBColor[0.772079, 0.431554, 0.102387]"], Appearance -> 
                    None, BaseStyle -> {}, BaselinePosition -> Baseline, 
                    DefaultBaseStyle -> {}, ButtonFunction :> 
                    With[{Typeset`box$ = EvaluationBox[]}, 
                    If[
                    Not[
                    AbsoluteCurrentValue["Deployed"]], 
                    SelectionMove[Typeset`box$, All, Expression]; 
                    FrontEnd`Private`$ColorSelectorInitialAlpha = 1; 
                    FrontEnd`Private`$ColorSelectorInitialColor = 
                    RGBColor[0.772079, 0.431554, 0.102387]; 
                    FrontEnd`Private`$ColorSelectorUseMakeBoxes = True; 
                    MathLink`CallFrontEnd[
                    FrontEnd`AttachCell[Typeset`box$, 
                    FrontEndResource["RGBColorValueSelector"], {
                    0, {Left, Bottom}}, {Left, Top}, 
                    "ClosingActions" -> {
                    "SelectionDeparture", "ParentChanged", 
                    "EvaluatorQuit"}]]]], BaseStyle -> Inherited, Evaluator -> 
                    Automatic, Method -> "Preemptive"], 
                    RGBColor[0.772079, 0.431554, 0.102387], Editable -> False,
                     Selectable -> False], ",", 
                   RowBox[{"AbsoluteThickness", "[", "1.6`", "]"}]}], "]"}]}],
              "}"}], ",", 
           RowBox[{"{", 
             RowBox[{#, ",", #2, ",", #3, ",", #4, ",", #5, ",", #6}], "}"}], 
           ",", 
           RowBox[{"LabelStyle", "\[Rule]", 
             RowBox[{"{", "}"}]}], ",", 
           RowBox[{"LegendLayout", "\[Rule]", "\"Column\""}]}], "]"}]& ), 
      Editable -> True], TraditionalForm], TraditionalForm]},
  "Legended",
  DisplayFunction->(GridBox[{{
      TagBox[
       ItemBox[
        PaneBox[
         TagBox[#, "SkipImageSizeLevel"], Alignment -> {Center, Baseline}, 
         BaselinePosition -> Baseline], DefaultBaseStyle -> "Labeled"], 
       "SkipImageSizeLevel"], 
      ItemBox[#2, DefaultBaseStyle -> "LabeledLabel"]}}, 
    GridBoxAlignment -> {"Columns" -> {{Center}}, "Rows" -> {{Center}}}, 
    AutoDelete -> False, GridBoxItemSize -> Automatic, 
    BaselinePosition -> {1, 1}]& ),
  Editable->True,
  InterpretationFunction->(RowBox[{"Legended", "[", 
     RowBox[{#, ",", 
       RowBox[{"Placed", "[", 
         RowBox[{#2, ",", "After"}], "]"}]}], "]"}]& )]], "Output"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Plot", "[", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{
     RowBox[{"VacuumEnergy", "[", "g", "]"}], ",", 
     RowBox[{"Energy", "[", "1", "]"}], ",", 
     RowBox[{"Energy", "[", "2", "]"}], ",", 
     RowBox[{"Energy", "[", "3", "]"}], ",", 
     RowBox[{"Energy", "[", "4", "]"}], ",", 
     RowBox[{"Energy", "[", "5", "]"}]}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"g", ",", "0", ",", "0.05"}], "}"}], ",", 
   RowBox[{"PlotLegends", "\[Rule]", " ", 
    RowBox[{"{", 
     RowBox[{
     "\"\<numerical\>\"", ",", "\"\<1st\>\"", ",", "\"\<2nd\>\"", ",", 
      "\"\<3rd\>\"", ",", "\"\<4th\>\"", ",", "\"\<5th\>\""}], "}"}]}]}], 
  "]"}]], "Input"],

Cell[BoxData[
 TemplateBox[{GraphicsBox[{{{}, {}, {
       Directive[
        Opacity[1.], 
        RGBColor[0.368417, 0.506779, 0.709798], 
        AbsoluteThickness[1.6]], 
       LineBox[CompressedData["
1:eJwV0Xs01GkYB3BDsogyhilRlO0ym2mi1Il8S6QoCetWorbZrJJRI2FRkUuy
JmmsDCG3VxI22rZaKl1sSUslt7xDsbS5pbSy6dcfz3nO55znPOc538dod6Cz
UFFBQWELU197bv81cX2cltUvq9a/n5zsROy37MoKzpDVhjP5M2cqULAtQ/KT
OQrYdJKYCxh73CcGkZwZcD1SftCX8adpdzyDOYbY63qrv4Zx/eWGMRFHAJ+F
y/KPsSjMWc/vHeCsRZkBK1RRiaJ/sWdpAMcJmdTokOJUioDDT7ck3fLFkkBu
Stg0Ch+N5Ebl136Q8f7TC2NTXFK072oIFGHHA8eH22ZS1HgtHi65I0Ze7Dlx
5hwKiQZbJaP+CN52Gl46ZEwRLGs2ncWPgK3O4x9v8CgGTXLUODePom/JoF2H
gELRYvvk/D+PI96vrzDRnMKwSmkyvD8G1ywstC5aUsheCo63D8dibsoJC09r
iu3ny3yVB+Nxw6Xr5y47CuPHc+wPcxOxOZqr17aFwqFY6+qgXhJqW8xMhS4U
EaMj/PJNyei1Ut3j7UkRuk/7mMFVCWynD5cIfCh2/xZY0lN1GgXjWQlpeyic
TNd3uDxKgf/ZnjX7/CneqrRstW04g5Bi7QdNgRTJnJ77rb2pKJ1+vbcwmMIl
QBbg+uksUo1tpB7hFPf0db53npBiWtK6n7KPUlgpKVuPsn/FxOzVxs6xFMK1
V0V63HScOqI7Q5bI5FnHrjfWPYfN67Wn35Uw/0t4zW/iZUBryCLSTUrxMTus
UdlEhridC1cez6AYH4gNb7fJhN8kv1onh8IgwdI5zT0LHJuxgbgCiqHQK/7P
3c9DV8zSlV6kePVkW227eTbUUzxS1copFkl4Ysnv2fDT2aWrX8Xk8755ddXK
HOx38JyveZ3C9cPaKaW3cuD2nh8urKGInpc4sNc6F278FTHb71Is75e0vavP
BX8XeVj5F8WajpC+dU4XcLPRW3i5gcL9sJmm4pMLmBAFSU8+pXhXXLwi1SsP
NmKzpXUtFL1qSpH/tOWhYnxKStFLCu+p3DpNv3zo7ddx6u9m7i2c5BUO52OV
2ol8jT6KO+K6S6yDBfgY2GTZ9Jb5d4ejGf//AuRsGJ43PkLRIaps0Y8qxNhO
b/faMYqC4iFpxOdC2CltFAZNUIxE9bqVxRfhYrT1m0iWHNKj1SvzVQnqqscf
CqbK4dMyX/W1OkHJxrJaW8aLTONbjTUJkv8W3vRi/Ee3U0Qem8Dws2FoDOPO
DV23L8wmCLG0W/Di67yGsmOuCcFoU7d9lIoc19Pt95zfRtCnaJBa/40cMSOl
KzpdCB4lNp7qYuzooK0y140gbOmLrWOM6UQbyfIiUN9xRt1IVY4pvgGDmT8Q
LKtUjRYzdlwgCZMFE4j2fgiarSYHN2rUoT2EYI76xHIBY9rsYaAfRqCcWm5i
y/hgglFNRiSBtGZ/3wHGaf9WKGfEEUQvN/vuNmNfW25zawKz/8WqgeeMeVnh
RXqnCDi7BnreML6x1cbhnISAJJFcHXU5ThQV6bemEDgvuyzkMXZkaQzMOkug
8OzKIjDmegVVe6YRdHv7G7kyphXPJOnpBKdZold+jL8AziFGew==
        "]]}, {
       Directive[
        Opacity[1.], 
        RGBColor[0.880722, 0.611041, 0.142051], 
        AbsoluteThickness[1.6]], 
       LineBox[CompressedData["
1:eJwV0Xs01HkYBvBhXLYLZUwNLZFMh9k1WUO2kseWOjU2uVSiLVJt4sgoVByX
3EJaQ8VqplHuX0XRxtaWRJdVSVvJus53kpRd16hdiv3tH+95z+ec54/3PO8i
/xCPveosFmsjM//v/P7rYU3H9Rz/EO5nRJHM51yr4g47yubMNjFgzHE4XJTB
ZeHH4YpV1oy3PSDGMdy5mKwci/RjPDm7wTucawq+Xdx4HeOmy80fJVxrHFmZ
039MjWKZ2sv7B7hOMF1/7wWbTdFv6V0RzHWDZNcioq1FERzxYuPJO37w9p2w
zJxN4auT8UyzNwDiv3bcvcihKFcXv2oOkUAj5HlNiQFFnY/lyKWGMHxRuz+B
b0Ih1eFoy5qOgCtLC7DlU4TLW20MhdEIWvOYf+8riiGrCzO5t+KQ1mPiYG9D
ob5y+/Ti2nhU6k0FrfiWwrSaPR3VnwizWZ8P3XakkHdbx3eOJEOelXymxZli
e94VP82hFHTrmc1tElOYP1kojuCdQOAP3f7r3ClcyvRqhhacxLQtv9nNiyJ6
bFRYuSEDXX+m63ftoDgapH/MuEaKpDCtSvEeCv+rIZfeVGfCVjEzYEMghZvN
mi7Px1kQnlr6sVFCMaDdtmlt8ylYz1fmdURQZHDfPGjvO42oPZdNQ6IpPIPl
wZsnz0BaljnRk0Bx32jeFo9P2SiPmRp4l0rhyNZcPcb5GfoL06+HSin2OtVI
FvByEe9SqEjLZvps5DSZzz+LG5InnYly5n+pvcLnAhkcdhm+ZxVQ/HM+8pmm
lRz3lYNmuoRiYjA5qtP5HM5a9grOXaYwTnXwyPFSwD5o4IRONcXw0V8CX3rl
oXNFnNacmxSvn7rf7Vx2Ho9aRN5Z9RQWUkGY9NfzqJ8KvVf8O9PPeOuKavsL
iDepT5M/odj8wUmj4s4FpM8SWXBbKBLMTgzuW50PRZJQadRBYdsv7XjflI+J
h+IDJSqKVV2H333nVgBfxcj8hj4KrwiRrvrTAqTvLzOoG6B4X1Zmd9qnEGxW
4qjjGEXfTHbM245CJJi/7XWdoNihxWvUDSjC99zgSspS4XXJtKBkpAjKSfZL
T20VGsIay9UOFuNoKJ+7TUcF8y5XkfBzMXZW7RN066vQJbnWZhRbAvExW59x
QxWKy4azo6dKwFqv3zNgosJobN/WKymlmG7jOAcsUSE77rZ90QwC7Ql57aGv
VfBtWzyjdxaBdUXwzZ8YW9iktJvrEvj4O94gjG/0uEUXcgjKHyqvKRkr172q
L/iSwENmVu5ixeR1NF3zrQgUDqWyxUIVfssV78lzJ7CLvXrk+VIVEkcr7JSe
BDtFiRGDjF1d9LVNthKk9G0Om2GtAv3UQRQ+BO2bPoSAsYZf8NC53QRxpsv3
XWTsukQaKQ8neHyndkvCNyrwYsdcOg8TjIdneCoY09ZtxkaRBAsFfu7XGR9M
XVQniyEIzVLbOMQ45+8qTdlxAt5uZ+ftNir4reW1tqcSOPHmrY5gLFBElS5I
Jwh81ItMxjc3ObuclRLcEh1f+YBxUmmpUXsWwZs+r+WvGLuq6QwaniGYK7ew
/8yY5xN62zuHYLnbv7YGIuaeqhZpbi6Bv8ZDGxHj/wCG3kuI
        "]]}, {
       Directive[
        Opacity[1.], 
        RGBColor[0.560181, 0.691569, 0.194885], 
        AbsoluteThickness[1.6]], 
       LineBox[CompressedData["
1:eJwV0ms01HkYB3CGyUYcpqkhlyQlszhziNpNfWdL7YnjEkWNE0oXl3XZloSD
RErClJT7bWL8x2WLbbS1LVI07c5OW5bc8jtWyFaG2OSy9t+L5zznc873xXOe
51l3NMLzOENFRcWVrs+9YvznKPkFvR1/2gTTIkjbwLrTwFbuuGffZKJPm+UY
U5nNVoEky9eRR/tgB2WcyNZFoYM4OoD2/Iq2Q9FsUzzycyYttOU/Kj5Gsnmw
023JTVYlcFDtag9n83HpY1AyQ41g3PJQfRjbA6tf3vJlLCMIO93pmtkagMDD
I+viVhD4a2c/Z74Owg/eT96dYRHUMZyHFBGRyIhq13PXJ2gRWE7WtkVhj5e+
Zp4JgVCbpVEoP4Neonc5xJwguqjb1sAmAQdC2lrruAQT1uWa7AdnoYzPSOrg
ETC2+S6t//UcmjoSBGEOBKZStaX48VQEfynZlexIUPSKd65/Mg1XlkwKzHYS
+JbeCmBOXMR3T0b0i78lMP/DxPk0JwM1BlZ6Ka4ELhK9pok1mWjMHZ6d9SRI
mJ6yub03G9QcY9vLgwSxoSuTjZuEyP/E6Mr2IzjaGFE7Ir2CIetNUz2BBB62
uwa8fr+KxSnW40vBBO80etx3K3JQyN8XpAgnyGaPdPSOXoNVjKzsTBSBV1hR
2P75XNzhK3nzsQTtRqsOeC5cx2bbFRvZSQQ71Jg7p1l5GFB/sDw7leA4vyly
DScfEzGrROXp9D5lLLn56gIMhifbC7Po+6W/tnnBLYQop8T0VQ7BbFncc6Z1
EZws6pza8wjm3qfF9zsVY8OJ4+VaJQTG6Y6eN3xKwLM+ZmElIlDG/hTS5VMK
WfKG2DdiguFn+x71O5ThLlPmp1pHsEnIjRLeLUNa7aR/0216PzPdX0u3lMN8
2Yili5Rg/7989frWcpSelH5wuE+QYpbx/uTOCtR8dV7Q1kyweVzY90Fegb3S
2orKRwTbB2LefOMhwmo/gx5LGYHPaTsdxjMRrFQWOi/LCT5IJPbXBDdRH21l
uOs5waimWuJY301Mz2p2FncRHF7GkekEVUL5BtK9ffS84iWueLISWVsfZr4Y
JGiLktWpnqpCa8dQqPowfe8BNzubxSoozUPlKWMEA5F3eoySxOAVjJ2veUtQ
JVFeT/hPDC2jI52NSoKppFHvWxerUWDK1vtihuD62eYtlcsp6JTut9D4RP93
z/rlr7UopGiFjurT3mR7sddch8KRudoqLu17f3sk3GRR8PJvNHOjPbhn6KHI
kILunqe6uZ/z2ky3CmsKM2mmj83mCO7nOx8r3UchsCs8a/s8QepUvf2gFwVZ
9oydO203l5Uaa70p5P0mfhlAmyz0USUCCiFPs9am0lYPCJsoDqTwz/AJ0dPP
+Y3CuKJoCt6ZSPFZIOAkTbv0x1CwD0s0DKZNug8aG8VRiFeMNcTRPpW+rqUw
kQIM+YPFtG+8bWAWXqBQoZi1HqYdsJvT3ZtO4cHDnOYZ2tyS+Oo1lynwtwo8
NBYJfnF3cikQUkiQBEdwaZ+vrjbqvUrBQdywuI22m6r2e4NcCj4bLTJcaXME
3zcfukFh0KKD40+bNPwlzM+noKi5Koqk/T9QemPZ
        "]]}, {
       Directive[
        Opacity[1.], 
        RGBColor[0.922526, 0.385626, 0.209179], 
        AbsoluteThickness[1.6]], 
       LineBox[CompressedData["
1:eJwV0Xs01GkYB3AzbqlGmWabmjXSIrJypNAp+cqqdmk0mkqoRWW7WBvFVCSR
VTZlkmvG/TJ+U2FtkW5EadVOumwsRt5JpaUoXbR02bc/3vOczznP85z3fJ+Z
G3esCmZraWmJ6PtSi/rrIlSHjFzu2m6jIki04J6r5r10+XFBrck0aq7z7tIU
nhZuFfs721Gvu8EI9/MmI69HERlIPTaxyTeSZ4r7ZStJA7WqsnUkjGeHJHF/
ehyLwJHV1vwLzxWV/12IZ2sT9M/2rQjliWHU5/oTW48gVPq36OjVQOw7mRAX
NZEggJNyT/fJVrwN8beM4hKcYXs8at0Rhozsu73e0wga/Ga/Ot0UgcV1LXW5
JgQyDlc/R7UHSzYKF0eYE0TK2+2n28bAw7cq6bI1wdCcwvG8ywcg9RqV9NgR
sBf5fza7Eo8N83VajzkSmNZof47uT4DAxryx0plA/tAuXv0qETXcj6MBbgT+
+VWBukOH4R+0TD2wnMD8tomHlH8Ex9iTBM9EBJ5Ko9ohwVEs27PAVSohiHkz
bPv7DynwVlmpd/kS7A2ZEiesleGAhU6FKIBg4x87Tj+tOQ6mo+HZxc0EYvvv
uiV/pcLJ5sHNrO0EL/Q7Vi5tPYE7f/ZumRBGkMJ7eqOzLw0JgQHjnkcSSELl
oavH0pFywv21Mpqg2firNas+ZMD+xBUVK47ARVvX7Q03C4PXF85tTiQIdq0N
E/CzYRo5VmmWTPNs4arMp57E2OMVd4OO0/slPbG9b50Dt0P5czmZBO8Lou7p
zpHjnMvyRImcYHQwMVrtnot4QXiPXhGBMMl5VaZPHnYF5J/KURC83Ht2e5tP
PgTJo0LVaYLHd7yvqR0LwFWwckOqCaxk1hGy8wUQFUw9pail+bxtX1jjVAgD
y7pPzZcIVr9z1am4Woj1NaZpkkaCg98cGdziVgSHs+e5sTcI5vfLul6rihDU
N5M1TUWwuHv3v0vExRi/c0uO+B6Bj3SeIftOMYyV3QOh7QSvlUqHNL8S1Dc6
zPtPTdA3Xnv/s64SjGQFO1o8Itigx28x3FoK2xVNhfV99L+Kz9aKV6Uo+T69
DS8ImiJazrB2lsGubEQZNEzv3e01z/ZjGW5n1F8fHiHoDjvXYRyrQJBGV2vW
R4Iy5cuMmE8K2LzbpOPK1mA4tm9t1eFy/PybzZqb+hpkHKh3KjVgcKWkaO3I
RA0COswMnkxg4K68LzXkaGBlf7jT3JBBQbhlpgX1hV5xTAmXgTLP6R8Jdc+y
R43FXzOo6/ngX/Wln6PrVTSHQXyI7fZthhpczPbYnO/NYAm/pbBrkgYJwxUO
PRI6vz6meZjay3OK/oy1DMKtfQYMJmtAPnQxeX4MemujHRZQ6wSGDuVuYiAc
d+RWGrXXLFmUPJLB8YEHn0RGGvBj33iqd9P9M15YBFOT9nVC4ygGqS7fivZR
70ya2ZCzn8HTLGO5kjrzebVuziEGHOu2RfpcDQKX8ts7kxhIrSw3m1Bb50WX
C5IZmF3LT3agvrTS3fOkjAHXZtzDTdS/lpcbd6bSvMzf60VTe7E4g9PTGcRM
mWyXSs33C6/3zWRw0FDsy1CT6gey7GwGOjbn4xuo/we4tko1
        "]]}, {
       Directive[
        Opacity[1.], 
        RGBColor[0.528488, 0.470624, 0.701351], 
        AbsoluteThickness[1.6]], 
       LineBox[CompressedData["
1:eJwV0XtUzHkYBvCaTLbWJGOYaqdCraOcUqzchifR7jLbnXRBSa0WmdZ2oU4N
ISXVSCaZSU1Nl1+5JDu1bBQ6EZtR20q39R02MUsXCru07dcf73nP55znj/e8
z+wwsW8ES09Pz4POp12suxLTenTaqjbHH6gIUr/kqmt4w6u2Lq2zMqPmCuNL
s3l6aCkJFjpRB9xmLJN5ppA8Lo8Npf4w5VZgLG8WBlRepJG69aLmXTTPCU3O
ulMH9Qlc9B827+G5YgfnSgrLgEBnF3ghiueNojlzvmcZEkTFdXhk3giF3MZV
kjCFIIST3c7uj4R4mZcggUtwnrX+iUYcjQ7z5Td9zAgag+xGzt2KgUvHivAC
KwIphztZ3roPxhVDWT/ZEsQqOheaOyahv4AtrLcnGHJQGvOuHYB6y+C0PicC
1orgCZvrKVgj9nHLcCGYVWswkag7jOQjPa6VQgLFn04pvSOp6FLMYAW4EQQX
Voeyh9JwumrddfINge19q/Vx/Az4yoPljzwIRJXT6oYsMtFtY6XY5keQNPra
8dK6bDSHKEUBgQT7d00/aFknxd2nqka7EIKwy+Jzz2pPII9tnZcVTuC9cE2f
3285KMke9g3ZSfBqcpeXu+YkjDNVaU1igmzes9vdA7mYXWE2KzeWwC9KEbXh
wylciguVIJGgWTBjo+9HGWrapvgfPECwyoDtNso9jXe1Gvn8VIII17poC34+
zjvne0Rl0H+2cFttZ57BZx/rLx2X0v7S+x1/t5cjbHtVMldG8L4ooZ3toMCd
/pv7nOUE/w6mJvauLUD89btW6iICy3Shb96ms5gfKMybUUYwvP/nnQ83FWLN
w/Yx0yqCvx74NPW6FGHYe7x8TzXBPKl9jPSXIsBEzYlU0/+MdS6vXaJEYa1c
i6sEG966TrpwQ4kXbQNZ8Q0Eh+ZkDO5wK0ZReUbGd00EX+mkPW9ai+Hs2rBW
0kKwsi/+xWrvEqj0XKZ63yfYFLfIhPWgBFmvpnpNtBO8qaxcnBukwvMun11G
jwgGjA2Sn/eoYG66xOVyL8EWQ36LSWQpWG9n7lZr6b3lE/blI6WQew9sz31G
cCum5bz+3jJYj1+8xvub9t3nuchxvAz72oL5uiGCvmh1l0BSjhSlQLZtlKCs
cliW9F85hDdSttS9J3gtGfCvTqvA7ghZytZxAtmBhiWlRgzu3Wk5wehrEdJl
Y9T/OYOw8BxlPfW8hWndtiYMbL0iqjXUV596J6m4DMRKu3tj1I+/fnKz5AsG
XBu7sdUsmuewPYsdGMT06Cy6qX/NXx9e6MNgZFnNOaNJWhx+fWHxYz8Gw/cy
CwTUnqLpk639GdRaRWcuoCYfe5izQQxkJ7/duZF6UmjUUMF2BppcdzPlp/xc
aYIiloFfnb/7UrYWfMmoqDeeQUe7dK6ImnQGWAoSGFhrNYZbqfemz26UJzP4
Z9vmpkPUeS9r2PKjDF42Vy3SUIe68zu70xnwRcacJ9T2ZxMrLI4z2Ny1u3+U
ut5rreiMlMGxwpUnLQy1OFJRIejOYWBSUBXpQO2pzxk0P8XgxQLLla7U/KAf
GwLzGPQtzzH1oyY1f0jz8xlUFhk/jaD+Hw+bTXA=
        "]]}, {
       Directive[
        Opacity[1.], 
        RGBColor[0.772079, 0.431554, 0.102387], 
        AbsoluteThickness[1.6]], 
       LineBox[CompressedData["
1:eJwV0Xs01HkYBnDG7eS2mWaTogu2w5zTHKuirHpsq7ZS7mE4mVHZWhlsGcJh
pBI7a80mWs24X8ZP0mU3s5JNSaVtsLpMhlnfqaiddg2R3ZLsb/94z3s+/z3n
eVbsSQyOZRgYGOyk7/9frWtNVp602fg752taBLmfMK9cZo1vjF6nWLqINtM7
ta6QZYDumihvN9oRdyiHLNZ8iIblQj7tGctOrpC1HC9rA0gHbeWF3n+SWG7Q
fqorPmpI4GH4+HYCywez81tzGEYEOldus4AViMhix68YpgSClIc7C27wkXkz
SJRuScCzKuw3GTmAV7d77NOZBOcZ25/2JiZhA7xuBS0i6Ih0nWjqTIZN1bm4
sqUEEiummVR5BGOjJeWHnQmEMpW7HScTx+7u4F1jE+hXVZmz2rPBNbaM0LgR
MD6LmnP6NQcBNu3tYg+C5S1Gcxm646jelrLwnDeB7A+3nKGJXDDuCnZxNxFE
VVzkm+jzcD/2WMfTLwmce5ZuT7EVI2ylff3gTgK/RhuFfnEBriul07EhBJlT
rzmXthVCpywUR3MJ0g4uOOqgkCB+691kdx7Bnp8Sm0ZbfkACU7H+7D6CQPcv
NCH3TyGob/lMQhzB32YDAZt7i3BkfaP2SSJBIWv0jvrFaYzxm242CwlCBDJB
6Ewxvh82bIvJILht//Gu4PclyDRd4tiUTbDRyGTTFPNHTF+JFsfkEsT6KJIW
25biRPxcxQUx3Wc3U+m88CyKJw9t1kjo/fJHOA/YUvgdKFgjLCH4tzK932SV
DIF1/Tq5lODdWG7GkG8ZeHGesb5VBA753sFnwssR4xi24kY9wXjaz3GPwyvA
jg1sVZ8jeN4XdGvIoxKh3TNDuy8RuEjYyZJfKqF21ZuLWuh+3qi8WjyrYPeR
ryqnjSB02se4+UYVTr6yCB3vIDjmKB7bv6ka/ardXbNdBGt0ksFJZTW2tKc5
CX8j2KBJ/fPzwBqk3surKugjCE9Zbc3oq4F/19uX3z4imGxsXHs6shYx4gaz
STXBC3OjrJeDtZC98pn/YZhgt6ltt/WBOsRRy1TJI3Re+RxbPlGHC1wXYb+O
oDO5+7zhoXqIpnqaH+rpvTX+qzmz9XgcZlPq+4ZAk3RlwF4kB8fc0yPyHUF9
43hJ5gc53OoPP4mYI3gtehF2Ma8B5c/0bXeMtSjJvu5ZN4/C+Mqiq5p5WvAG
nOaNWFCI9+alT9F2cc9TO1tTEL7N87Iw1+Lqs8DMWiYF7v7ptnW0h7c8vVmz
hIKroU9nEW0XKxP/6lUU+Pe6nm2z0KKtdPu+iiAKImZUQoulFsdfN68dDqFg
Jwv3UtL291tgtiyMwn5lq+lz2uT9IFUeScGXfbHSxkoLY75AX7aXgnrk4EA8
bf+VknSZkEJRcES0k7UWtqIpv6FUCnt6lW5etIkqwsE+nYK56XmjINqH8ld0
SLMoLJFTjVm0z/x12UR6ksLW0erZAdr8zbYqdT4FKU/wYJw2uzyjYfF3FA5W
1lBmH2lxLcDX76yEwvrs0PC1tE80NNirT1Fwr+nl7KDtb2g1ZldMoaxYYbqX
tm3kN9e5ZyiUlFoOp9Emlx9JSkspbJnoUUho/weE3FaW
        "]]}}}, {
    DisplayFunction -> Identity, AspectRatio -> 
     NCache[GoldenRatio^(-1), 0.6180339887498948], Axes -> {True, True}, 
     AxesLabel -> {None, None}, AxesOrigin -> {0, 0.498}, DisplayFunction :> 
     Identity, Frame -> {{False, False}, {False, False}}, 
     FrameLabel -> {{None, None}, {None, None}}, 
     FrameTicks -> {{Automatic, Automatic}, {Automatic, Automatic}}, 
     GridLines -> {None, None}, GridLinesStyle -> Directive[
       GrayLevel[0.5, 0.4]], 
     Method -> {
      "DefaultBoundaryStyle" -> Automatic, "ScalingFunctions" -> None}, 
     PlotRange -> {{0, 0.05}, {0.49999998999999995`, 0.5374999992346938}}, 
     PlotRangeClipping -> True, PlotRangePadding -> {{
        Scaled[0.02], 
        Scaled[0.02]}, {
        Scaled[0.05], 
        Scaled[0.05]}}, Ticks -> {Automatic, Automatic}}],FormBox[
    FormBox[
     TemplateBox[{
      "\"numerical\"", "\"1st\"", "\"2nd\"", "\"3rd\"", "\"4th\"", "\"5th\""},
       "LineLegend", DisplayFunction -> (FormBox[
        StyleBox[
         StyleBox[
          PaneBox[
           TagBox[
            GridBox[{{
               TagBox[
                GridBox[{{
                   GraphicsBox[{{
                    Directive[
                    EdgeForm[
                    Directive[
                    Opacity[0.3], 
                    GrayLevel[0]]], 
                    PointSize[0.5], 
                    Opacity[1.], 
                    RGBColor[0.368417, 0.506779, 0.709798], 
                    AbsoluteThickness[1.6]], {
                    LineBox[{{0, 10}, {20, 10}}]}}, {
                    Directive[
                    EdgeForm[
                    Directive[
                    Opacity[0.3], 
                    GrayLevel[0]]], 
                    PointSize[0.5], 
                    Opacity[1.], 
                    RGBColor[0.368417, 0.506779, 0.709798], 
                    AbsoluteThickness[1.6]], {}}}, AspectRatio -> Full, 
                    ImageSize -> {20, 10}, PlotRangePadding -> None, 
                    ImagePadding -> Automatic, 
                    BaselinePosition -> (Scaled[0.1] -> Baseline)], #}, {
                   GraphicsBox[{{
                    Directive[
                    EdgeForm[
                    Directive[
                    Opacity[0.3], 
                    GrayLevel[0]]], 
                    PointSize[0.5], 
                    Opacity[1.], 
                    RGBColor[0.880722, 0.611041, 0.142051], 
                    AbsoluteThickness[1.6]], {
                    LineBox[{{0, 10}, {20, 10}}]}}, {
                    Directive[
                    EdgeForm[
                    Directive[
                    Opacity[0.3], 
                    GrayLevel[0]]], 
                    PointSize[0.5], 
                    Opacity[1.], 
                    RGBColor[0.880722, 0.611041, 0.142051], 
                    AbsoluteThickness[1.6]], {}}}, AspectRatio -> Full, 
                    ImageSize -> {20, 10}, PlotRangePadding -> None, 
                    ImagePadding -> Automatic, 
                    BaselinePosition -> (Scaled[0.1] -> Baseline)], #2}, {
                   GraphicsBox[{{
                    Directive[
                    EdgeForm[
                    Directive[
                    Opacity[0.3], 
                    GrayLevel[0]]], 
                    PointSize[0.5], 
                    Opacity[1.], 
                    RGBColor[0.560181, 0.691569, 0.194885], 
                    AbsoluteThickness[1.6]], {
                    LineBox[{{0, 10}, {20, 10}}]}}, {
                    Directive[
                    EdgeForm[
                    Directive[
                    Opacity[0.3], 
                    GrayLevel[0]]], 
                    PointSize[0.5], 
                    Opacity[1.], 
                    RGBColor[0.560181, 0.691569, 0.194885], 
                    AbsoluteThickness[1.6]], {}}}, AspectRatio -> Full, 
                    ImageSize -> {20, 10}, PlotRangePadding -> None, 
                    ImagePadding -> Automatic, 
                    BaselinePosition -> (Scaled[0.1] -> Baseline)], #3}, {
                   GraphicsBox[{{
                    Directive[
                    EdgeForm[
                    Directive[
                    Opacity[0.3], 
                    GrayLevel[0]]], 
                    PointSize[0.5], 
                    Opacity[1.], 
                    RGBColor[0.922526, 0.385626, 0.209179], 
                    AbsoluteThickness[1.6]], {
                    LineBox[{{0, 10}, {20, 10}}]}}, {
                    Directive[
                    EdgeForm[
                    Directive[
                    Opacity[0.3], 
                    GrayLevel[0]]], 
                    PointSize[0.5], 
                    Opacity[1.], 
                    RGBColor[0.922526, 0.385626, 0.209179], 
                    AbsoluteThickness[1.6]], {}}}, AspectRatio -> Full, 
                    ImageSize -> {20, 10}, PlotRangePadding -> None, 
                    ImagePadding -> Automatic, 
                    BaselinePosition -> (Scaled[0.1] -> Baseline)], #4}, {
                   GraphicsBox[{{
                    Directive[
                    EdgeForm[
                    Directive[
                    Opacity[0.3], 
                    GrayLevel[0]]], 
                    PointSize[0.5], 
                    Opacity[1.], 
                    RGBColor[0.528488, 0.470624, 0.701351], 
                    AbsoluteThickness[1.6]], {
                    LineBox[{{0, 10}, {20, 10}}]}}, {
                    Directive[
                    EdgeForm[
                    Directive[
                    Opacity[0.3], 
                    GrayLevel[0]]], 
                    PointSize[0.5], 
                    Opacity[1.], 
                    RGBColor[0.528488, 0.470624, 0.701351], 
                    AbsoluteThickness[1.6]], {}}}, AspectRatio -> Full, 
                    ImageSize -> {20, 10}, PlotRangePadding -> None, 
                    ImagePadding -> Automatic, 
                    BaselinePosition -> (Scaled[0.1] -> Baseline)], #5}, {
                   GraphicsBox[{{
                    Directive[
                    EdgeForm[
                    Directive[
                    Opacity[0.3], 
                    GrayLevel[0]]], 
                    PointSize[0.5], 
                    Opacity[1.], 
                    RGBColor[0.772079, 0.431554, 0.102387], 
                    AbsoluteThickness[1.6]], {
                    LineBox[{{0, 10}, {20, 10}}]}}, {
                    Directive[
                    EdgeForm[
                    Directive[
                    Opacity[0.3], 
                    GrayLevel[0]]], 
                    PointSize[0.5], 
                    Opacity[1.], 
                    RGBColor[0.772079, 0.431554, 0.102387], 
                    AbsoluteThickness[1.6]], {}}}, AspectRatio -> Full, 
                    ImageSize -> {20, 10}, PlotRangePadding -> None, 
                    ImagePadding -> Automatic, 
                    BaselinePosition -> (Scaled[0.1] -> Baseline)], #6}}, 
                 GridBoxAlignment -> {
                  "Columns" -> {Center, Left}, "Rows" -> {{Baseline}}}, 
                 AutoDelete -> False, 
                 GridBoxDividers -> {
                  "Columns" -> {{False}}, "Rows" -> {{False}}}, 
                 GridBoxItemSize -> {"Columns" -> {{All}}, "Rows" -> {{All}}},
                  GridBoxSpacings -> {
                  "Columns" -> {{0.5}}, "Rows" -> {{0.8}}}], "Grid"]}}, 
             GridBoxAlignment -> {"Columns" -> {{Left}}, "Rows" -> {{Top}}}, 
             AutoDelete -> False, 
             GridBoxItemSize -> {
              "Columns" -> {{Automatic}}, "Rows" -> {{Automatic}}}, 
             GridBoxSpacings -> {"Columns" -> {{1}}, "Rows" -> {{0}}}], 
            "Grid"], Alignment -> Left, AppearanceElements -> None, 
           ImageMargins -> {{5, 5}, {5, 5}}, ImageSizeAction -> 
           "ResizeToFit"], LineIndent -> 0, StripOnInput -> False], {
         FontFamily -> "Arial"}, Background -> Automatic, StripOnInput -> 
         False], TraditionalForm]& ), 
      InterpretationFunction :> (RowBox[{"LineLegend", "[", 
         RowBox[{
           RowBox[{"{", 
             RowBox[{
               RowBox[{"Directive", "[", 
                 RowBox[{
                   RowBox[{"Opacity", "[", "1.`", "]"}], ",", 
                   InterpretationBox[
                    ButtonBox[
                    TooltipBox[
                    RowBox[{
                    GraphicsBox[{{
                    GrayLevel[0], 
                    RectangleBox[{0, 0}]}, {
                    GrayLevel[0], 
                    RectangleBox[{1, -1}]}, {
                    RGBColor[0.368417, 0.506779, 0.709798], 
                    RectangleBox[{0, -1}, {2, 1}]}}, AspectRatio -> 1, Frame -> 
                    True, FrameStyle -> 
                    RGBColor[
                    0.24561133333333335`, 0.3378526666666667, 
                    0.4731986666666667], FrameTicks -> None, PlotRangePadding -> 
                    None, ImageSize -> 
                    Dynamic[{
                    Automatic, 
                    1.35 (CurrentValue["FontCapHeight"]/AbsoluteCurrentValue[
                    Magnification])}]], "\[InvisibleSpace]"}], 
                    "RGBColor[0.368417, 0.506779, 0.709798]"], Appearance -> 
                    None, BaseStyle -> {}, BaselinePosition -> Baseline, 
                    DefaultBaseStyle -> {}, ButtonFunction :> 
                    With[{Typeset`box$ = EvaluationBox[]}, 
                    If[
                    Not[
                    AbsoluteCurrentValue["Deployed"]], 
                    SelectionMove[Typeset`box$, All, Expression]; 
                    FrontEnd`Private`$ColorSelectorInitialAlpha = 1; 
                    FrontEnd`Private`$ColorSelectorInitialColor = 
                    RGBColor[0.368417, 0.506779, 0.709798]; 
                    FrontEnd`Private`$ColorSelectorUseMakeBoxes = True; 
                    MathLink`CallFrontEnd[
                    FrontEnd`AttachCell[Typeset`box$, 
                    FrontEndResource["RGBColorValueSelector"], {
                    0, {Left, Bottom}}, {Left, Top}, 
                    "ClosingActions" -> {
                    "SelectionDeparture", "ParentChanged", 
                    "EvaluatorQuit"}]]]], BaseStyle -> Inherited, Evaluator -> 
                    Automatic, Method -> "Preemptive"], 
                    RGBColor[0.368417, 0.506779, 0.709798], Editable -> False,
                     Selectable -> False], ",", 
                   RowBox[{"AbsoluteThickness", "[", "1.6`", "]"}]}], "]"}], 
               ",", 
               RowBox[{"Directive", "[", 
                 RowBox[{
                   RowBox[{"Opacity", "[", "1.`", "]"}], ",", 
                   InterpretationBox[
                    ButtonBox[
                    TooltipBox[
                    RowBox[{
                    GraphicsBox[{{
                    GrayLevel[0], 
                    RectangleBox[{0, 0}]}, {
                    GrayLevel[0], 
                    RectangleBox[{1, -1}]}, {
                    RGBColor[0.880722, 0.611041, 0.142051], 
                    RectangleBox[{0, -1}, {2, 1}]}}, AspectRatio -> 1, Frame -> 
                    True, FrameStyle -> 
                    RGBColor[
                    0.587148, 0.40736066666666665`, 0.09470066666666668], 
                    FrameTicks -> None, PlotRangePadding -> None, ImageSize -> 
                    Dynamic[{
                    Automatic, 
                    1.35 (CurrentValue["FontCapHeight"]/AbsoluteCurrentValue[
                    Magnification])}]], "\[InvisibleSpace]"}], 
                    "RGBColor[0.880722, 0.611041, 0.142051]"], Appearance -> 
                    None, BaseStyle -> {}, BaselinePosition -> Baseline, 
                    DefaultBaseStyle -> {}, ButtonFunction :> 
                    With[{Typeset`box$ = EvaluationBox[]}, 
                    If[
                    Not[
                    AbsoluteCurrentValue["Deployed"]], 
                    SelectionMove[Typeset`box$, All, Expression]; 
                    FrontEnd`Private`$ColorSelectorInitialAlpha = 1; 
                    FrontEnd`Private`$ColorSelectorInitialColor = 
                    RGBColor[0.880722, 0.611041, 0.142051]; 
                    FrontEnd`Private`$ColorSelectorUseMakeBoxes = True; 
                    MathLink`CallFrontEnd[
                    FrontEnd`AttachCell[Typeset`box$, 
                    FrontEndResource["RGBColorValueSelector"], {
                    0, {Left, Bottom}}, {Left, Top}, 
                    "ClosingActions" -> {
                    "SelectionDeparture", "ParentChanged", 
                    "EvaluatorQuit"}]]]], BaseStyle -> Inherited, Evaluator -> 
                    Automatic, Method -> "Preemptive"], 
                    RGBColor[0.880722, 0.611041, 0.142051], Editable -> False,
                     Selectable -> False], ",", 
                   RowBox[{"AbsoluteThickness", "[", "1.6`", "]"}]}], "]"}], 
               ",", 
               RowBox[{"Directive", "[", 
                 RowBox[{
                   RowBox[{"Opacity", "[", "1.`", "]"}], ",", 
                   InterpretationBox[
                    ButtonBox[
                    TooltipBox[
                    RowBox[{
                    GraphicsBox[{{
                    GrayLevel[0], 
                    RectangleBox[{0, 0}]}, {
                    GrayLevel[0], 
                    RectangleBox[{1, -1}]}, {
                    RGBColor[0.560181, 0.691569, 0.194885], 
                    RectangleBox[{0, -1}, {2, 1}]}}, AspectRatio -> 1, Frame -> 
                    True, FrameStyle -> 
                    RGBColor[
                    0.37345400000000006`, 0.461046, 0.12992333333333334`], 
                    FrameTicks -> None, PlotRangePadding -> None, ImageSize -> 
                    Dynamic[{
                    Automatic, 
                    1.35 (CurrentValue["FontCapHeight"]/AbsoluteCurrentValue[
                    Magnification])}]], "\[InvisibleSpace]"}], 
                    "RGBColor[0.560181, 0.691569, 0.194885]"], Appearance -> 
                    None, BaseStyle -> {}, BaselinePosition -> Baseline, 
                    DefaultBaseStyle -> {}, ButtonFunction :> 
                    With[{Typeset`box$ = EvaluationBox[]}, 
                    If[
                    Not[
                    AbsoluteCurrentValue["Deployed"]], 
                    SelectionMove[Typeset`box$, All, Expression]; 
                    FrontEnd`Private`$ColorSelectorInitialAlpha = 1; 
                    FrontEnd`Private`$ColorSelectorInitialColor = 
                    RGBColor[0.560181, 0.691569, 0.194885]; 
                    FrontEnd`Private`$ColorSelectorUseMakeBoxes = True; 
                    MathLink`CallFrontEnd[
                    FrontEnd`AttachCell[Typeset`box$, 
                    FrontEndResource["RGBColorValueSelector"], {
                    0, {Left, Bottom}}, {Left, Top}, 
                    "ClosingActions" -> {
                    "SelectionDeparture", "ParentChanged", 
                    "EvaluatorQuit"}]]]], BaseStyle -> Inherited, Evaluator -> 
                    Automatic, Method -> "Preemptive"], 
                    RGBColor[0.560181, 0.691569, 0.194885], Editable -> False,
                     Selectable -> False], ",", 
                   RowBox[{"AbsoluteThickness", "[", "1.6`", "]"}]}], "]"}], 
               ",", 
               RowBox[{"Directive", "[", 
                 RowBox[{
                   RowBox[{"Opacity", "[", "1.`", "]"}], ",", 
                   InterpretationBox[
                    ButtonBox[
                    TooltipBox[
                    RowBox[{
                    GraphicsBox[{{
                    GrayLevel[0], 
                    RectangleBox[{0, 0}]}, {
                    GrayLevel[0], 
                    RectangleBox[{1, -1}]}, {
                    RGBColor[0.922526, 0.385626, 0.209179], 
                    RectangleBox[{0, -1}, {2, 1}]}}, AspectRatio -> 1, Frame -> 
                    True, FrameStyle -> 
                    RGBColor[
                    0.6150173333333333, 0.25708400000000003`, 
                    0.13945266666666667`], FrameTicks -> None, 
                    PlotRangePadding -> None, ImageSize -> 
                    Dynamic[{
                    Automatic, 
                    1.35 (CurrentValue["FontCapHeight"]/AbsoluteCurrentValue[
                    Magnification])}]], "\[InvisibleSpace]"}], 
                    "RGBColor[0.922526, 0.385626, 0.209179]"], Appearance -> 
                    None, BaseStyle -> {}, BaselinePosition -> Baseline, 
                    DefaultBaseStyle -> {}, ButtonFunction :> 
                    With[{Typeset`box$ = EvaluationBox[]}, 
                    If[
                    Not[
                    AbsoluteCurrentValue["Deployed"]], 
                    SelectionMove[Typeset`box$, All, Expression]; 
                    FrontEnd`Private`$ColorSelectorInitialAlpha = 1; 
                    FrontEnd`Private`$ColorSelectorInitialColor = 
                    RGBColor[0.922526, 0.385626, 0.209179]; 
                    FrontEnd`Private`$ColorSelectorUseMakeBoxes = True; 
                    MathLink`CallFrontEnd[
                    FrontEnd`AttachCell[Typeset`box$, 
                    FrontEndResource["RGBColorValueSelector"], {
                    0, {Left, Bottom}}, {Left, Top}, 
                    "ClosingActions" -> {
                    "SelectionDeparture", "ParentChanged", 
                    "EvaluatorQuit"}]]]], BaseStyle -> Inherited, Evaluator -> 
                    Automatic, Method -> "Preemptive"], 
                    RGBColor[0.922526, 0.385626, 0.209179], Editable -> False,
                     Selectable -> False], ",", 
                   RowBox[{"AbsoluteThickness", "[", "1.6`", "]"}]}], "]"}], 
               ",", 
               RowBox[{"Directive", "[", 
                 RowBox[{
                   RowBox[{"Opacity", "[", "1.`", "]"}], ",", 
                   InterpretationBox[
                    ButtonBox[
                    TooltipBox[
                    RowBox[{
                    GraphicsBox[{{
                    GrayLevel[0], 
                    RectangleBox[{0, 0}]}, {
                    GrayLevel[0], 
                    RectangleBox[{1, -1}]}, {
                    RGBColor[0.528488, 0.470624, 0.701351], 
                    RectangleBox[{0, -1}, {2, 1}]}}, AspectRatio -> 1, Frame -> 
                    True, FrameStyle -> 
                    RGBColor[
                    0.3523253333333333, 0.3137493333333333, 
                    0.46756733333333333`], FrameTicks -> None, 
                    PlotRangePadding -> None, ImageSize -> 
                    Dynamic[{
                    Automatic, 
                    1.35 (CurrentValue["FontCapHeight"]/AbsoluteCurrentValue[
                    Magnification])}]], "\[InvisibleSpace]"}], 
                    "RGBColor[0.528488, 0.470624, 0.701351]"], Appearance -> 
                    None, BaseStyle -> {}, BaselinePosition -> Baseline, 
                    DefaultBaseStyle -> {}, ButtonFunction :> 
                    With[{Typeset`box$ = EvaluationBox[]}, 
                    If[
                    Not[
                    AbsoluteCurrentValue["Deployed"]], 
                    SelectionMove[Typeset`box$, All, Expression]; 
                    FrontEnd`Private`$ColorSelectorInitialAlpha = 1; 
                    FrontEnd`Private`$ColorSelectorInitialColor = 
                    RGBColor[0.528488, 0.470624, 0.701351]; 
                    FrontEnd`Private`$ColorSelectorUseMakeBoxes = True; 
                    MathLink`CallFrontEnd[
                    FrontEnd`AttachCell[Typeset`box$, 
                    FrontEndResource["RGBColorValueSelector"], {
                    0, {Left, Bottom}}, {Left, Top}, 
                    "ClosingActions" -> {
                    "SelectionDeparture", "ParentChanged", 
                    "EvaluatorQuit"}]]]], BaseStyle -> Inherited, Evaluator -> 
                    Automatic, Method -> "Preemptive"], 
                    RGBColor[0.528488, 0.470624, 0.701351], Editable -> False,
                     Selectable -> False], ",", 
                   RowBox[{"AbsoluteThickness", "[", "1.6`", "]"}]}], "]"}], 
               ",", 
               RowBox[{"Directive", "[", 
                 RowBox[{
                   RowBox[{"Opacity", "[", "1.`", "]"}], ",", 
                   InterpretationBox[
                    ButtonBox[
                    TooltipBox[
                    RowBox[{
                    GraphicsBox[{{
                    GrayLevel[0], 
                    RectangleBox[{0, 0}]}, {
                    GrayLevel[0], 
                    RectangleBox[{1, -1}]}, {
                    RGBColor[0.772079, 0.431554, 0.102387], 
                    RectangleBox[{0, -1}, {2, 1}]}}, AspectRatio -> 1, Frame -> 
                    True, FrameStyle -> 
                    RGBColor[
                    0.5147193333333333, 0.28770266666666666`, 
                    0.06825800000000001], FrameTicks -> None, 
                    PlotRangePadding -> None, ImageSize -> 
                    Dynamic[{
                    Automatic, 
                    1.35 (CurrentValue["FontCapHeight"]/AbsoluteCurrentValue[
                    Magnification])}]], "\[InvisibleSpace]"}], 
                    "RGBColor[0.772079, 0.431554, 0.102387]"], Appearance -> 
                    None, BaseStyle -> {}, BaselinePosition -> Baseline, 
                    DefaultBaseStyle -> {}, ButtonFunction :> 
                    With[{Typeset`box$ = EvaluationBox[]}, 
                    If[
                    Not[
                    AbsoluteCurrentValue["Deployed"]], 
                    SelectionMove[Typeset`box$, All, Expression]; 
                    FrontEnd`Private`$ColorSelectorInitialAlpha = 1; 
                    FrontEnd`Private`$ColorSelectorInitialColor = 
                    RGBColor[0.772079, 0.431554, 0.102387]; 
                    FrontEnd`Private`$ColorSelectorUseMakeBoxes = True; 
                    MathLink`CallFrontEnd[
                    FrontEnd`AttachCell[Typeset`box$, 
                    FrontEndResource["RGBColorValueSelector"], {
                    0, {Left, Bottom}}, {Left, Top}, 
                    "ClosingActions" -> {
                    "SelectionDeparture", "ParentChanged", 
                    "EvaluatorQuit"}]]]], BaseStyle -> Inherited, Evaluator -> 
                    Automatic, Method -> "Preemptive"], 
                    RGBColor[0.772079, 0.431554, 0.102387], Editable -> False,
                     Selectable -> False], ",", 
                   RowBox[{"AbsoluteThickness", "[", "1.6`", "]"}]}], "]"}]}],
              "}"}], ",", 
           RowBox[{"{", 
             RowBox[{#, ",", #2, ",", #3, ",", #4, ",", #5, ",", #6}], "}"}], 
           ",", 
           RowBox[{"LabelStyle", "\[Rule]", 
             RowBox[{"{", "}"}]}], ",", 
           RowBox[{"LegendLayout", "\[Rule]", "\"Column\""}]}], "]"}]& ), 
      Editable -> True], TraditionalForm], TraditionalForm]},
  "Legended",
  DisplayFunction->(GridBox[{{
      TagBox[
       ItemBox[
        PaneBox[
         TagBox[#, "SkipImageSizeLevel"], Alignment -> {Center, Baseline}, 
         BaselinePosition -> Baseline], DefaultBaseStyle -> "Labeled"], 
       "SkipImageSizeLevel"], 
      ItemBox[#2, DefaultBaseStyle -> "LabeledLabel"]}}, 
    GridBoxAlignment -> {"Columns" -> {{Center}}, "Rows" -> {{Center}}}, 
    AutoDelete -> False, GridBoxItemSize -> Automatic, 
    BaselinePosition -> {1, 1}]& ),
  Editable->True,
  InterpretationFunction->(RowBox[{"Legended", "[", 
     RowBox[{#, ",", 
       RowBox[{"Placed", "[", 
         RowBox[{#2, ",", "After"}], "]"}]}], "]"}]& )]], "Output"]
}, Open  ]]
}, Open  ]],

Cell["g \:304c\:5c0f\:3055\:3044\:3046\:3061\:306f\:6b21\:6570\:3092\:3042\
\:3052\:308b\:3068\:53ce\:675f\:304c\:3088\:304f\:306a\:308b\:304c\:3001g \
\:304c\:5927\:304d\:304f\:306a\:308b\:3068\:6b21\:6570\:304c\:3042\:304c\:308b\
\:3068\:304b\:3048\:3063\:3066\:3072\:3069\:304f\:306a\:308b\:3053\:3068\:304c\
\:308f\:304b\:308b\:3002", "Subsection"]
}, Open  ]],

Cell[CellGroupData[{

Cell["\:6442\:52d5\:7d1a\:6570\:306e\:632f\:821e\:3044", "Section"],

Cell[CellGroupData[{

Cell["\:3068\:3044\:3046\:308f\:3051\:3067\:3001\:6442\:52d5\:5c55\:958b\:306e\
\:53ce\:675f\:534a\:5f84\:3092\:3088\:307f\:3068\:3063\:3066\:307f\:3088\:3046\
\:3002", "Subsection"],

Cell[TextData[{
 Cell[BoxData[
  FormBox[
   RowBox[{"\:305d\:306e\:305f\:3081", "\:3001"}], TraditionalForm]]],
 Cell[BoxData[
  FormBox[
   RowBox[{
    SubscriptBox["E", "0"], "(", "g", ")"}], TraditionalForm]]],
 " \:306e\:5c55\:958b\:4fc2\:6570 \:3092 ",
 Cell[BoxData[
  FormBox[
   RowBox[{
    SubscriptBox["c", "n"], " "}], TraditionalForm]]],
 "\:3068\:3057\:3066\:3001 ",
 Cell[BoxData[
  FormBox[
   RowBox[{"|", " ", 
    SubscriptBox["c", "n"], 
    SuperscriptBox["|", 
     RowBox[{"1", "/", "n"}]]}], TraditionalForm]]],
 " \:306e\:30c6\:30fc\:30d6\:30eb\:3092\:3064\:304f\:308b:"
}], "Text"],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"coeff", "=", 
  RowBox[{"Table", "[", 
   RowBox[{
    RowBox[{"{", 
     RowBox[{"i", ",", 
      RowBox[{
       RowBox[{
        RowBox[{"Abs", "[", 
         RowBox[{"Coefficient", "[", 
          RowBox[{
           RowBox[{"Energy", "[", "20", "]"}], ",", 
           RowBox[{"g", "^", "i"}]}], "]"}], "]"}], "^", 
        RowBox[{"(", 
         RowBox[{"1", "/", "i"}], ")"}]}], "//", "N"}]}], "}"}], ",", 
    RowBox[{"{", 
     RowBox[{"i", ",", "1", ",", "20"}], "}"}]}], "]"}]}]], "Input"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{"1", ",", "0.75`"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"2", ",", "1.620185174601965`"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"3", ",", "2.75068853282797`"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"4", ",", "3.9412538591771793`"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"5", ",", "5.138074421809732`"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"6", ",", "6.32427222324687`"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"7", ",", "7.495895549725044`"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"8", ",", "8.653653455943038`"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"9", ",", "9.79973072338696`"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"10", ",", "10.936522783361152`"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"11", ",", "12.066161402611224`"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"12", ",", "13.190379041880146`"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"13", ",", "14.310515393774233`"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"14", ",", "15.42757643116172`"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"15", ",", "16.542304984966012`"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"16", ",", "17.65524540346886`"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"17", ",", "18.76679637679227`"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"18", ",", "19.877251406328856`"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"19", ",", "20.986828598786943`"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"20", ",", "22.09569206106217`"}], "}"}]}], "}"}]], "Output"]
}, Open  ]],

Cell["\:8868\:793a\:3059\:308b\:3068", "Text"],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"ListPlot", "[", 
  RowBox[{"coeff", ",", 
   RowBox[{"AxesLabel", "\[Rule]", 
    RowBox[{"{", 
     RowBox[{
     "n", ",", 
      "\"\<|\!\(\*SubscriptBox[\(c\), \(n\)]\)\!\(\*SuperscriptBox[\(|\), \
\(1/n\)]\)\>\""}], "}"}]}]}], "]"}]], "Input"],

Cell[BoxData[
 GraphicsBox[{{}, {{}, 
    {RGBColor[0.368417, 0.506779, 0.709798], PointSize[0.019444444444444445`],
      AbsoluteThickness[1.6], PointBox[CompressedData["
1:eJxTTMoPSmViYGAQAWIQDQEf7CH0CyjN4JAipObl/uYnlM/hsPqy/98MRjYH
CF/AYa9RsdSGdn4oX8ThX8SMxOReEShfwuFaUjsLn48klC/jYG3CyH3mtyyU
r+CwNqlg1Wo/RShfySFSWtevbKYylK/iUPaX51X9fVUoX82B9ZPHi/uKGlC+
hoNawdyYykQtKF/LITdk1f3f83SgfB2H3kMePq9v6UH5eg6sj+/VX+kygPIN
HE4dWKW5b7khlG/oMIe58IjPESMo38ghtzekZ/IDYyjf2EGlp+/Mgj8mUL6J
w9b9CW71EmYOANtKTVI=
      "]]}, {}}, {}},
  AspectRatio->NCache[GoldenRatio^(-1), 0.6180339887498948],
  Axes->{True, True},
  AxesLabel->{
    FormBox["n", TraditionalForm], 
    FormBox[
    "\"|\\!\\(\\*SubscriptBox[\\(c\\), \
\\(n\\)]\\)\\!\\(\\*SuperscriptBox[\\(|\\), \\(1/n\\)]\\)\"", 
     TraditionalForm]},
  AxesOrigin->{0, 0},
  DisplayFunction->Identity,
  Frame->{{False, False}, {False, False}},
  FrameLabel->{{None, None}, {None, None}},
  FrameTicks->{{Automatic, Automatic}, {Automatic, Automatic}},
  GridLines->{None, None},
  GridLinesStyle->Directive[
    GrayLevel[0.5, 0.4]],
  Method->{},
  PlotRange->{{0, 20.}, {0, 22.09569206106217}},
  PlotRangeClipping->True,
  PlotRangePadding->{{
     Scaled[0.02], 
     Scaled[0.02]}, {
     Scaled[0.02], 
     Scaled[0.05]}},
  Ticks->{Automatic, Automatic}]], "Output"]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell[TextData[{
 "\:3060\:3044\:305f\:3044 ",
 Cell[BoxData[
  FormBox[
   RowBox[{
    RowBox[{
     SubscriptBox["c", 
      RowBox[{"n", " "}]], "~", " ", 
     SuperscriptBox["\[Alpha]", "n"]}], " ", 
    RowBox[{"n", "!"}]}], TraditionalForm]]],
 " \:3068\:306a\:3063\:3066\:3044\:308b\:306e\:304c\:307f\:3066\:3068\:308c\
\:308b\:3002\:3088\:3063\:3066\:3001\:53ce\:675f\:534a\:5f84\:306f\:30bc\:30ed\
\:3002n=20 \:307e\:3067\:3067 fit \:3059\:308b\:3068"
}], "Subsection"],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Fit", "[", 
  RowBox[{"coeff", ",", 
   RowBox[{"{", 
    RowBox[{"1", ",", "x"}], "}"}], ",", "x"}], "]"}]], "Input"],

Cell[BoxData[
 RowBox[{
  RowBox[{"-", "0.49820980578464996`"}], "+", 
  RowBox[{"1.1347296378124065`", " ", "x"}]}]], "Output"]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell[TextData[{
 Cell[BoxData[
  FormBox[
   RowBox[{
   "\:4e00\:65b9", "\:3001", 
    "\:5225\:306e\:8a08\:7b97\:306b\:3088\:308b\:3068", "\:3001"}], 
   TraditionalForm]]],
 Cell[BoxData[
  FormBox[
   SubscriptBox["c", 
    RowBox[{"n", " "}]], TraditionalForm]]],
 "~ ",
 Cell[BoxData[
  FormBox[
   RowBox[{
    SuperscriptBox["3", "n"], " ", 
    RowBox[{"n", "!"}]}], TraditionalForm]]],
 " \:3067\:3042\:308b\:306e\:304c\:3057\:3089\:308c\:3066\:3044\:308b\:306e\
\:3067\:3001"
}], "Subsection"],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Limit", "[", 
  RowBox[{
   RowBox[{
    RowBox[{"(", 
     RowBox[{
      RowBox[{"(", 
       RowBox[{
        RowBox[{"3", "^", "k"}], "  ", 
        RowBox[{"k", "!"}]}], ")"}], "^", 
      RowBox[{"(", 
       RowBox[{"1", "/", "k"}], ")"}]}], ")"}], "/", "k"}], ",", 
   RowBox[{"k", "\[Rule]", " ", "Infinity"}]}], "]"}]], "Input"],

Cell[BoxData[
 FractionBox["3", "\[ExponentialE]"]], "Output"]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"%", "//", "N"}]], "Input"],

Cell[BoxData["1.103638323514327`"], "Output"]
}, Open  ]]
}, Open  ]],

Cell["\:3068\:3044\:3046\:308f\:3051\:3067\:3001\:305d\:3093\:306a\:3082\:306e\
\:3067\:3042\:308d\:3046\:3002", "Subsection"]
}, Open  ]]
},
WindowSize->{808, 755},
Visible->True,
ScrollingOptions->{"VerticalScrollRange"->Fit},
ShowCellBracket->Automatic,
CellContext->Notebook,
TrackCellChangeTimes->False,
FrontEndVersion->"10.0 for Mac OS X x86 (32-bit, 64-bit Kernel) (2014\:5e749\
\:670811\:65e5)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[1486, 35, 37, 0, 64, "Section"],
Cell[CellGroupData[{
Cell[1548, 39, 372, 6, 94, "Subsection"],
Cell[1923, 47, 258, 5, 30, "Text"],
Cell[2184, 54, 435, 15, 28, "Input"],
Cell[2622, 71, 275, 9, 28, "Input"],
Cell[2900, 82, 292, 9, 28, "Input"],
Cell[3195, 93, 110, 2, 30, "Text"],
Cell[3308, 97, 388, 13, 28, "Input"],
Cell[3699, 112, 409, 14, 28, "Input"],
Cell[4111, 128, 114, 1, 30, "Text"],
Cell[4228, 131, 129, 4, 28, "Input"],
Cell[4360, 137, 209, 6, 31, "Text"],
Cell[4572, 145, 355, 12, 28, "Input"],
Cell[4930, 159, 47, 0, 30, "Text"],
Cell[CellGroupData[{
Cell[5002, 163, 114, 3, 28, "Input"],
Cell[5119, 168, 316, 15, 59, "Output"]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[5484, 189, 227, 2, 44, "Subsection"],
Cell[5714, 193, 202, 6, 28, "Input"],
Cell[5919, 201, 226, 7, 28, "Input"],
Cell[6148, 210, 208, 6, 28, "Input"]
}, Open  ]],
Cell[CellGroupData[{
Cell[6393, 221, 273, 7, 45, "Subsection"],
Cell[6669, 230, 70, 0, 30, "Text"],
Cell[6742, 232, 286, 9, 28, "Input"],
Cell[7031, 243, 281, 9, 28, "Input"],
Cell[7315, 254, 503, 17, 35, "Text"],
Cell[7821, 273, 475, 15, 28, "Input"]
}, Open  ]],
Cell[CellGroupData[{
Cell[8333, 293, 385, 6, 69, "Subsection"],
Cell[8721, 301, 722, 19, 53, "Text"],
Cell[9446, 322, 121, 1, 30, "Text"],
Cell[CellGroupData[{
Cell[9592, 327, 111, 3, 28, "Input"],
Cell[9706, 332, 54, 2, 35, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[9797, 339, 105, 3, 28, "Input"],
Cell[9905, 344, 48, 1, 47, "Output"]
}, Open  ]],
Cell[9968, 348, 527, 13, 53, "Text"],
Cell[10498, 363, 279, 7, 30, "Text"],
Cell[10780, 372, 322, 10, 28, "Input"],
Cell[11105, 384, 58, 0, 30, "Text"],
Cell[11166, 386, 509, 15, 46, "Input"],
Cell[11678, 403, 58, 0, 30, "Text"],
Cell[11739, 405, 571, 17, 46, "Input"]
}, Open  ]],
Cell[CellGroupData[{
Cell[12347, 427, 136, 1, 44, "Subsection"],
Cell[12486, 430, 98, 1, 30, "Text"],
Cell[CellGroupData[{
Cell[12609, 435, 60, 1, 28, "Input"],
Cell[12672, 438, 2112, 61, 337, "Output"]
}, Open  ]]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[14845, 506, 43, 0, 64, "Section"],
Cell[CellGroupData[{
Cell[14913, 510, 348, 4, 69, "Subsection"],
Cell[15264, 516, 688, 10, 68, "Text"],
Cell[15955, 528, 1646, 46, 97, "Input"],
Cell[17604, 576, 937, 14, 87, "Text"],
Cell[18544, 592, 1380, 39, 182, "Input"],
Cell[19927, 633, 207, 2, 30, "Text"],
Cell[CellGroupData[{
Cell[20159, 639, 61, 1, 28, "Input"],
Cell[20223, 642, 112, 2, 28, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[20372, 649, 63, 1, 28, "Input"],
Cell[20438, 652, 114, 2, 28, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[20589, 659, 65, 1, 28, "Input"],
Cell[20657, 662, 116, 2, 28, "Output"]
}, Open  ]],
Cell[20788, 667, 53, 0, 30, "Text"],
Cell[CellGroupData[{
Cell[20866, 671, 66, 1, 28, "Input"],
Cell[20935, 674, 45, 0, 28, "Output"]
}, Open  ]],
Cell[20995, 677, 96, 1, 30, "Text"]
}, Open  ]],
Cell[CellGroupData[{
Cell[21128, 683, 127, 1, 44, "Subsection"],
Cell[CellGroupData[{
Cell[21280, 688, 680, 18, 46, "Input"],
Cell[21963, 708, 41596, 802, 240, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[63596, 1515, 681, 18, 46, "Input"],
Cell[64280, 1535, 34902, 690, 245, "Output"]
}, Open  ]]
}, Open  ]],
Cell[99209, 2229, 352, 4, 69, "Subsection"]
}, Open  ]],
Cell[CellGroupData[{
Cell[99598, 2238, 67, 0, 64, "Section"],
Cell[CellGroupData[{
Cell[99690, 2242, 182, 2, 44, "Subsection"],
Cell[99875, 2246, 609, 21, 34, "Text"],
Cell[CellGroupData[{
Cell[100509, 2271, 524, 16, 28, "Input"],
Cell[101036, 2289, 1576, 42, 80, "Output"]
}, Open  ]],
Cell[102627, 2334, 46, 0, 30, "Text"],
Cell[CellGroupData[{
Cell[102698, 2338, 272, 8, 35, "Input"],
Cell[102973, 2348, 1322, 35, 259, "Output"]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[104344, 2389, 479, 13, 70, "Subsection"],
Cell[CellGroupData[{
Cell[104848, 2406, 142, 4, 28, "Input"],
Cell[104993, 2412, 128, 3, 28, "Output"]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[105170, 2421, 504, 19, 45, "Subsection"],
Cell[CellGroupData[{
Cell[105699, 2444, 362, 12, 28, "Input"],
Cell[106064, 2458, 62, 1, 48, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[106163, 2464, 50, 1, 28, "Input"],
Cell[106216, 2467, 45, 0, 28, "Output"]
}, Open  ]]
}, Open  ]],
Cell[106288, 2471, 126, 1, 44, "Subsection"]
}, Open  ]]
}
]
*)

(* End of internal cache information *)

(* NotebookSignature mwpZEjzio854LBgUct9rcnGp *)
