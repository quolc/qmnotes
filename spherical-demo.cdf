(* Content-type: application/vnd.wolfram.cdf.text *)

(*** Wolfram CDF File ***)
(* http://www.wolfram.com/cdf *)

(* CreatedBy='Mathematica 10.0' *)

(*************************************************************************)
(*                                                                       *)
(*  The Mathematica License under which this file was created prohibits  *)
(*  restricting third parties in receipt of this file from republishing  *)
(*  or redistributing it by any means, including but not limited to      *)
(*  rights management or terms of use, without the express consent of    *)
(*  Wolfram Research, Inc. For additional information concerning CDF     *)
(*  licensing and redistribution see:                                    *)
(*                                                                       *)
(*        www.wolfram.com/cdf/adopting-cdf/licensing-options.html        *)
(*                                                                       *)
(*************************************************************************)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[      1064,         20]
NotebookDataLength[     24560,        570]
NotebookOptionsPosition[     24793,        555]
NotebookOutlinePosition[     25244,        575]
CellTagsIndexPosition[     25201,        572]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell["\<\
Energy eigenvalues can be found by tuning E in the Sch. eq. so that the wave \
function does not diverge in the large r region. In the demo below, L is the \
orbital angular momentum, En is the energy in the Sch. eq. The potential is \
harmonic. \
\>", "Subsection"],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Manipulate", "[", 
  RowBox[{"(*", " ", 
   RowBox[{"First", ",", " ", 
    RowBox[{"numerically", " ", "solve", " ", "the", " ", 
     RowBox[{"Sch", ".", " ", "eq", ".", " ", "from"}], " ", "the", " ", 
     "origin"}], ",", " ", 
    RowBox[{"giving", " ", "the", " ", 
     RowBox[{"b", ".", "c", ".", " ", "appropriate"}], " ", "at", " ", 
     RowBox[{"r", "~", "0."}], " ", "Tell", " ", "Mathematica", " ", "to", 
     " ", "stop", " ", "when", " ", "the", " ", "wavefunction", " ", "blows", 
     " ", 
     RowBox[{"up", "."}]}]}], "*)"}], "\[IndentingNewLine]", 
  RowBox[{
   RowBox[{
    RowBox[{"s", "=", 
     RowBox[{
      RowBox[{"NDSolve", "[", 
       RowBox[{
        RowBox[{"{", "\[IndentingNewLine]", 
         RowBox[{
          RowBox[{
           RowBox[{
            RowBox[{
             RowBox[{
              RowBox[{"-", " ", 
               RowBox[{"D", "[", " ", 
                RowBox[{
                 RowBox[{"r", " ", 
                  RowBox[{"F", "[", "r", "]"}]}], ",", 
                 RowBox[{"{", 
                  RowBox[{"r", ",", "2"}], "}"}]}], "]"}]}], "/", "r"}], " ", 
             "/", "2"}], " ", "+", 
            RowBox[{
             RowBox[{"(", " ", 
              RowBox[{
               RowBox[{"L", 
                RowBox[{
                 RowBox[{
                  RowBox[{"(", 
                   RowBox[{"L", "+", "1"}], ")"}], "/", 
                  RowBox[{"r", "^", "2"}]}], " ", "/", "2"}]}], "+", " ", 
               RowBox[{"V", "[", "r", "]"}], "-", " ", "En"}], ")"}], " ", 
             RowBox[{"F", "[", "r", "]"}]}]}], "\[Equal]", "0"}], ",", 
          "\[IndentingNewLine]", 
          RowBox[{
           RowBox[{"F", "[", "r0", "]"}], "\[Equal]", 
           RowBox[{"r0", "^", "L"}]}], ",", 
          RowBox[{
           RowBox[{
            RowBox[{"F", "'"}], "[", "r0", "]"}], "\[Equal]", 
           RowBox[{"L", " ", 
            RowBox[{"r0", "^", 
             RowBox[{"(", 
              RowBox[{"L", "-", "1"}], ")"}]}]}]}], ",", 
          RowBox[{"WhenEvent", "[", 
           RowBox[{
            RowBox[{
             RowBox[{"Abs", "[", 
              RowBox[{"F", "[", "r", "]"}], "]"}], ">", 
             RowBox[{"10", "^", "3"}]}], ",", "\"\<StopIntegration\>\""}], 
           "]"}]}], "}"}], ",", "\[IndentingNewLine]", 
        RowBox[{"F", "[", "r", "]"}], ",", 
        RowBox[{"{", 
         RowBox[{"r", ",", "r0", ",", "rE"}], "}"}]}], "]"}], 
      "\[LeftDoubleBracket]", "1", "\[RightDoubleBracket]"}]}], ";", 
    "\[IndentingNewLine]", 
    RowBox[{"(*", 
     RowBox[{
      RowBox[{
      "Find", " ", "the", " ", "maximum", " ", "of", " ", "the", " ", 
       RowBox[{"sol", "'"}], "n"}], ",", " ", 
      RowBox[{"just", " ", "to", " ", "prettify", " ", "the", " ", "plot"}]}],
      "*)"}], "\[IndentingNewLine]", 
    RowBox[{"m", "=", 
     RowBox[{"NMaxValue", "[", 
      RowBox[{
       RowBox[{"{", 
        RowBox[{
         RowBox[{
          RowBox[{"F", "[", "r", "]"}], "/.", "s"}], ",", 
         RowBox[{"r", "\[GreaterEqual]", " ", "r0"}], ",", 
         RowBox[{"r", "<", " ", "2"}]}], "}"}], ",", "r"}], "]"}]}], ";", 
    "\[IndentingNewLine]", 
    RowBox[{"(*", 
     RowBox[{
      RowBox[{"Plot", " ", "the", " ", "potential"}], ",", " ", 
      RowBox[{"etc", "."}]}], "*)"}], "\[IndentingNewLine]", 
    RowBox[{"Plot", "[", 
     RowBox[{
      RowBox[{"{", 
       RowBox[{
        RowBox[{"V", "[", "r", "]"}], ",", 
        RowBox[{"L", 
         RowBox[{
          RowBox[{"(", 
           RowBox[{"L", "+", "1"}], ")"}], "/", 
          RowBox[{"r", "^", "2"}]}]}], ",", "\[IndentingNewLine]", 
        RowBox[{"(", 
         RowBox[{
          RowBox[{"5", 
           RowBox[{
            RowBox[{"F", "[", "r", "]"}], "/", "m"}]}], "/.", "s"}], ")"}]}], 
       "}"}], ",", 
      RowBox[{"{", 
       RowBox[{"r", ",", "r0", ",", "rE"}], "}"}], ",", "\[IndentingNewLine]", 
      RowBox[{"PlotRange", "\[Rule]", 
       RowBox[{"{", 
        RowBox[{
         RowBox[{"-", "10"}], ",", "10"}], "}"}]}], ",", 
      RowBox[{"ImageSize", "\[Rule]", "Full"}], ",", 
      RowBox[{"PlotLegends", "\[Rule]", 
       RowBox[{"Placed", "[", " ", 
        RowBox[{
         RowBox[{"{", 
          RowBox[{
          "\"\<original pot.\>\"", ",", "\"\<centrifugal pot.\>\"", ",", 
           "\"\<wave function\>\""}], "}"}], ",", "Above"}], "]"}]}]}], 
     "]"}]}], ",", "\[IndentingNewLine]", 
   RowBox[{"(*", 
    RowBox[{"creates", " ", "the", " ", "sliders"}], "*)"}], 
   "\[IndentingNewLine]", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{"L", ",", "2"}], "}"}], ",", "0", ",", "5", ",", "1", ",", 
     RowBox[{"Appearance", "\[Rule]", " ", 
      RowBox[{"{", "\"\<Open\>\"", "}"}]}]}], "}"}], ",", 
   "\[IndentingNewLine]", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{"En", ",", "3"}], "}"}], ",", "0", ",", "20", ",", 
     RowBox[{"Appearance", "\[Rule]", " ", 
      RowBox[{"{", "\"\<Open\>\"", "}"}]}]}], "}"}], ",", 
   "\[IndentingNewLine]", 
   RowBox[{"(*", 
    RowBox[{"Definitions", " ", "used"}], "*)"}], "\[IndentingNewLine]", 
   RowBox[{"Initialization", "\[RuleDelayed]", 
    RowBox[{"{", "\[IndentingNewLine]", 
     RowBox[{
      RowBox[{
       RowBox[{"V", "[", "r_", "]"}], ":=", 
       RowBox[{
        RowBox[{"r", "^", "2"}], "/", "2"}]}], ";", "\[IndentingNewLine]", 
      RowBox[{"r0", "=", ".0001"}], ";", 
      RowBox[{"rE", "=", "10"}]}], "}"}]}]}], "]"}]], "Input"],

Cell[BoxData[
 TagBox[
  StyleBox[
   DynamicModuleBox[{$CellContext`En$$ = 11.5, $CellContext`L$$ = 2, 
    Typeset`show$$ = True, Typeset`bookmarkList$$ = {}, 
    Typeset`bookmarkMode$$ = "Menu", Typeset`animator$$, Typeset`animvar$$ = 
    1, Typeset`name$$ = "\"untitled\"", Typeset`specs$$ = {{{
       Hold[$CellContext`L$$], 2}, 0, 5, 1}, {{
       Hold[$CellContext`En$$], 3}, 0, 20}}, Typeset`size$$ = {
    600., {187., 223.}}, Typeset`update$$ = 0, Typeset`initDone$$, 
    Typeset`skipInitDone$$ = False, $CellContext`L$142908$$ = 
    0, $CellContext`En$142909$$ = 0}, 
    DynamicBox[Manipulate`ManipulateBoxes[
     1, StandardForm, 
      "Variables" :> {$CellContext`En$$ = 3, $CellContext`L$$ = 2}, 
      "ControllerVariables" :> {
        Hold[$CellContext`L$$, $CellContext`L$142908$$, 0], 
        Hold[$CellContext`En$$, $CellContext`En$142909$$, 0]}, 
      "OtherVariables" :> {
       Typeset`show$$, Typeset`bookmarkList$$, Typeset`bookmarkMode$$, 
        Typeset`animator$$, Typeset`animvar$$, Typeset`name$$, 
        Typeset`specs$$, Typeset`size$$, Typeset`update$$, Typeset`initDone$$,
         Typeset`skipInitDone$$}, "Body" :> ($CellContext`s = Part[
          
          NDSolve[{((-
                D[$CellContext`r $CellContext`F[$CellContext`r], \
{$CellContext`r, 2}])/$CellContext`r)/
              2 + ($CellContext`L$$ ((($CellContext`L$$ + 
                   1)/$CellContext`r^2)/
                 2) + $CellContext`V[$CellContext`r] - $CellContext`En$$) \
$CellContext`F[$CellContext`r] == 
            0, $CellContext`F[$CellContext`r0] == \
$CellContext`r0^$CellContext`L$$, 
            Derivative[
             1][$CellContext`F][$CellContext`r0] == $CellContext`L$$ \
$CellContext`r0^($CellContext`L$$ - 1), 
            WhenEvent[Abs[
               $CellContext`F[$CellContext`r]] > 10^3, "StopIntegration"]}, 
           $CellContext`F[$CellContext`r], {$CellContext`r, $CellContext`r0, \
$CellContext`rE}], 1]; $CellContext`m = NMaxValue[{
           ReplaceAll[
            $CellContext`F[$CellContext`r], $CellContext`s], $CellContext`r >= \
$CellContext`r0, $CellContext`r < 2}, $CellContext`r]; Plot[{
          $CellContext`V[$CellContext`r], $CellContext`L$$ (($CellContext`L$$ + 
            1)/$CellContext`r^2), 
          ReplaceAll[
          5 ($CellContext`F[$CellContext`r]/$CellContext`m), \
$CellContext`s]}, {$CellContext`r, $CellContext`r0, $CellContext`rE}, 
         PlotRange -> {-10, 10}, ImageSize -> Full, PlotLegends -> 
         Placed[{"original pot.", "centrifugal pot.", "wave function"}, 
           Above]]), 
      "Specifications" :> {{{$CellContext`L$$, 2}, 0, 5, 1, 
         Appearance -> {"Open"}}, {{$CellContext`En$$, 3}, 0, 20, 
         Appearance -> {"Open"}}}, "Options" :> {}, "DefaultOptions" :> {}],
     ImageSizeCache->{645., {288., 293.}},
     SingleEvaluation->True],
    Deinitialization:>None,
    DynamicModuleValues:>{},
    Initialization:>({$CellContext`V[
          Pattern[$CellContext`r, 
           Blank[]]] := $CellContext`r^2/2; $CellContext`r0 = 
        0.0001; $CellContext`rE = 10}; Typeset`initDone$$ = True),
    SynchronousInitialization->True,
    UndoTrackedVariables:>{Typeset`show$$, Typeset`bookmarkMode$$},
    UnsavedVariables:>{Typeset`initDone$$},
    UntrackedVariables:>{Typeset`size$$}], "Manipulate",
   Deployed->True,
   StripOnInput->False],
  Manipulate`InterpretManipulate[1]]], "Output",
 GeneratedCell->False,
 CellAutoOverwrite->False],

Cell[BoxData[
 TagBox[
  StyleBox[
   DynamicModuleBox[{$CellContext`En$$ = 15.5, $CellContext`L$$ = 4, 
    Typeset`show$$ = True, Typeset`bookmarkList$$ = {}, 
    Typeset`bookmarkMode$$ = "Menu", Typeset`animator$$, Typeset`animvar$$ = 
    1, Typeset`name$$ = "\"untitled\"", Typeset`specs$$ = {{{
       Hold[$CellContext`L$$], 2}, 0, 5, 1}, {{
       Hold[$CellContext`En$$], 3}, 0, 20}}, Typeset`size$$ = {
    600., {187., 223.}}, Typeset`update$$ = 0, Typeset`initDone$$, 
    Typeset`skipInitDone$$ = False, $CellContext`L$127361$$ = 
    0, $CellContext`En$127362$$ = 0}, 
    DynamicBox[Manipulate`ManipulateBoxes[
     1, StandardForm, 
      "Variables" :> {$CellContext`En$$ = 3, $CellContext`L$$ = 2}, 
      "ControllerVariables" :> {
        Hold[$CellContext`L$$, $CellContext`L$127361$$, 0], 
        Hold[$CellContext`En$$, $CellContext`En$127362$$, 0]}, 
      "OtherVariables" :> {
       Typeset`show$$, Typeset`bookmarkList$$, Typeset`bookmarkMode$$, 
        Typeset`animator$$, Typeset`animvar$$, Typeset`name$$, 
        Typeset`specs$$, Typeset`size$$, Typeset`update$$, Typeset`initDone$$,
         Typeset`skipInitDone$$}, "Body" :> ($CellContext`s = Part[
          
          NDSolve[{((-
                D[$CellContext`r $CellContext`F[$CellContext`r], \
{$CellContext`r, 2}])/$CellContext`r)/
              2 + ($CellContext`L$$ ((($CellContext`L$$ + 
                   1)/$CellContext`r^2)/
                 2) + $CellContext`V[$CellContext`r] - $CellContext`En$$) \
$CellContext`F[$CellContext`r] == 
            0, $CellContext`F[$CellContext`r0] == \
$CellContext`r0^$CellContext`L$$, 
            Derivative[
             1][$CellContext`F][$CellContext`r0] == $CellContext`L$$ \
$CellContext`r0^($CellContext`L$$ - 1), 
            WhenEvent[Abs[
               $CellContext`F[$CellContext`r]] > 10^3, "StopIntegration"]}, 
           $CellContext`F[$CellContext`r], {$CellContext`r, $CellContext`r0, \
$CellContext`rE}], 1]; $CellContext`m = NMaxValue[{
           ReplaceAll[
            $CellContext`F[$CellContext`r], $CellContext`s], $CellContext`r >= \
$CellContext`r0, $CellContext`r < 2}, $CellContext`r]; Plot[{
          $CellContext`V[$CellContext`r], $CellContext`L$$ (($CellContext`L$$ + 
            1)/$CellContext`r^2), 
          ReplaceAll[
          5 ($CellContext`F[$CellContext`r]/$CellContext`m), \
$CellContext`s]}, {$CellContext`r, $CellContext`r0, $CellContext`rE}, 
         PlotRange -> {-10, 10}, ImageSize -> Full, PlotLegends -> 
         Placed[{"original pot.", "centrifugal pot.", "wave function"}, 
           Above]]), 
      "Specifications" :> {{{$CellContext`L$$, 2}, 0, 5, 1, 
         Appearance -> {"Open"}}, {{$CellContext`En$$, 3}, 0, 20, 
         Appearance -> {"Open"}}}, "Options" :> {}, "DefaultOptions" :> {}],
     ImageSizeCache->{645., {288., 293.}},
     SingleEvaluation->True],
    Deinitialization:>None,
    DynamicModuleValues:>{},
    Initialization:>({$CellContext`V[
          Pattern[$CellContext`r, 
           Blank[]]] := $CellContext`r^2/2; $CellContext`r0 = 
        0.001; $CellContext`rE = 10}; Typeset`initDone$$ = True),
    SynchronousInitialization->True,
    UndoTrackedVariables:>{Typeset`show$$, Typeset`bookmarkMode$$},
    UnsavedVariables:>{Typeset`initDone$$},
    UntrackedVariables:>{Typeset`size$$}], "Manipulate",
   Deployed->True,
   StripOnInput->False],
  Manipulate`InterpretManipulate[1]]], "Output",
 GeneratedCell->False,
 CellAutoOverwrite->False],

Cell[BoxData[
 TagBox[
  StyleBox[
   DynamicModuleBox[{$CellContext`En$$ = 17.50001, $CellContext`L$$ = 2, 
    Typeset`show$$ = True, Typeset`bookmarkList$$ = {}, 
    Typeset`bookmarkMode$$ = "Menu", Typeset`animator$$, Typeset`animvar$$ = 
    1, Typeset`name$$ = "\"untitled\"", Typeset`specs$$ = {{{
       Hold[$CellContext`L$$], 2}, 0, 5, 1}, {{
       Hold[$CellContext`En$$], 3}, 0, 50}}, Typeset`size$$ = {
    600., {187., 223.}}, Typeset`update$$ = 0, Typeset`initDone$$, 
    Typeset`skipInitDone$$ = False, $CellContext`L$115671$$ = 
    0, $CellContext`En$115672$$ = 0}, 
    DynamicBox[Manipulate`ManipulateBoxes[
     1, StandardForm, 
      "Variables" :> {$CellContext`En$$ = 3, $CellContext`L$$ = 2}, 
      "ControllerVariables" :> {
        Hold[$CellContext`L$$, $CellContext`L$115671$$, 0], 
        Hold[$CellContext`En$$, $CellContext`En$115672$$, 0]}, 
      "OtherVariables" :> {
       Typeset`show$$, Typeset`bookmarkList$$, Typeset`bookmarkMode$$, 
        Typeset`animator$$, Typeset`animvar$$, Typeset`name$$, 
        Typeset`specs$$, Typeset`size$$, Typeset`update$$, Typeset`initDone$$,
         Typeset`skipInitDone$$}, "Body" :> ($CellContext`s = Part[
          
          NDSolve[{((-
                D[$CellContext`r $CellContext`F[$CellContext`r], \
{$CellContext`r, 2}])/$CellContext`r)/
              2 + ($CellContext`L$$ ((($CellContext`L$$ + 
                   1)/$CellContext`r^2)/
                 2) + $CellContext`V[$CellContext`r] - $CellContext`En$$) \
$CellContext`F[$CellContext`r] == 
            0, $CellContext`F[$CellContext`r0] == \
$CellContext`r0^$CellContext`L$$, 
            Derivative[
             1][$CellContext`F][$CellContext`r0] == $CellContext`L$$ \
$CellContext`r0^($CellContext`L$$ - 1), 
            WhenEvent[Abs[
               $CellContext`F[$CellContext`r]] > 10^3, "StopIntegration"]}, 
           $CellContext`F[$CellContext`r], {$CellContext`r, $CellContext`r0, \
$CellContext`rE}], 1]; $CellContext`m = NMaxValue[{
           ReplaceAll[
            $CellContext`F[$CellContext`r], $CellContext`s], $CellContext`r >= \
$CellContext`r0, $CellContext`r < 2}, $CellContext`r]; Plot[{
          $CellContext`V[$CellContext`r], $CellContext`L$$ (($CellContext`L$$ + 
            1)/$CellContext`r^2), 
          ReplaceAll[
          5 ($CellContext`F[$CellContext`r]/$CellContext`m), \
$CellContext`s]}, {$CellContext`r, $CellContext`r0, $CellContext`rE}, 
         PlotRange -> {-10, 10}, ImageSize -> Full, PlotLegends -> 
         Placed[{"original pot.", "centrifugal pot.", "wave function"}, 
           Above]]), 
      "Specifications" :> {{{$CellContext`L$$, 2}, 0, 5, 1, 
         Appearance -> {"Open"}}, {{$CellContext`En$$, 3}, 0, 50, 
         Appearance -> {"Open"}}}, "Options" :> {}, "DefaultOptions" :> {}],
     ImageSizeCache->{645., {288., 293.}},
     SingleEvaluation->True],
    Deinitialization:>None,
    DynamicModuleValues:>{},
    Initialization:>({$CellContext`V[
          Pattern[$CellContext`r, 
           Blank[]]] := $CellContext`r^2/2; $CellContext`r0 = 
        0.001; $CellContext`rE = 15}; Typeset`initDone$$ = True),
    SynchronousInitialization->True,
    UndoTrackedVariables:>{Typeset`show$$, Typeset`bookmarkMode$$},
    UnsavedVariables:>{Typeset`initDone$$},
    UntrackedVariables:>{Typeset`size$$}], "Manipulate",
   Deployed->True,
   StripOnInput->False],
  Manipulate`InterpretManipulate[1]]], "Output",
 GeneratedCell->False,
 CellAutoOverwrite->False],

Cell[BoxData[
 TagBox[
  StyleBox[
   DynamicModuleBox[{$CellContext`En$$ = 0., $CellContext`L$$ = 4, 
    Typeset`show$$ = True, Typeset`bookmarkList$$ = {}, 
    Typeset`bookmarkMode$$ = "Menu", Typeset`animator$$, Typeset`animvar$$ = 
    1, Typeset`name$$ = "\"untitled\"", Typeset`specs$$ = {{{
       Hold[$CellContext`L$$], 2}, 0, 5, 1}, {{
       Hold[$CellContext`En$$], 3}, 0, 50}}, Typeset`size$$ = {
    600., {187., 223.}}, Typeset`update$$ = 0, Typeset`initDone$$, 
    Typeset`skipInitDone$$ = False, $CellContext`L$26635$$ = 
    0, $CellContext`En$26636$$ = 0}, 
    DynamicBox[Manipulate`ManipulateBoxes[
     1, StandardForm, 
      "Variables" :> {$CellContext`En$$ = 3, $CellContext`L$$ = 2}, 
      "ControllerVariables" :> {
        Hold[$CellContext`L$$, $CellContext`L$26635$$, 0], 
        Hold[$CellContext`En$$, $CellContext`En$26636$$, 0]}, 
      "OtherVariables" :> {
       Typeset`show$$, Typeset`bookmarkList$$, Typeset`bookmarkMode$$, 
        Typeset`animator$$, Typeset`animvar$$, Typeset`name$$, 
        Typeset`specs$$, Typeset`size$$, Typeset`update$$, Typeset`initDone$$,
         Typeset`skipInitDone$$}, "Body" :> ($CellContext`s = Part[
          
          NDSolve[{((-
                D[$CellContext`r $CellContext`F[$CellContext`r], \
{$CellContext`r, 2}])/$CellContext`r)/
              2 + ($CellContext`L$$ ((($CellContext`L$$ + 
                   1)/$CellContext`r^2)/
                 2) + $CellContext`V[$CellContext`r] - $CellContext`En$$) \
$CellContext`F[$CellContext`r] == 
            0, $CellContext`F[$CellContext`r0] == \
$CellContext`r0^$CellContext`L$$, 
            Derivative[
             1][$CellContext`F][$CellContext`r0] == $CellContext`L$$ \
$CellContext`r0^($CellContext`L$$ - 1), 
            WhenEvent[Abs[
               $CellContext`F[$CellContext`r]] > 10^3, "StopIntegration"]}, 
           $CellContext`F[$CellContext`r], {$CellContext`r, $CellContext`r0, \
$CellContext`rE}], 1]; $CellContext`m = NMaxValue[{
           ReplaceAll[
            $CellContext`F[$CellContext`r], $CellContext`s], $CellContext`r >= \
$CellContext`r0, $CellContext`r < 2}, $CellContext`r]; Plot[{
          $CellContext`V[$CellContext`r], $CellContext`L$$ (($CellContext`L$$ + 
            1)/$CellContext`r^2), 
          ReplaceAll[
          5 ($CellContext`F[$CellContext`r]/$CellContext`m), \
$CellContext`s]}, {$CellContext`r, $CellContext`r0, $CellContext`rE}, 
         PlotRange -> {-10, 10}, ImageSize -> Full, PlotLegends -> 
         Placed[{"original pot.", "centrifugal pot.", "wave function"}, 
           Above]]), 
      "Specifications" :> {{{$CellContext`L$$, 2}, 0, 5, 1, 
         Appearance -> {"Open"}}, {{$CellContext`En$$, 3}, 0, 50, 
         Appearance -> {"Open"}}}, "Options" :> {}, "DefaultOptions" :> {}],
     ImageSizeCache->{645., {288., 293.}},
     SingleEvaluation->True],
    Deinitialization:>None,
    DynamicModuleValues:>{},
    Initialization:>({$CellContext`V[
          Pattern[$CellContext`r, 
           Blank[]]] := $CellContext`r^2/2; $CellContext`r0 = 
        0.001; $CellContext`rE = 5}; Typeset`initDone$$ = True),
    SynchronousInitialization->True,
    UndoTrackedVariables:>{Typeset`show$$, Typeset`bookmarkMode$$},
    UnsavedVariables:>{Typeset`initDone$$},
    UntrackedVariables:>{Typeset`size$$}], "Manipulate",
   Deployed->True,
   StripOnInput->False],
  Manipulate`InterpretManipulate[1]]], "Output",
 GeneratedCell->False,
 CellAutoOverwrite->False],

Cell[BoxData[
 TagBox[
  StyleBox[
   DynamicModuleBox[{$CellContext`En$$ = 5.6, $CellContext`L$$ = 2, 
    Typeset`show$$ = True, Typeset`bookmarkList$$ = {}, 
    Typeset`bookmarkMode$$ = "Menu", Typeset`animator$$, Typeset`animvar$$ = 
    1, Typeset`name$$ = "\"\:540d\:79f0\:672a\:5b9a\:7fa9\"", 
    Typeset`specs$$ = {{{
       Hold[$CellContext`L$$], 2}, 0, 5, 1}, {{
       Hold[$CellContext`En$$], 3}, 0, 50}}, Typeset`size$$ = {
    600., {187., 223.}}, Typeset`update$$ = 0, Typeset`initDone$$, 
    Typeset`skipInitDone$$ = False, $CellContext`L$31218$$ = 
    0, $CellContext`En$31219$$ = 0}, 
    DynamicBox[Manipulate`ManipulateBoxes[
     1, StandardForm, 
      "Variables" :> {$CellContext`En$$ = 3, $CellContext`L$$ = 2}, 
      "ControllerVariables" :> {
        Hold[$CellContext`L$$, $CellContext`L$31218$$, 0], 
        Hold[$CellContext`En$$, $CellContext`En$31219$$, 0]}, 
      "OtherVariables" :> {
       Typeset`show$$, Typeset`bookmarkList$$, Typeset`bookmarkMode$$, 
        Typeset`animator$$, Typeset`animvar$$, Typeset`name$$, 
        Typeset`specs$$, Typeset`size$$, Typeset`update$$, Typeset`initDone$$,
         Typeset`skipInitDone$$}, "Body" :> ($CellContext`s = Part[
          
          NDSolve[{((-
                D[$CellContext`r $CellContext`F[$CellContext`r], \
{$CellContext`r, 2}])/$CellContext`r)/
              2 + ($CellContext`L$$ ((($CellContext`L$$ + 
                   1)/$CellContext`r^2)/
                 2) + $CellContext`V[$CellContext`r] - $CellContext`En$$) \
$CellContext`F[$CellContext`r] == 
            0, $CellContext`F[$CellContext`r0] == \
$CellContext`r0^$CellContext`L$$, 
            Derivative[
             1][$CellContext`F][$CellContext`r0] == $CellContext`L$$ \
$CellContext`r0^($CellContext`L$$ - 1), 
            WhenEvent[Abs[
               $CellContext`F[$CellContext`r]] > 10^3, "StopIntegration"]}, 
           $CellContext`F[$CellContext`r], {$CellContext`r, $CellContext`r0, \
$CellContext`rE}], 1]; $CellContext`m = NMaxValue[{
           ReplaceAll[
            $CellContext`F[$CellContext`r], $CellContext`s], $CellContext`r >= \
$CellContext`r0, $CellContext`r < 2}, $CellContext`r]; Plot[{
          $CellContext`V[$CellContext`r], $CellContext`L$$ (($CellContext`L$$ + 
            1)/$CellContext`r^2), 
          ReplaceAll[
          10 ($CellContext`F[$CellContext`r]/$CellContext`m), \
$CellContext`s]}, {$CellContext`r, $CellContext`r0, $CellContext`rE}, 
         PlotRange -> {-10, 10}, ImageSize -> Full, PlotLegends -> 
         Placed[{"original pot.", "centrifugal pot.", "wave function"}, 
           Above]]), 
      "Specifications" :> {{{$CellContext`L$$, 2}, 0, 5, 1, 
         Appearance -> {"Open"}}, {{$CellContext`En$$, 3}, 0, 50, 
         Appearance -> {"Open"}}}, "Options" :> {}, "DefaultOptions" :> {}],
     ImageSizeCache->{645., {288., 293.}},
     SingleEvaluation->True],
    Deinitialization:>None,
    DynamicModuleValues:>{},
    Initialization:>({$CellContext`V[
          Pattern[$CellContext`r, 
           Blank[]]] := $CellContext`r^2/2; $CellContext`r0 = 
        0.001; $CellContext`rE = 5}; Typeset`initDone$$ = True),
    SynchronousInitialization->True,
    UndoTrackedVariables:>{Typeset`show$$, Typeset`bookmarkMode$$},
    UnsavedVariables:>{Typeset`initDone$$},
    UntrackedVariables:>{Typeset`size$$}], "Manipulate",
   Deployed->True,
   StripOnInput->False],
  Manipulate`InterpretManipulate[1]]], "Output",
 GeneratedCell->False,
 CellAutoOverwrite->False]
}, Open  ]]
}, Open  ]]
},
WindowSize->{772, 911},
Visible->True,
ScrollingOptions->{"VerticalScrollRange"->Fit},
ShowCellBracket->Automatic,
CellContext->Notebook,
TrackCellChangeTimes->False,
FrontEndVersion->"10.0 for Mac OS X x86 (32-bit, 64-bit Kernel) (September \
11, 2014)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[1486, 35, 276, 5, 94, "Subsection"],
Cell[CellGroupData[{
Cell[1787, 44, 5528, 146, 420, "Input"],
Cell[7318, 192, 3484, 70, 598, "Output"],
Cell[10805, 264, 3483, 70, 598, "Output"],
Cell[14291, 336, 3487, 70, 598, "Output"],
Cell[17781, 408, 3476, 70, 598, "Output"],
Cell[21260, 480, 3505, 71, 598, "Output"]
}, Open  ]]
}, Open  ]]
}
]
*)

(* End of internal cache information *)

(* NotebookSignature GxpBGdQeRCqrYAKDmMdXJUZK *)
